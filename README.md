# Changelog

## v2.2.4

* [Zlepšit hlášky při načítání a ukládání rozpisu z webu](https://trello.com/c/sDcTfdjl)

## v2.2.3

* [Ukládat playoff nasazení do json souboru](https://trello.com/c/5DZffAGz)

## v2.2.2

* [Oprava podporu pro JAVA 9 a 10](https://trello.com/c/bhWoPkdI)

## v2.2.1

* [Oprava řazení tabulky se stejným počtem bodů a remízovým vzájemným zápasem](https://trello.com/c/IoLcOTH)

## v2.2.0

### Novinky

* [Rozšíření zápasu o hráčské/týmové statistiky a obecnou poznámku](https://trello.com/c/mWDxe3sq/)
* [Podpora pro turnaje jednotlivců hrané na sety (_typicky tenis, ping pong..._)](https://trello.com/c/mWDxe3sq/)
* [Aktualizace playoff pavouka, když postoupí hůže nasazený tým](https://trello.com/c/gpNjOa2A)

### Vylepšení

* [Zlepšení zobrazení turnajových statistik a statistik střelců (předvybraný řádek, velikost modalu)](https://trello.com/c/O7p7eu1N)
* [Skrytí dnu, pokud se všechny zápasy hrají ve stejný den](https://trello.com/c/O7p7eu1N)
* [Pamatovat si zobrazené sloupce v seznamu zápasů, když se vracím z detailu zápasu](https://trello.com/c/HFaWw1vs)
* [Přidání podpory pro JAVA 9](https://trello.com/c/AbjzZ1kM/)

### Opravy

* [Řazení více než 2 týmů se stejným počtem bodů (_porovnávaly se jen 2 týmy bez minitabulky_)](https://trello.com/c/Zjc0QEpE)
* [Ukládání výsledků po smazání týmů (_teď se aktualizovaly jiné zápasy_)](https://trello.com/c/IAX0vExI/)
* [Zobrazení zimního času (_teď byly všechny časy +2h_)](https://trello.com/c/O7p7eu1N/)
* [Řazení seznamu zápasu podle pořadí/hřiště (_teď se čísla špatně řadila_)](https://trello.com/c/HFaWw1vs)
* [Zobrazení hodně hráčů při editaci zápasu (_překrývalo tlačítka a výsledek_)](https://trello.com/c/mWDxe3sq)
* [Zobrazení hlavičky tabulky při zvětšení písma v novém okně (_text se nevešel_)](https://trello.com/c/O7p7eu1N)
* [Kontrola aktualizací po spuštění webu na https](https://trello.com/c/BF9Dhhlc/)

## v2.1.0

### Novinky

* [Speciální výsledek zápasu (prodloužení, penalty, kontumace)](https://trello-attachments.s3.amazonaws.com/5503e9a218de67e249a60821/243x80/87ba1e3807cf88dd70e9055643f83088/Screenshot_from_2016-01-30_10_29_25.png)
* [Interaktivní vkládání gólů při editaci zápasu](https://trello-attachments.s3.amazonaws.com/56754357c3179caaee934aa6/642x164/86a7c194f11465588271d83c42d6caae/Screenshot_from_2015-12-22_16_20_14.png)

### Vylepšení

* [Doplnění informací o řazení tabulky](https://trello-attachments.s3.amazonaws.com/5621f1246f0ae411313dbffd/572x60/a9abe32413b4653df15bb97e4eb4342c/Screenshot_from_2015-12-04_17_08_10.png)
* [Vylepšení notifikace po přejmenování týmu](https://trello-attachments.s3.amazonaws.com/56754347e77b7fed128e4e04/406x26/b012065615629d62aa417cae34426ec1/Screenshot_from_2015-12-19_12_53_06.png)
* [Vylepšení chybové hlášky, pokud se nepodaří stáhnout rozpis z webu](https://trello-attachments.s3.amazonaws.com/5621f04f33a094593113444f/578x284/a719450a69701393883d9fdfa7b869ab/Screenshot_from_2015-12-19_12_41_42.png)
* [Oprava zobrazení periody v detailu zápasu](https://trello.com/c/uIir5fan)

## v2.0.0

### Novinky

* [Otevření zápasů, tabulek, statistik v novém okně](https://trello-attachments.s3.amazonaws.com/55a1513dbb7eeec37929c6d5/795x525/d4abe4a0b6064c1d252310d6e2ae4843/undecorated-modal.png)
* [Signalizace neuložených změn (zvýraznění tlačítka Uložit, dialog před ukončením aplikace)](https://trello-attachments.s3.amazonaws.com/5479c8ddd6cd310dbd1afb3f/608x334/2b11676f9372b96bb877938a2eb2e207/save-before-close.png)

### Vylepšení

* [Číslo hřiště v seznamu zápasů](https://trello-attachments.s3.amazonaws.com/55ab544a63d6b2495666003e/522x150/9ea2f7f2d0bc2ea5a7445b3000adea0e/fields.png)
* Pamatovat si filtr na seznamu zápasů, když se vracím z detailu zápasu
* [Vylepšit vybrané řádky v tabulkách (špatná čitelnost, předvybraný řádek na W8)](https://trello-attachments.s3.amazonaws.com/55ab54ee72cdf72e274157a4/258x136/85eb7a22d0e177e49176d5d8f9838442/highlighted-table-row.png)
* [Vylepšit zobrazení textu tlačítek na Linuxu](https://trello-attachments.s3.amazonaws.com/55ab54ee72cdf72e274157a4/213x162/81accbfb2b468598fa6d4165a9f8bcab/linux-text.png)

### Oprava chyb

* Nejde změnit střelce u zápasů se skóre, ale bez střelců (chyba ve verzi 1.5)

## v1.5.1

* Špatný výpočet počtu gólů jednotlivých hráčů ve statistikách (chyba ve verzi 1.5)

## v1.5.0

* [Vylepšení seznamu zápasů - zvýraznění editovaného zápasu, skrytí střelci, pořadí zápasu](https://trello-attachments.s3.amazonaws.com/5544cc2780fbd7262362e967/599x261/a672f12a5c0cbfbf54e7aa636be04a0e/matches.png)
* [Vylepšení detailu zápasu - odkaz na tým, informace o zápasu, nevyplnění střelci bez 0, oprava zarovnání](https://trello-attachments.s3.amazonaws.com/5544cafe6e6c5ce7058aa689/598x206/782894901e642c53194643ad060969df/match-detail.png)
* [Možnost řazení hráčů v týmu](https://trello-attachments.s3.amazonaws.com/5544cbd457abc37387028382/577x414/beb40f3e353250e1081b39b2315cad07/players.png)
* [Přejmenování skupiny](https://trello-attachments.s3.amazonaws.com/5479c8cbb6f327455eaa6dd8/574x524/dff4d5a1ca5084d50aa11db57d80b147/rename-groups.png)
* [Přesun aktualizací a stažení nejnovější verze do API (v rámci webu, ne v aplikaci)](https://trello-attachments.s3.amazonaws.com/558e49f63b85f31b075e191e/185x61/ebdcbecfbf2ac290685119216c36f9d1/results-api.png)

## v1.4.0

* [Nastavit počet bodů za konkrétní výsledek](https://trello-attachments.s3.amazonaws.com/54f9637ce7bee7f6ca07ed54/596x294/5e6c4c066a193ddf355749e7f9f17ac8/vysledky-body.png)

## v1.3.0

* [Nastavit počet bodů za výhru, remízu, prohru](https://trello-attachments.s3.amazonaws.com/54cce46a974f065951421d59/600x137/3811eb7c18709108a4be93e3df98f4f9/body.png)
* [V aktualizacích zobrazit i datum vydání verze](https://trello-attachments.s3.amazonaws.com/54df220644c5727e5b97de5d/368x159/e565945be4cedce89ae6851893d7941a/aktualizace.png)

## v1.2.0

* [Statistiky turnaje - nejlepší střelci, nejlepší gólmani a statistiky turnaje](https://trello-attachments.s3.amazonaws.com/54a28c8c42cc632cde8c0430/525x303/35cb1fa71bc633e7e0eb43a5667f0e5c/statistiky.png)
* [Zobrazení křížové tabulky](https://trello-attachments.s3.amazonaws.com/54a28d251782836fffc447d9/1164x241/115a67b7751c5fa435be9c6ba3321843/krizova-tabulka.png)
* [Kontrola aktualizací - seznam změn a stažení nejnovější verze](https://trello-attachments.s3.amazonaws.com/547accd6126e9ed9e972bf8a/707x431/c2ce8330ea5f80bc06eebf337385b8b4/upgrade.png)
* [Smazat tým z turnaje](https://trello-attachments.s3.amazonaws.com/54841a07772b341a48531cf8/589x113/00a9169b78b491f6f1e6d97c47e24569/smazat-tym.png)
* [Hráč se stejným jménem může být hráčem v několika týmech](https://trello-attachments.s3.amazonaws.com/54a7a6f19cb66162d27e7bcc/315x79/662abc338707deb3cde2e15e73e0cd99/stejne-jmeno-hrace.png)
* [Jeden spustitelný soubor (zabalit do JARu i GSON knihovnu)](https://trello-attachments.s3.amazonaws.com/54a80faccf0e5dcf85150237/594x90/5f920ea32e2ede2641c48be415115ec4/jar.png)
* [Při exportu do html nastavit jednotky (zda se jedná o gól/set, tým/hráče)](https://trello-attachments.s3.amazonaws.com/54a7aebacfe179ead9970274/317x384/62b06aadbd0608d037b84d5b63480277/hrac-set.png)

## v1.1.0

### Vylepšení

* Lepší hláška, pokud není načten žádný rozpis v aplikaci (neukládat chybovou hlášku)
* Podpora pro načítání playoff rozpisů
* Přejmenovat rozhodčí, pokud se změní jméno týmů a rozpis obsahuje rozhodčí z nehrajícího týmu
* Ikona aplikace

### Oprava chyb

* Odstranit null při exportu křížové tabulky do JSON
* Překlep v editaci týmu

## v1.0.0

* Vkládání výsledků zápasů i se střelci gólů, tabulka nejlepších střelců
* Správa týmů - úprava jména, editace týmových hráčů
* Filtrování zápasů podle kola, týmu, odehraných zápasů
* Týmy se stejným počtem bodů v tabulce řadí podle vzájemných zápasů
* Vytvoření souhrnu turnaje, který lze převést do HTML
* Správa několika rozpisů současně
* Načtení JSON rozpisu z webu nebo disku

