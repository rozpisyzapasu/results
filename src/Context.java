/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.out.exporter.JsonWriter;
import sportsscheduler.results.out.importer.JsonReader;
import sportsscheduler.results.overview.PresentableSchedule;
import sportsscheduler.results.stats.goalkeepers.Goalkeepers;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.stats.schedule.ScheduleStats;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.Tables;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.zone.ZoneRules;
import java.util.List;

public class Context {

    public static Schedule currentSchedule;
    public static PresentableSchedule schedule;
    public static Iterable<PresentablePlayer> players;
    public static Tables tables;
    public static Goalkeepers goalkeepers;
    public static ScheduleStats stats;

    public static ScheduleRepository repository;
    public static JsonReader jsonReader, webReader;
    public static JsonWriter jsonWriter;

    public static String defaultGroup;

    public static void loadSchedule(String name) {
        currentSchedule = repository.find(name);
    }

    public static ZoneOffset getZone(Instant date) {
        return getZone().getOffset(date);
    }

    public static ZoneOffset getZone(LocalDateTime date) {
        return getZone().getOffset(date);
    }

    private static ZoneRules getZone() {
        return ZoneId.of("Europe/Prague").getRules();
    }

    public static void reload() {
        schedule = UseCaseInvoker.get(UseCaseInvoker.SCHEDULE_OVERVIEW);
        players = UseCaseInvoker.get(UseCaseInvoker.STATS_SCORERS);
        tables = UseCaseInvoker.get(UseCaseInvoker.STATS_TABLE);
        goalkeepers = UseCaseInvoker.get(UseCaseInvoker.STATS_GOALKEEPERS);
        stats = UseCaseInvoker.get(UseCaseInvoker.STATS_SCHEDULE);
    }

    public static List<PresentableTeamResult> firstClassicTable() {
        return tables.getFirstTable().classicTable.teams;
    }

    public static boolean hasPlayers() {
        return getTeamUnit() == ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS;
    }

    public static ScheduleSettings.Units.Team getTeamUnit() {
        return currentSchedule != null ? currentSchedule.settings.teamUnit : ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS;
    }

    public static ScheduleSettings.Units.Match getMatchUnit() {
        return currentSchedule != null ? currentSchedule.settings.matchUnit : ScheduleSettings.Units.Match.GOAL;
    }
}
