/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.match.Matches;

public class Schedule {

    public String name, originalPath;
    public boolean isSaved = true;

    public ScheduleSettings settings;
    public Matches matches;
}
