/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

public interface ScheduleRepository {
    void save(Schedule schedule);

    Schedule find(String name);

    boolean areAllSchedulesSaved();
}
