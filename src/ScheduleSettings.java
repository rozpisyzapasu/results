/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.points.TablePoints;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;

public class ScheduleSettings {

    public String scheduleType;
    public int periodsCount, fieldsCount;
    public boolean hasReferees, hasMatchDate, hasDefaultUnits = true;
    public TablePoints points = TablePoints.defaultPoints();
    public Units.Team teamUnit = Units.Team.TEAM_WITH_PLAYERS;
    public Units.Match matchUnit = Units.Match.GOAL;

    public String day;
    private DateTimeFormatter dateTimeFormatter, fullDateFormatter;

    public List<String> matchPeriods = new ArrayList<>();
    public List<String> teamStatsNames = new ArrayList<>();
    public List<String> playerStatsNames = new ArrayList<>();
    public boolean hasMatchNote;

    public boolean hasAdvancedStats() {
        return teamStatsNames.size() > 0 || playerStatsNames.size() > 0;
    }

    public boolean isTeamStat(String type) {
        return teamStatsNames.contains(type) || isMatchPeriod(type);
    }

    public boolean isMatchPeriod(String type) {
        return matchPeriods.contains(type);
    }

    public boolean isPlayerStat(String type) {
        return playerStatsNames.contains(type);
    }

    public void setDay(String day) {
        this.day = day;
        fullDateFormatter = ofPattern("dd.MM.yyyy HH:mm");
        dateTimeFormatter = day == null ? fullDateFormatter : ofPattern("HH:mm");
    }

    public String formatDate(LocalDateTime date) {
        return formatDate(date, false);
    }

    public String formatDate(LocalDateTime date, boolean isFullDateRequired) {
        return date.format(isFullDateRequired ? fullDateFormatter : dateTimeFormatter);
    }

    public static class Units {
        public enum Team {
            TEAM_WITH_PLAYERS("team"),
            PLAYER("player");

            private final String jsonValue;

            Team(String t) {
                jsonValue = t;
            }

            public String toString() {
                return jsonValue;
            }
        }
        public enum Match {
            GOAL("goal"),
            SET("set");

            private final String jsonValue;

            Match(String t) {
                jsonValue = t;
            }

            public String toString() {
                return jsonValue;
            }
        }
    }
}
