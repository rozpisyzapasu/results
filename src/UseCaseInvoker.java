/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.out.exporter.ExportSchedule;
import sportsscheduler.results.out.importer.ImportSchedule;
import sportsscheduler.results.out.summary.SummarySchedule;
import sportsscheduler.results.out.upgrade.Upgrade;
import sportsscheduler.results.overview.OverviewSchedule;
import sportsscheduler.results.points.ChangeTablePoints;
import sportsscheduler.results.score.ChangeScore;
import sportsscheduler.results.settings.EditSettings;
import sportsscheduler.results.stats.goalkeepers.GoalkeepersStats;
import sportsscheduler.results.stats.players.ScorersStats;
import sportsscheduler.results.stats.schedule.CreateScheduleStats;
import sportsscheduler.results.stats.table.CreateTable;
import sportsscheduler.results.team.DeleteTeam;
import sportsscheduler.results.team.UpdateTeam;
import sportsscheduler.results.team.group.RenameGroups;
import sportsscheduler.results.team.player.RenamePlayer;

public class UseCaseInvoker {

    public static final String TEAM_UPDATE = "team/update";
    public static final String TEAM_DELETE = "team/delete";
    public static final String PLAYER_RENAME = "player/rename";
    public static final String GROUPS_RENAME = "groups/rename";
    public static final String MATCH_SCORE = "match/score";
    public static final String TABLE_POINTS = "table/points";
    public static final String SCHEDULE_IMPORT = "schedule/import";
    public static final String SCHEDULE_EXPORT = "schedule/export";
    public static final String SCHEDULE_OVERVIEW = "schedule/overview";
    public static final String APP_UPGRADE = "app/upgrade";
    public static final String EDIT_SETTINGS = "settings/edit";

    public static final String SCHEDULE_SUMMARY = "schedule/summary";
    public static final String STATS_GOALKEEPERS = "stats/goalkeepers";
    public static final String STATS_SCORERS = "stats/scorers";
    public static final String STATS_SCHEDULE = "stats/schedule";
    public static final String STATS_TABLE = "stats/table";

    public static <R, P> void execute(String usecase, R request, P presenter) {
        command(usecase).execute(request, presenter);
    }

    private static Usecase command(String name) {
        switch (name) {
            case TEAM_UPDATE: return new UpdateTeam();
            case TEAM_DELETE: return new DeleteTeam();
            case PLAYER_RENAME: return new RenamePlayer();
            case GROUPS_RENAME: return new RenameGroups();
            case MATCH_SCORE: return new ChangeScore();
            case TABLE_POINTS: return new ChangeTablePoints();
            case SCHEDULE_IMPORT: return new ImportSchedule();
            case SCHEDULE_EXPORT: return new ExportSchedule();
            case APP_UPGRADE: return new Upgrade();
            case EDIT_SETTINGS: return new EditSettings();
            default: return null;
        }
    }

    public static <P> P get(String usecase) {
        return (P) getter(usecase).execute();
    }

    private static UsecaseGetter getter(String name) {
        switch (name) {
            case SCHEDULE_OVERVIEW: return new OverviewSchedule();
            case SCHEDULE_SUMMARY: return new SummarySchedule();
            case STATS_GOALKEEPERS: return new GoalkeepersStats();
            case STATS_SCORERS: return new ScorersStats();
            case STATS_SCHEDULE: return new CreateScheduleStats();
            case STATS_TABLE: return new CreateTable();
            default: return null;
        }
    }
}
