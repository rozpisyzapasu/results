/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import java.util.ArrayList;
import java.util.List;

public class Group {
    public String name;
    private final List<Team> teams = new ArrayList<>();

    public Group(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTeam(Team t) {
        teams.add(t);
        t.group = this;
    }

    public void deleteTeam(Team t) {
        teams.remove(t);
    }

    public boolean isEmpty() {
        return teams.isEmpty();
    }
}
