/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.score.type.ExistingScore;

import java.util.ArrayList;

public class Match {

    private final ArrayList<TeamInMatch> teams;
    private final MatchAttributes attributes;
    private final int id;

    public Match(int id, Team home, Team away, Score scoreHome, MatchAttributes attributes, TablePoints points) {
        this.id = id;
        teams = new ArrayList<>();
        if (scoreHome instanceof ExistingScore) {
            ((ExistingScore) scoreHome).loadTeams(home, away);
        }
        teams.add(new TeamInMatch(home, scoreHome, points));
        teams.add(new TeamInMatch(away, scoreHome.reverse(), points));
        this.attributes = attributes;
    }

    public int getId() {
        return id;
    }

    public Score getScore() {
        return teams.get(0).getScore();
    }

    public boolean isScoreChanged(Score s) {
        TeamInMatch homeTeam = teams.get(0);
        return homeTeam.isScoreChanged(s);
    }

    public void setScore(Score score) {
        if (score instanceof ExistingScore) {
            ((ExistingScore) score).loadTeams(teams.get(0).getTeam(), teams.get(1).getTeam());
        }
        for (TeamInMatch team : teams) {
            team.updateScore(score);
            score = score.reverse();
        }
    }

    public Iterable<TeamInMatch> getTeams() {
        return teams;
    }

    public MatchAttributes getAttributes() {
        return attributes;
    }

    public boolean hasTeam(Team team) {
        for (TeamInMatch t : teams) {
            if (t.getTeam() == team) {
                return true;
            }
        }
        return false;
    }

    public boolean isPlayoffMatch() {
        for (TeamInMatch t : teams) {
            if (t.getTeam().playoffSeed == null) {
                return false;
            }
        }
        return true;
    }

    public void replaceTeam(Team favorite, Team outsider) {
        for (TeamInMatch team : teams) {
            if (team.getTeam().equals(favorite)) {
                team.replaceBy(outsider);
            }
        }
    }
}