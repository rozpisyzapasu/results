/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import sportsscheduler.results.points.TablePoints;

import java.util.*;

public class Matches {

    private final List<Match> matches = new ArrayList<>();
    private final Map<String, Group> groups = new LinkedHashMap<>();
    private final Map<String, Team> teams = new LinkedHashMap<>();

    public void addTeam(String team, String group) {
        teams.put(team, new Team(team, getGroup(group)));
    }

    public void changeTeamGroup(String team, String group) {
        Team t = getTeam(team);
        String oldGroup = t.getGroup();
        getGroup(group).addTeam(t);
        if (getGroup(oldGroup).isEmpty()) {
            groups.remove(oldGroup);
        }
    }

    private Group getGroup(String group) {
        if (!groups.containsKey(group)) {
            groups.put(group, new Group(group));
        }
        return groups.get(group);
    }

    public void renameGroups(String[] newNames) {
        Group[] entities = getGroups();
        groups.clear();
        for (int i = 0; i < entities.length; i++) {
            entities[i].setName(newNames[i]);
            groups.put(entities[i].name, entities[i]);
        }
    }

    public int countGroups() {
        return groups.size();
    }

    public Set<String> getGroupNames() {
        return groups.keySet();
    }

    public Group[] getGroups() {
        return groups.values().toArray(new Group[groups.size()]);
    }

    public Map<String, String[]> mapGroupAndTeams() {
        Map<String, List<String>> groups = new LinkedHashMap<>();
        for (String groupName : getGroupNames()) {
            groups.put(groupName, new ArrayList<>());
        }
        for (Team team : getTeams()) {
            groups.get(team.getGroup()).add(team.name);
        }
        Map<String, String[]> result = new HashMap<>();
        for (Map.Entry<String, List<String>> e : groups.entrySet()) {
            result.put(
                    e.getKey(),
                    e.getValue().toArray(new String[e.getValue().size()])
            );
        }
        return result;
    }

    public void addMatch(Match m) {
        matches.add(m);
    }

    public int countMatches() {
        return matches.size();
    }

    public Iterable<Match> getMatches() {
        return matches;
    }

    public Iterable<Match> getFutureMatches(Match m) {
        int index = matches.indexOf(m);
        return matches.subList(index + 1, matches.size());
    }

    public Match getMatch(int idMatch) {
        for (Match m : matches) {
            if (m.getId() == idMatch) {
                return m;
            }
        }
        return null;
    }

    public int countTeams() {
        return teams.size();
    }

    public Iterable<Team> getTeams() {
        return teams.values();
    }

    public boolean hasTeam(String team) {
        return teams.containsKey(team);
    }

    public Team getTeam(String team) {
        return teams.get(team);
    }

    public void renameTeam(String team, String newName) {
        Team renamedTeam = teams.remove(team);
        renamedTeam.setName(newName);
        teams.put(newName, renamedTeam);
        renameReferees(team, newName);
    }

    private void renameReferees(String team, String newName) {
        for (Match m : matches) {
            MatchAttributes attributes = m.getAttributes();
            if (attributes.referee.equals(team)) {
                attributes.referee = newName;
            }
        }
    }

    public Team deleteTeam(String team) {
        Team deletedTeam = teams.remove(team);
        deletedTeam.group.deleteTeam(deletedTeam);
        Iterator<Match> iterator = matches.iterator();
        while (iterator.hasNext()) {
            Match m = iterator.next();
            if (m.hasTeam(deletedTeam)) {
                iterator.remove();
            }
        }
        return deletedTeam;
    }

    public void recalculatePoints(TablePoints points) {
        for (Match m : matches) {
            for (TeamInMatch t : m.getTeams()) {
                t.recalculatePoints(points);
            }
        }
    }
}
