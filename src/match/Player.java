/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import java.util.LinkedHashMap;
import java.util.Map;

public class Player {
    public String name;
    public int goalsCount;
    private Team team;
    public final Stats advancedStats = new Stats();

    public Player(String name, Team team) {
        this.name = name;
        this.team = team;
    }

    public String getTeam() {
        return team.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
