/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

public enum ResultType {
    NORMAL("Normální výsledek", ""),
    EXTRA_TIME("Po prodloužení", "pr."),
    SHOOTOUT("Po penaltách", "np."),
    WIN_BY_DEFAULT("Kontumace", "k.");

    private final String translation, abbrevation;

    ResultType(String t, String a) {
        translation = t;
        abbrevation = a;
    }

    public String toString() {
        return translation;
    }

    public String getAbbrevation() {
        return abbrevation;
    }

    public static ResultType parse(String value) {
        try {
            return ResultType.valueOf(value);
        } catch (Exception e) {
            return ResultType.NORMAL;
        }
    }
}
