/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import java.util.LinkedHashMap;
import java.util.Map;

public class Results {

    private final Map<ResultType, Integer> counts = new LinkedHashMap<>();

    public void update(ResultType resultType, int count) {
        counts.put(resultType, count(resultType) + count);
    }

    public int count() {
        int count = 0;
        for (Integer resultCount : counts.values()) {
            count += resultCount;
        }
        return count;
    }

    public int countNormal() {
        return count(ResultType.NORMAL) + count(ResultType.WIN_BY_DEFAULT);
    }

    public int countExtra() {
        return count(ResultType.EXTRA_TIME) + count(ResultType.SHOOTOUT);
    }

    public int count(ResultType resultType) {
        return counts.containsKey(resultType) ? counts.get(resultType) : 0;
    }
}
