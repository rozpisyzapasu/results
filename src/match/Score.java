/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

public interface Score {

    boolean isSet();

    ResultType getResultType();

    boolean isValid();

    boolean isDifferent(Score s);

    Score reverse();

    int countGoals();
}
