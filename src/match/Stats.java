/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Stats {
    private final Map<String, Integer> stats = new LinkedHashMap<>();

    public void put(Map.Entry<String, Integer> entry) {
        put(entry.getKey(), entry.getValue());
    }

    public void put(String type, int value) {
        stats.put(type, value);
    }

    public void update(Stats thatStats, int one) {
        for (Map.Entry<String, Integer> stat : thatStats.iterate()) {
            put(stat.getKey(), get(stat.getKey()) + stat.getValue() * one);
        }
    }

    public Stats getActive() {
        Stats stats = new Stats();
        for (Map.Entry<String, Integer> entry : this.stats.entrySet()) {
            if (entry.getValue() > 0) {
                stats.put(entry);
            }
        }
        return stats;
    }

    public Set<Map.Entry<String, Integer>> iterate() {
        return stats.entrySet();
    }

    public Integer get(String key) {
        return stats.getOrDefault(key, 0);
    }

    public Iterator<Integer> iterateValues() {
        return stats.values().iterator();
    }

    public boolean isDifferent(Stats thatStats) {
        return !this.stats.toString().equals(thatStats.stats.toString());
    }
}
