/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import java.util.*;

public class Team {

    public String name;
    public Group group;
    public int drawsCount, goalsFor, goalsAgainst, points;
    private final Map<String, Player> players = new LinkedHashMap<>();
    public final Results wins = new Results(), losses = new Results();
    public Integer playoffSeed;

    public final Stats advancedStats = new Stats();

    public static Team test(String name, String group) {
        return new Team(name, new Group(group));
    }

    public Team(String name, Group group) {
        this.name = name;
        this.group = group;
    }

    public String getGroup() {
        return group.name;
    }

    public Team clone() {
        return new Team(name, group);
    }

    public int countMatches() {
        return wins.count() + drawsCount + losses.count();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int countPlayers() {
        return players.size();
    }

    public void addPlayer(String player) {
        if (!hasPlayer(player)) {
            players.put(player, new Player(player, this));
        }
    }

    public boolean hasPlayer(String player) {
        return players.containsKey(player);
    }

    public Player getPlayer(String name) {
        return players.get(name);
    }

    public void deletePlayer(String player) {
        players.remove(player);
    }

    public void renamePlayer(String player, String newName) {
        Player renamedPlayer = players.remove(player);
        renamedPlayer.setName(newName);
        players.put(newName, renamedPlayer);
    }

    public void sortPlayers(Comparator<Map.Entry<String, Player>> comparator) {
        List<Map.Entry<String, Player>> list = new LinkedList(players.entrySet());
        Collections.sort(list, comparator);
        players.clear();
        for (Map.Entry<String, Player> entry : list) {
            players.put(entry.getKey(), entry.getValue());
        }
    }

    public Iterable<Player> getPlayers() {
        return players.values();
    }
}
