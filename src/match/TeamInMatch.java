/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.score.type.MatchResultEvaluator;

public class TeamInMatch {

    private Team team;
    private Score score;
    private MatchResultEvaluator evaluator;

    public TeamInMatch(Team team, Score score, TablePoints p) {
        this.team = team;
        this.score = score;
        evaluator = new MatchResultEvaluator(p);
        initPoints(team);
    }

    public void replaceBy(Team t) {
        evaluator.subtract(team, score);
        evaluator.add(t, score);
        team = t;
    }

    public boolean isScoreChanged(Score s) {
        return score.isDifferent(s);
    }

    public void initPoints(Team t) {
        evaluator.add(t, score);
    }

    public void updateScore(Score s) {
        evaluator.subtract(team, score);
        score = s;
        evaluator.add(team, score);
    }

    public void recalculatePoints(TablePoints p) {
        evaluator.subtract(team, score);
        evaluator = new MatchResultEvaluator(p);
        evaluator.add(team, score);
    }

    public Team getTeam() {
        return team;
    }

    public Score getScore() {
        return score;
    }
}
