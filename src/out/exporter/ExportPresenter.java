/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.exporter;

import sportsscheduler.results.out.exporter.JsonWriter.WriteResult;

public interface ExportPresenter {
    void scheduleNotSaved(WriteResult result);
    void scheduleExported();
}
