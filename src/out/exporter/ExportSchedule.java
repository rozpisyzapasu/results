/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.exporter;

import org.easygson.JsonEntity;
import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.out.exporter.JsonWriter.WriteResult;

public class ExportSchedule implements Usecase<String, ExportPresenter> {

    private final ScheduleToJson scheduleToJson = new ScheduleToJson();

    public void execute(String path, ExportPresenter presenter) {
        Schedule schedule = Context.currentSchedule;
        schedule.originalPath = path;
        JsonEntity json = scheduleToJson.convert(schedule);
        WriteResult result = Context.jsonWriter.write(path, json);
        schedule.isSaved = result == WriteResult.OK;
        if (schedule.isSaved) {
            presenter.scheduleExported();
        } else {
            presenter.scheduleNotSaved(result);
        }
    }
}
