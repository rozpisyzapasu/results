/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.exporter;

import org.easygson.JsonEntity;

public interface JsonWriter {
    WriteResult write(String path, JsonEntity json);

    enum WriteResult {
        OK,
        PATH_IS_WEBLINK,
        IO_ERROR
    }
}
