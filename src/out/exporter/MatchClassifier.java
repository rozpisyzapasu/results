/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.exporter;

import sportsscheduler.results.match.Match;

import java.util.*;
import java.util.stream.Collectors;

class MatchClassifier {

    public List<List<List<Match>>> separatePeriodsAndRounds(Iterable<Match> allMatches) {
        Map<Integer, Map<Integer, List<Match>>> periods = new HashMap<>();
        for (Match match : allMatches) {
            int period = match.getAttributes().period;
            if (!periods.containsKey(period)) {
                periods.put(period, new HashMap<>());
            }

            Map<Integer, List<Match>> periodRounds = periods.get(period);
            int round = match.getAttributes().round;
            if (!periodRounds.containsKey(round)) {
                periodRounds.put(round, new ArrayList<>());
            }

            periodRounds.get(round).add(match);
        }
        return convertMapsToLists(periods);
    }

    private List<List<List<Match>>> convertMapsToLists(Map<Integer, Map<Integer, List<Match>>> periods) {
        List<List<List<Match>>> result = new ArrayList<>();
        for (Map<Integer, List<Match>> period : periods.values()) {
            List<List<Match>> p = period.values().stream().collect(Collectors.toList());
            result.add(p);
        }
        return result;
    }
}
