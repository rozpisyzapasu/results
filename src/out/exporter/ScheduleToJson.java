/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.exporter;

import org.easygson.JsonEntity;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.out.system.Dates;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.match.*;
import sportsscheduler.results.score.type.ExistingScore;
import sportsscheduler.results.score.type.Scorer;

import java.util.List;
import java.util.Map;

import static org.easygson.JsonEntity.emptyArray;
import static org.easygson.JsonEntity.emptyObject;

public class ScheduleToJson {

    private final MatchClassifier classifier = new MatchClassifier();

    public JsonEntity convert(Schedule schedule) {
        JsonEntity json = emptyObject();
        json.create("description", descriptionToJson(schedule));
        json.create("schedule", infoToJson(schedule));
        json.create("players", playersToJson(schedule.matches.getTeams()));
        json.create("periods", matchesToJson(schedule.matches.getMatches(), schedule.settings));
        return json;
    }

    private JsonEntity descriptionToJson(Schedule schedule) {
        JsonEntity s = emptyObject();
        s.create("scheduleName", schedule.name);
        return s;
    }

    private JsonEntity infoToJson(Schedule schedule) {
        JsonEntity s = emptyObject();
        s.create("scheduleType", schedule.settings.scheduleType);
        groupsToJson(schedule.matches.mapGroupAndTeams(), s);
        s.create("periodsCount", schedule.settings.periodsCount);
        s.create("fields", emptyObject().create("count", schedule.settings.fieldsCount));
        if (schedule.settings.hasMatchDate) {
            s.create("duration", emptyObject());
        }
        if (schedule.settings.hasReferees) {
            s.create("referees", emptyObject());
        }
        JsonEntity p = pointsToJson(schedule.settings.points);
        p.create("scores", matchScoresToJson(schedule.settings.points));
        s.create("points", p);
        s.create("units", unitsToJson(schedule.settings));
        s.create("stats", statsToJson(schedule.settings));
        return s;
    }

    private JsonEntity pointsToJson(TablePoints points) {
        JsonEntity p = emptyObject();
        p.create("win", points.win);
        p.create("draw", points.draw);
        p.create("loss", points.loss);
        return p;
    }

    private JsonEntity unitsToJson(ScheduleSettings s) {
        JsonEntity p = emptyObject();
        p.create("team", s.teamUnit.toString());
        p.create("match", s.matchUnit.toString());
        return p;
    }

    private JsonEntity statsToJson(ScheduleSettings settings) {
        JsonEntity stats = emptyObject();
        stats.create("matchPeriods", listToArray(settings.matchPeriods));
        stats.create("team", listToArray(settings.teamStatsNames));
        stats.create("player", listToArray(settings.playerStatsNames));
        return stats;
    }

    private JsonEntity listToArray(List<String> matchPeriods) {
        JsonEntity a = emptyArray();
        for (String period : matchPeriods) {
            a.create(period);
        }
        return a;
    }

    private JsonEntity matchScoresToJson(TablePoints points) {
        JsonEntity scores = emptyArray();
        for (TablePoints.MatchScorePoints score : points.matchScores) {
            JsonEntity m = pointsToJson(score.points);
            m.create("goals", emptyArray()
                    .create(score.goals[0])
                    .create(score.goals[1]));
            scores.create(m);
        }
        for (Map.Entry<ResultType, TablePoints> score : points.resultScores.entrySet()) {
            JsonEntity m = pointsToJson(score.getValue());
            m.create("resultType", score.getKey().name());
            scores.create(m);
        }
        return scores;
    }

    private void groupsToJson(Map<String, String[]> scheduleGroups, JsonEntity schedule) {
        if (scheduleGroups.size() == 1) {
            String[] firstGroup = scheduleGroups.values().iterator().next();
            schedule.create("teams", teamsToJson(firstGroup));
        } else {
            JsonEntity groups = JsonEntity.emptyObject();
            for (Map.Entry<String, String[]> e : scheduleGroups.entrySet()) {
                groups.create(e.getKey(), teamsToJson(e.getValue()));
            }
            schedule.create("groups", groups);
        }
    }

    private JsonEntity teamsToJson(String[] group) {
        JsonEntity teams = JsonEntity.emptyArray();
        for (String team : group) {
            teams.create(team);
        }
        return teams;
    }

    private JsonEntity playersToJson(Iterable<Team> teams) {
        JsonEntity players = emptyObject();
        for (Team team : teams) {
            JsonEntity t = emptyArray();
            for (Player player : team.getPlayers()) {
                t.create(player.name);
            }
            players.create(team.name, t);
        }
        return players;
    }

    private JsonEntity matchesToJson(Iterable<Match> allMatches, ScheduleSettings settings) {
        JsonEntity periods = emptyArray();
        for (List<List<Match>> period : classifier.separatePeriodsAndRounds(allMatches)) {
            JsonEntity p = emptyArray();
            for (List<Match> round : period) {
                p.create(roundToJson(round, settings));
            }
            periods.create(p);
        }
        return periods;
    }

    private JsonEntity roundToJson(List<Match> round, ScheduleSettings settings) {
        JsonEntity json = emptyArray();
        for (Match m : round) {
            json.create(matchToJson(m, settings));
        }
        return emptyObject().create("matches", json).parent();
    }

    private JsonEntity matchToJson(Match match, ScheduleSettings settings) {
        JsonEntity json = emptyObject();
        json.create("teams", teamsFromMatchToJson(match));
        if (match.isPlayoffMatch()) {
            json.create("seed", seedFromMatch(match));
        }
        MatchAttributes attributes = match.getAttributes();
        if (attributes.date != null) {
            json.create("timestamp", Dates.dateToTimestamp(attributes.date));
        }
        if (!attributes.referee.isEmpty()) {
            json.create("referee", attributes.referee);
        }
        Score score = match.getScore();
        if (score.isSet()) {
            json.create("result", resultToJson((ExistingScore) score));
        }
        attributesToJson(attributes, settings, match.getScore(), json);
        return json;
    }

    private JsonEntity seedFromMatch(Match m) {
        JsonEntity stats = emptyArray();
        for (TeamInMatch t : m.getTeams()) {
            stats.create(t.getTeam().playoffSeed);
        }
        return stats;
    }

    public static void attributesToJson(MatchAttributes attributes, ScheduleSettings settings, Score s, JsonEntity json) {
        JsonEntity stats = emptyArray();
        if (!attributes.note.isEmpty()) {
            JsonEntity note = emptyObject();
            note.create("type", "note");
            note.create("match", attributes.note);
            stats.create(note);
        }
        if (s.isSet()) {
            ExistingScore score = (ExistingScore) s;
            for (Map.Entry<String, Map.Entry<Integer, Integer>> stat : score.getTeamStats().entrySet()) {
                if (settings.isTeamStat(stat.getKey())) {
                    JsonEntity team = emptyObject();
                    team.create("type", stat.getKey());
                    team.create("teams", emptyArray()
                            .create(stat.getValue().getKey())
                            .create(stat.getValue().getValue()));
                    stats.create(team);
                }
            }
            for (Scorer p : score.getScorersFromAllTeams()) {
                for (Map.Entry<String, Integer> stat : p.advancedStats.iterate()) {
                    if (settings.isPlayerStat(stat.getKey())) {
                        JsonEntity o = emptyObject();
                        o.create("type", stat.getKey());
                        o.create("player", p.getName());
                        o.create("team", p.getTeam());
                        o.create("value", stat.getValue());
                        stats.create(o);
                    }
                }
            }
        }
        if (stats.arraySize() > 0) {
            json.create("stats", stats);
        }
    }

    private JsonEntity teamsFromMatchToJson(Match match) {
        JsonEntity json = emptyArray();
        for (TeamInMatch t : match.getTeams()) {
            json.create(t.getTeam().name);
        }
        return json;
    }

    private JsonEntity resultToJson(ExistingScore score) {
        JsonEntity json = emptyObject();
        json.create("type", score.getResultType().name());
        json.create("goals", goalsToJson(score));
        json.create("scorers", scorersToJson(score));
        return json;
    }

    private JsonEntity goalsToJson(ExistingScore score) {
        return emptyArray()
                .create(score.getGoalsFor())
                .create(score.getGoalsAgainst());
    }

    private JsonEntity scorersToJson(ExistingScore score) {
        JsonEntity scorers = emptyArray();
        for (Scorer s : score.getScorersFromAllTeams()) {
            JsonEntity o = emptyObject();
            o.create("player", s.getName());
            o.create("team", s.getTeam());
            o.create("goals", s.goalsCount);
            scorers.create(o);
        }
        return scorers;
    }
}
