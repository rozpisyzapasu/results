/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer;

import sportsscheduler.results.out.system.JsonResult.JsonError;

public interface ImportPresenter {
    void nonExistentSchedule(JsonError error);

    void scheduleImported(String scheduleName);
}
