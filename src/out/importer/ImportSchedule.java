/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer;

import org.easygson.JsonEntity;
import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.out.importer.build.MatchesBuilder;
import sportsscheduler.results.out.system.JsonResult;
import sportsscheduler.results.out.system.JsonResult.JsonError;

import java.util.Map;

public class ImportSchedule implements Usecase<String, ImportPresenter> {

    private JsonToMatches matches = new JsonToMatches();
    private JsonToSettings settings = new JsonToSettings();

    public void execute(String path, ImportPresenter presenter) {
        JsonResult input = Context.jsonReader.read(path);
        if (input.containsValidSchedule()) {
            JsonEntity json = input.json;
            Schedule s = new Schedule();
            s.name = settings.loadName(json);
            if (Context.repository.find(s.name) instanceof Schedule) {
                presenter.nonExistentSchedule(JsonError.ALREADY_IMPORTED);
                return;
            }
            s.originalPath = path;
            s.settings = settings.load(json);

            MatchesBuilder builder = getBuilder(s.settings, settings.loadGroups(json));
            s.matches = matches.load(builder, json, s.settings);
            s.settings.setDay(builder.getDay());

            Context.repository.save(s);
            presenter.scheduleImported(s.name);
        } else {
            presenter.nonExistentSchedule(input.error);
        }

    }

    private MatchesBuilder getBuilder(ScheduleSettings settings, Map<String, String[]> groups) {
        MatchesBuilder builder = new MatchesBuilder(settings.points);
        for (Map.Entry<String, String[]> group : groups.entrySet()) {
            for (String team : group.getValue()) {
                builder.addTeam(team, group.getKey());
            }
        }
        return builder;
    }
}
