/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer;

import sportsscheduler.results.out.system.JsonResult;

public interface JsonReader {
    JsonResult read(String path);
}
