/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.easygson.JsonEntity;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.out.importer.build.MatchesBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static sportsscheduler.results.ScheduleSettings.Units.Match.SET;

class JsonToMatches {

    private MatchesBuilder builder;
    private ScheduleSettings settings;
    private int currentPeriod, currentRound, currentField;

    public Matches load(MatchesBuilder b, JsonEntity json, ScheduleSettings s) {
        builder = b;
        settings = s;
        loadPlayers(json.getSafely("players"));
        loadMatches(json.getSafely("periods"));
        return b.build();
    }

    private void loadPlayers(JsonEntity playersObject) {
        if (playersObject.isObject()) {
            List<String> tempPlayer = new ArrayList<>();
            for (Map.Entry<String, JsonElement> e : playersObject.raw().getAsJsonObject().entrySet()) {
                for (JsonElement player : e.getValue().getAsJsonArray()) {
                    tempPlayer.add(player.getAsString());
                }
                builder.players(e.getKey(), tempPlayer.toArray(new String[tempPlayer.size()]));
                tempPlayer.clear();
            }
        }
    }

    private void loadMatches(JsonEntity periods) {
        currentPeriod = 1;
        currentRound = 1;
        for (JsonEntity period : periods) {
            for (JsonEntity round : period) {
                currentField = 1;
                for (JsonEntity match : round.getSafely("matches")) {
                    loadMatch(match);
                    currentField++;
                }
                currentRound++;
            }
            currentPeriod++;
        }
    }

    private void loadMatch(JsonEntity match) {
        JsonEntity teamsInMatch = match.getSafely("teams");
        if (hasTwoTeams(teamsInMatch)) {
            MatchBuilder b = MatchBuilder.match(
                teamsInMatch.get(0).asString(),
                teamsInMatch.get(1).asString()
            );
            b.period(currentPeriod).round(currentRound).field(currentField);
            if (hasAttribute(match, "result")) {
                loadResult(match, b);
            }
            if (hasAttribute(match, "referee")) {
                b.referee(match.get("referee").asString());
            }
            if (hasAttribute(match, "timestamp")) {
                b.date(match.asString("timestamp"));
            }
            if (hasAttribute(match, "seed")) {
                JsonEntity seedsInMatch = match.getSafely("seed");
                b.seed(
                    seedsInMatch.get(0).asInt(),
                    seedsInMatch.get(1).asInt()
                );
            }
            if (hasAttribute(match, "stats")) {
                JsonEntity stats = match.getSafely("stats");
                for (JsonEntity stat : stats) {
                    String type = stat.getSafely("type").asString();
                    if (type.equals("note")) {
                        b.note(stat.getSafely("match").asString());
                    } else if (stat.getSafely("teams").isArray() && settings.isTeamStat(type)) {
                        JsonEntity teams = stat.getSafely("teams");
                        b.teamStat(
                                type,
                                teams.get(0).asInt(),
                                teams.get(1).asInt()
                        );
                    } else if (stat.getSafely("player").isPrimitive() && settings.isPlayerStat(type)) {
                        b.playerStat(
                                stat.getSafely("player").asString(),
                                stat.getSafely("team").asString(),
                                type,
                                stat.getSafely("value").asInt()
                        );
                    }
                }
            }
            builder.match(b);
        }
    }

    private boolean hasTwoTeams(JsonEntity teamsInMatch) {
        int teamsCount = 0;
        if (teamsInMatch.isArray()) {
            for (JsonEntity t : teamsInMatch) {
                if (!t.asString().isEmpty()) {
                    teamsCount++;
                }
            }
        }
        return teamsCount == 2;
    }

    private void loadResult(JsonEntity match, MatchBuilder b) {
        JsonEntity result = match.get("result");
        if (hasAttribute(result, "type")) {
            ResultType type = ResultType.parse(result.get("type").asString());
            b.result(type);
        }
        if (hasAttribute(result, "goals")) {
            JsonEntity goals = result.get("goals");
            b.score(goals.get(0).asInt(), goals.get(1).asInt());
            loadScorers(result, b);
        }
    }

    private void loadScorers(JsonEntity result, MatchBuilder b) {
        JsonElement jsonScorers = result.get("scorers").raw();
        if (jsonScorers.isJsonObject()) {
            for (Map.Entry<String, JsonElement> e : jsonScorers.getAsJsonObject().entrySet()) {
                b.scorer(e.getKey(), e.getValue().getAsInt());
            }
        } else if (jsonScorers.isJsonArray()) {
            for (JsonElement e : jsonScorers.getAsJsonArray()) {
                JsonObject o = e.getAsJsonObject();
                b.scorer(
                        o.get("player").getAsString(),
                        o.get("goals").getAsInt(),
                        o.get("team").getAsString()
                );
            }
        }
    }

    private boolean hasAttribute(JsonEntity json, String attribute) {
        return !json.getSafely(attribute).isNull();
    }
}
