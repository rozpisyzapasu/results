/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer;

import com.google.gson.JsonElement;
import org.easygson.JsonEntity;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.points.TablePoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static sportsscheduler.results.points.TablePoints.*;

class JsonToSettings {

    public String loadName(JsonEntity json) {
        JsonEntity description = json.getSafely("description");
        return description.get("scheduleName").asString();
    }

    public ScheduleSettings load(JsonEntity json) {
        JsonEntity schedule = json.getSafely("schedule");
        ScheduleSettings s = new ScheduleSettings();
        s.scheduleType = schedule.get("scheduleType").asString();
        s.periodsCount = json.getSafely("periods").arraySize();
        s.fieldsCount = loadFieldsCount(schedule);
        s.hasReferees = hasAttribute(schedule, "referees");
        s.hasMatchDate = hasAttribute(schedule, "duration");
        s.points = loadPoints(schedule.getSafely("points"));
        loadUnits(s, schedule.getSafely("units"));
        loadStats(s, schedule.getSafely("stats"));
        return s;
    }

    private int loadFieldsCount(JsonEntity schedule) {
        JsonEntity fields = schedule.getSafely("fields");
        if (fields.isObject()) {
            JsonEntity count = fields.getSafely("count");
            if (count.isPrimitive()) {
                return count.asInt();
            }
        }
        return 0;
    }

    public Map<String, String[]> loadGroups(JsonEntity json) {
        JsonEntity schedule = json.getSafely("schedule");
        List<String> teams = new ArrayList<>();
        Map<String, String[]> groups = new HashMap<>();
        if (hasAttribute(schedule, "teams")) {
            teams.clear();
            for (JsonEntity team : schedule.get("teams")) {
                teams.add(team.asString());
            }
            groups.put(Context.defaultGroup, teams.toArray(new String[teams.size()]));
        } else if (hasAttribute(schedule, "groups")) {
            JsonEntity groupsElement = schedule.get("groups");
            for (Map.Entry<String, JsonElement> e : groupsElement.raw().getAsJsonObject().entrySet()) {
                teams.clear();
                for (JsonElement team : e.getValue().getAsJsonArray()) {
                    teams.add(team.getAsString());
                }
                groups.put(e.getKey(), teams.toArray(new String[teams.size()]));
            }
        }
        return groups;
    }

    private TablePoints loadPoints(JsonEntity points) {
        if (points.isObject()) {
            TablePoints p = createPoints(points);
            loadMatchPoint(p, points);
            if (p.areValid()) {
                return p;
            }
        }
        return defaultPoints();
    }

    private void loadMatchPoint(TablePoints points, JsonEntity json) {
        JsonEntity scores = json.getSafely("scores");
        if (scores.isArray()) {
            for (JsonEntity score : scores) {
                JsonEntity goals = score.getSafely("goals");
                if (goals.isArray()) {
                    MatchScorePoints p = new MatchScorePoints(goals.get(0).asInt(), goals.get(1).asInt());
                    loadPoints(p.points, score);
                    points.addMatchScore(p);
                }
                JsonEntity resultType = score.getSafely("resultType");
                if (resultType.isPrimitive()) {
                    points.addResultScore(ResultType.parse(resultType.asString()), createPoints(score));
                }
            }
        }
    }

    private TablePoints createPoints(JsonEntity json) {
        TablePoints p = TablePoints.defaultPoints();
        loadPoints(p, json);
        return p;
    }

    private void loadPoints(TablePoints p, JsonEntity json) {
        p.win = json.asInt("win");
        p.draw = json.asInt("draw");
        p.loss = json.asInt("loss");
    }

    private void loadUnits(ScheduleSettings s, JsonEntity units) {
        s.hasDefaultUnits = !units.isObject();
        if (!s.hasDefaultUnits) {
            s.teamUnit = units.getSafely("team").asString().equals(ScheduleSettings.Units.Team.PLAYER.toString())
                    ? ScheduleSettings.Units.Team.PLAYER : ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS;
            s.matchUnit = units.getSafely("match").asString().equals(ScheduleSettings.Units.Match.SET.toString())
                    ? ScheduleSettings.Units.Match.SET : ScheduleSettings.Units.Match.GOAL;
        }
    }

    private void loadStats(ScheduleSettings s, JsonEntity stats) {
        if (stats.isObject()) {
            s.matchPeriods = arrayToList(stats.getSafely("matchPeriods"));
            s.teamStatsNames = arrayToList(stats.getSafely("team"));
            s.playerStatsNames = arrayToList(stats.getSafely("player"));
        }
    }

    private List<String> arrayToList(JsonEntity json) {
        List<String> periods = new ArrayList<>();
        if (json.isArray()) {
            for (JsonEntity jsonPeriod : json) {
                periods.add(jsonPeriod.asString());
            }
        }
        return periods;
    }

    private boolean hasAttribute(JsonEntity json, String attribute) {
        return !json.getSafely(attribute).isNull();
    }
}
