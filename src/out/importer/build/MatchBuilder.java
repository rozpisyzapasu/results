/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer.build;

import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.out.system.Dates;
import sportsscheduler.results.match.MatchAttributes;
import sportsscheduler.results.score.MatchPlayers;

public class MatchBuilder {

    public final String home, away;

    public Integer scoreHome, scoreAway, playoffSeedHome, playoffSeedAway;
    public MatchPlayers players = new MatchPlayers();
    public final MatchAttributes attributes;
    public ResultType resultType = ResultType.NORMAL;
    public final Stats statsHome = new Stats(), statsAway = new Stats();

    public static MatchBuilder match(String home, String away) {
        return new MatchBuilder(home, away);
    }

    private MatchBuilder(String home, String away) {
        this.home = home;
        this.away = away;
        attributes = new MatchAttributes();
    }

    public MatchBuilder score(int home, int away) {
        scoreHome = home;
        scoreAway = away;
        return this;
    }

    public MatchBuilder seed(int home, int away) {
        playoffSeedHome = home;
        playoffSeedAway = away;
        return this;
    }

    public MatchBuilder scorer(String player, int goalsCount) {
        return scorer(player, goalsCount, null);
    }

    public MatchBuilder scorer(String player, int goalsCount, String team) {
        players.addGoals(player, team, goalsCount);
        return this;
    }

    public MatchBuilder playerStat(String player, String team, String type, int value) {
        players.addStat(player, team, type, value);
        return this;
    }

    public MatchBuilder result(ResultType type) {
        resultType = type;
        return this;
    }

    public MatchBuilder period(int period) {
        attributes.period = period;
        return this;
    }

    public MatchBuilder round(int round) {
        attributes.round = round;
        return this;
    }

    public MatchBuilder field(int field) {
        attributes.field = field;
        return this;
    }

    public MatchBuilder referee(String referee) {
        attributes.referee = referee;
        return this;
    }

    public MatchBuilder note(String note) {
        attributes.note = note;
        return this;
    }

    public MatchBuilder teamStat(String type, int home, int away) {
        statsHome.put(type, home);
        statsAway.put(type, away);
        return this;
    }

    public MatchBuilder date(String timestamp) {
        attributes.date = Dates.timestampToDate(timestamp);
        return this;
    }
}
