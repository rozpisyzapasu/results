/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.importer.build;

import sportsscheduler.results.match.Match;
import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.Team;
import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.match.Score;
import sportsscheduler.results.score.type.ScoreFactory;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class MatchesBuilder {

    private final Matches matches;
    private final TablePoints points;
    private final Set<String> days = new HashSet<>();
    private int matchesCount = 0;

    public MatchesBuilder(TablePoints points) {
        matches = new Matches();
        this.points = points;
    }

    public void addTeam(String team, String group) {
        matches.addTeam(team, group);
    }

    public MatchesBuilder players(String team, String[] players) {
        Team t = matches.getTeam(team);
        for (String player : players) {
            t.addPlayer(player);
        }
        return this;
    }

    public MatchesBuilder match(String home, String away) {
        MatchBuilder b = MatchBuilder.match(home, away);
        return match(b);
    }

    public MatchesBuilder playedMatch(String home, String away, int scoreHome, int scoreAway) {
        MatchBuilder b = MatchBuilder.match(home, away).score(scoreHome, scoreAway);
        return match(b);
    }

    public MatchesBuilder match(MatchBuilder b) {
        Team h = matches.getTeam(b.home);
        Team a = matches.getTeam(b.away);
        h.playoffSeed = b.playoffSeedHome;
        a.playoffSeed = b.playoffSeedAway;

        Score s =  ScoreFactory.buildScore(
                b.resultType, b.scoreHome, b.scoreAway, b.players,
                b.statsHome, b.statsAway, new String[] { b.home, b.away }
        );

        matches.addMatch(new Match(matchesCount++, h, a, s, b.attributes, points));
        if (b.attributes.date != null) {
            days.add(b.attributes.date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        }
        return this;
    }

    public Matches build() {
        return matches;
    }

    public String getDay() {
        return days.size() == 1 ? days.iterator().next() : null;
    }
}
