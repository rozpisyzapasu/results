/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.summary;

import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.stats.table.Tables;

import java.util.List;
import java.util.Map;

public class SummaryResponse {

    public String scheduleType;
    public Map<String, String> teams;

    public int groupsCount, teamsCount, fieldsCount, playersCount, matchesCount, goalsCount;
    public double averageGoalsInMatch;

    public Iterable<PresentableMatch> matches;
    public Map<String, List<PresentablePlayer>> players;
    public Tables tables;

    public Iterable<String> topTeams;
    public Map<Integer, List<PresentablePlayer>> topScorers;
    public Iterable<String> topGoalkeepers;
    public int goalkeeperMinGoalsCount;

    public String json;
}
