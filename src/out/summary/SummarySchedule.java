/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.summary;

import sportsscheduler.results.Context;
import sportsscheduler.results.UsecaseGetter;
import sportsscheduler.results.overview.PresentableSchedule;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.stats.schedule.ScheduleStats;
import sportsscheduler.results.stats.table.PresentableTeamResult;

import java.util.*;
import java.util.stream.Collectors;

public class SummarySchedule implements UsecaseGetter<SummaryResponse> {

    private SummaryResponse response;
    private SummaryToJson summaryToJson = new SummaryToJson();

    public SummaryResponse execute() {
        response = new SummaryResponse();

        ScheduleStats stats = Context.stats;
        response.groupsCount = stats.groupsCount;
        response.teamsCount = stats.teamsCount;
        response.fieldsCount = stats.fieldsCount;
        response.matchesCount = stats.matchesCount;
        response.averageGoalsInMatch = stats.averageGoalsInMatch;
        response.playersCount = stats.playersCount;
        response.goalsCount = stats.goalsCount;

        PresentableSchedule s = Context.schedule;
        response.scheduleType = s.settings.scheduleType;
        response.teams = convertTeams(s.groups);
        response.matches = s.matches;

        table();
        players();

        response.goalkeeperMinGoalsCount = Context.goalkeepers.getMinGoalsCount();
        response.topGoalkeepers = Context.goalkeepers.getTop();

        response.json = summaryToJson.convert(response);
        return response;
    }

    private Map<String, String> convertTeams(Map<String, String[]> groups) {
        Map<String, String> map = new LinkedHashMap<>();
        for (Map.Entry<String, String[]> e : groups.entrySet()) {
            map.put(e.getKey(), Arrays.asList(e.getValue()).stream().collect(Collectors.joining(", ")));
        }
        return map;
    }

    private void table() {
        response.tables = Context.tables;

        List<String> winners = new ArrayList<>();
        for (PresentableTeamResult result : response.tables.fullTable.teams) {
            winners.add(result.team);
            if (winners.size() == 3) {
                break;
            }
        }
        response.topTeams = winners;
    }

    private void players() {
        Map<String, List<PresentablePlayer>> map = new LinkedHashMap<>();
        for (PresentablePlayer player : Context.players) {
            if (!map.containsKey(player.team)) {
                map.put(player.team, new ArrayList<>());
            }
            map.get(player.team).add(player);
        }
        response.players = map;

        int lastGoalsCount = -1;
        int scorersCount = 0;
        Map<Integer, List<PresentablePlayer>> winners = new LinkedHashMap<>();
        for (PresentablePlayer player : Context.players) {
            if (scorersCount > 3 && lastGoalsCount != player.goalsCount) {
                break;
            }
            if (!winners.containsKey(player.goalsCount)) {
                winners.put(player.goalsCount, new ArrayList<>());
            }
            winners.get(player.goalsCount).add(player);
            scorersCount++;
            lastGoalsCount = player.goalsCount;
        }
        response.topScorers = winners;
    }
}
