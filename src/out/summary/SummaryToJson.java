/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.summary;

import com.google.gson.GsonBuilder;
import org.easygson.JsonEntity;
import sportsscheduler.results.Context;
import sportsscheduler.results.out.exporter.ScheduleToJson;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.Tables;

import java.util.List;
import java.util.Map;

import static org.easygson.JsonEntity.emptyArray;
import static org.easygson.JsonEntity.emptyObject;

class SummaryToJson {

    private SummaryResponse response;

    public String convert(SummaryResponse response) {
        this.response = response;
        JsonEntity json = emptyObject();
        json.create("schedule", infoToJson());
        json.create("scheduleStats", statsToJson());
        json.create("winners", winnersToJson());
        json.create("teamPlayers", playersToJson());
        json.create("tables", emptyObject());
        json.get("tables").create("classicTable", classicTableToJson());
        json.get("tables").create("crossTable", crossTableToJson());
        json.create("matches", matchesToJson());

        return new GsonBuilder().setPrettyPrinting().create().toJson(json.raw());
    }

    private JsonEntity infoToJson() {
        JsonEntity s = emptyObject();
        s.create("scheduleType", response.scheduleType);
        s.create("groups", groupsToJson());
        return s;
    }

    private JsonEntity groupsToJson() {
        JsonEntity groups = emptyObject();
        for (Map.Entry<String, String> group : response.teams.entrySet()) {
            groups.create(group.getKey(), group.getValue());
        }
        return groups;
    }

    private JsonEntity statsToJson() {
        JsonEntity s = emptyObject();
        s.create("groupsCount", response.groupsCount);
        s.create("teamsCount", response.teamsCount);
        s.create("fieldsCount", response.fieldsCount);
        s.create("playersCount", response.playersCount);
        s.create("matchesCount", response.matchesCount);
        s.create("goalsCount", response.goalsCount);
        s.create("averageGoalsInMatch", response.averageGoalsInMatch);
        return s;
    }

    private JsonEntity winnersToJson() {
        JsonEntity s = emptyObject();
        s.create("teams", topTeams());
        s.create("scorers", topScorers());
        s.create("goalkeeper", topGoalkeeper());
        return s;
    }

    private JsonEntity topTeams() {
        JsonEntity s = emptyObject();
        int rank = 1;
        for (String t : response.topTeams) {
            s.create(rank++ + "", t);
        }
        return s;
    }

    private JsonEntity topScorers() {
        JsonEntity s = emptyObject();
        for (Map.Entry<Integer, List<PresentablePlayer>> e : response.topScorers.entrySet()) {
            JsonEntity a = emptyArray();
            for (PresentablePlayer player : e.getValue()) {
                JsonEntity p = emptyObject();
                p.create("player", player.name);
                p.create("team", player.team);
                a.create(p);
            }
            s.create(e.getKey().toString(), a);
        }
        return s;
    }

    private JsonEntity topGoalkeeper() {
        JsonEntity s = emptyObject();
        JsonEntity a = emptyArray();
        for (String g : response.topGoalkeepers) {
            a.create(g);
        }
        s.create(response.goalkeeperMinGoalsCount + "", a);
        return s;
    }

    private JsonEntity playersToJson() {
        JsonEntity s = emptyObject();
        for (Map.Entry<String, List<PresentablePlayer>> team : response.players.entrySet()) {
            JsonEntity players = emptyObject();
            for (PresentablePlayer player : team.getValue()) {
                players.create(player.name, player.goalsCount);
            }
            s.create(team.getKey(), players);
        }
        return s;
    }

    private JsonEntity classicTableToJson() {
        JsonEntity s = emptyObject();
        for (Map.Entry<String, Tables.GroupTable> group : response.tables.groups.entrySet()) {
            JsonEntity teams = emptyObject();
            for (PresentableTeamResult result : group.getValue().classicTable.teams) {
                JsonEntity r = emptyObject();
                r.create("rank", result.rank);
                r.create("matchesCount", result.matchesCount);
                r.create("winsCount", result.totalWins());
                r.create("winsNormalCount", result.winsCount);
                r.create("winsExtraCount", result.winsExtraCount);
                r.create("drawsCount", result.drawsCount);
                r.create("lossesCount", result.totalLosses());
                r.create("lossesNormalCount", result.lossesCount);
                r.create("lossesExtraCount", result.lossesExtraCount);
                r.create("goalsFor", result.goalsFor);
                r.create("goalsAgainst", result.goalsAgainst);
                r.create("points", result.points);
                teams.create(result.team, r);
            }
            s.create(group.getKey(), teams);
        }
        return s;
    }

    private JsonEntity crossTableToJson() {
        JsonEntity s = emptyObject();
        for (Map.Entry<String, Tables.GroupTable> group : response.tables.groups.entrySet()) {
            JsonEntity teams = emptyObject();
            for (Map.Entry<PresentableTeamResult, Map<String, List<String>>> team : group.getValue().crossTable.entrySet()) {
                JsonEntity opponents = emptyObject();
                for (Map.Entry<String, List<String>> opponent : team.getValue().entrySet()) {
                    JsonEntity results = emptyArray();
                    for (String score : opponent.getValue()) {
                        results.create(score);
                    }
                    opponents.create(opponent.getKey(), results);
                }
                PresentableTeamResult r = team.getKey();
                JsonEntity teamRow = emptyObject();
                teamRow.create("opponents", opponents);
                teamRow.create("points", r.points);
                teamRow.create("score", String.format("%s:%s", r.goalsFor, r.goalsAgainst));
                teamRow.create("rank", r.rank);
                teams.create(r.team, teamRow);
            }
            s.create(group.getKey(), teams);
        }
        return s;
    }

    private JsonEntity matchesToJson() {
        JsonEntity s = emptyArray();
        for (PresentableMatch match : response.matches) {
            JsonEntity m = emptyObject();

            JsonEntity teams = emptyArray();
            teams.create(match.home.team);
            teams.create(match.away.team);
            m.create("teams", teams);

            JsonEntity result = emptyObject();
            if (match.isPlayed) {
                JsonEntity goals = emptyArray();
                goals.create(match.home.goalsCount);
                goals.create(match.away.goalsCount);
                result.create("goals", goals);

                JsonEntity scorers = emptyArray();
                scorers.create(scorersToJson(match.home.getScorers()));
                scorers.create(scorersToJson(match.away.getScorers()));
                result.create("scorers", scorers);
                result.create("type", match.resultType.getAbbrevation());
            }
            m.create("result", result);
            ScheduleToJson.attributesToJson(match.attributes, Context.currentSchedule.settings, Context.currentSchedule.matches.getMatch(match.idMatch).getScore(), m);

            s.create(m);
        }
        return s;
    }

    private JsonEntity scorersToJson(List<PresentablePlayer> scorers) {
        JsonEntity s = emptyObject();
        for (PresentablePlayer p : scorers) {
            s.create(p.name, p.goalsCount);
        }
        return s;
    }
}
