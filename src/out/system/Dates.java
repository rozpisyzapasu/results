/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;

import static sportsscheduler.results.Context.getZone;

public class Dates {

    public static long dateToTimestamp(LocalDateTime date) {
        return date.toInstant(getZone(date)).getEpochSecond();
    }

    public static LocalDateTime timestampToDate(String timestampValue) {
        BigInteger timestamp = new BigInteger(timestampValue);
        Instant instant = Instant.ofEpochSecond(timestamp.longValue());
        return LocalDateTime.ofInstant(instant, getZone(instant));
    }
}
