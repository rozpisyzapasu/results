/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;
import sportsscheduler.results.out.importer.JsonReader;

public class FileWebReader implements JsonReader {

    private JsonReader file = new JsonFileReader();
    private JsonWebReader web = new JsonWebReader();

    public JsonResult read(String path) {
        JsonResult result = file.read(path);
        if (result.containsValidSchedule()) {
            return result;
        } else {
            return web.read(path);
        }
    }
}
