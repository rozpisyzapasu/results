/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import sportsscheduler.results.out.system.JsonResult.JsonError;

public class InvalidJsonException extends Exception {

    public final JsonError error;

    public InvalidJsonException(JsonError error) {
        this.error = error;
    }
}
