/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import java.nio.file.Path;

import static java.nio.file.FileSystems.getDefault;
import static java.nio.file.Files.readAllBytes;

public class JsonFileReader extends StringToJson {

    protected String getContent(String path) throws Exception {
        Path p = getDefault().getPath(path);
        byte[] bytes = readAllBytes(p);
        return new String(bytes, "UTF-8");
    }
}
