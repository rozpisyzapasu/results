/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;
import sportsscheduler.results.out.exporter.JsonWriter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class JsonFileWriter implements JsonWriter {

    public WriteResult write(String path, JsonEntity json) {
        if (isWebLink(path)) {
            return WriteResult.PATH_IS_WEBLINK;
        }
        try {
            Path p = Paths.get(path);
            Files.write(p, Arrays.asList(json.toString()), StandardCharsets.UTF_8);
            return WriteResult.OK;
        } catch (IOException e) {
            return WriteResult.IO_ERROR;
        }
    }

    private boolean isWebLink(String path) {
        return path.startsWith("http://")
            || path.startsWith("https://");
    }
}
