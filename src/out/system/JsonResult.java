/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;

public class JsonResult {

    public JsonEntity json;
    public JsonError error;

    public static JsonResult valid(JsonEntity json) {
        JsonResult r = new JsonResult();
        r.json = json;
        return r;
    }

    public static JsonResult invalid() {
        return JsonResult.invalid(JsonError.UNKNOWN_ERROR);
    }

    public static JsonResult invalid(JsonError error) {
        JsonResult r = new JsonResult();
        r.json = JsonEntity.emptyObject();
        r.error = error;
        return r;
    }

    public boolean containsValidSchedule() {
        return error == null
            && json.isObject()
            && !json.equals(JsonEntity.emptyObject());
    }

    public enum JsonError {
        UNKNOWN_ERROR,
        NO_SCHEDULE,
        ALREADY_IMPORTED,
        INVALID_RESPONSE_CODE,
        INVALID_JSON_CONTENT;
    }
}
