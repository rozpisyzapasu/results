/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import sportsscheduler.results.out.system.JsonResult.JsonError;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import static java.net.HttpURLConnection.HTTP_MOVED_PERM;
import static java.net.HttpURLConnection.HTTP_MOVED_TEMP;

public class JsonWebReader extends StringToJson {

    protected String getContent(String path) throws Exception {
        HttpURLConnection connection = openConnection(path);
        if (isRedirected(connection)) {
            connection = openConnection(connection.getHeaderField("Location"));
        }
        checkResponse(connection);
        return getPageContent(connection);
    }

    private boolean isRedirected(HttpURLConnection connection) throws Exception {
        int status = connection.getResponseCode();
        return status == HTTP_MOVED_TEMP || status == HTTP_MOVED_PERM;
    }

    private HttpURLConnection openConnection(String path) throws Exception {
        URL url = new URL(path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(5 * 1000);
        connection.setReadTimeout(5 * 1000);
        connection.connect();
        return connection;
    }

    private void checkResponse(HttpURLConnection connection) throws Exception {
        if (connection.getResponseCode() != 200) {
            throw new InvalidJsonException(JsonError.INVALID_RESPONSE_CODE);
        }
        if (!connection.getHeaderField("Content-Type").contains("application/json")) {
            throw new InvalidJsonException(JsonError.INVALID_JSON_CONTENT);
        }
    }

    private String getPageContent(HttpURLConnection connection) throws Exception {
        Scanner scanner = new Scanner(connection.getInputStream(), "UTF-8");
        scanner.useDelimiter("\n");
        String content = "";
        while (scanner.hasNext()) {
            content += scanner.next();
        }
        return content;
    }
}
