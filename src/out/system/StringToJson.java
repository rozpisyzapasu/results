/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;
import sportsscheduler.results.out.importer.JsonReader;
import sportsscheduler.results.out.system.JsonResult.JsonError;

abstract class StringToJson implements JsonReader {

    public JsonResult read(String path) {
        if (path.isEmpty()) {
            return JsonResult.invalid(JsonError.NO_SCHEDULE);
        }
        try {
            String content = getContent(path);
            return JsonResult.valid(new JsonEntity(content));
        } catch (InvalidJsonException e) {
            return JsonResult.invalid(e.error);
        } catch (Exception e) {
            return JsonResult.invalid();
        }
    }

    protected abstract String getContent(String path) throws Exception;
}
