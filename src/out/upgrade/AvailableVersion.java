/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.upgrade;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AvailableVersion {
    public String version;
    public Date releaseDate;
    public List<String> changelog = new ArrayList<>();

    public String releaseDateTo(String format) {
        return new SimpleDateFormat(format).format(releaseDate);
    }
}
