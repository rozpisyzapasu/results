/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.upgrade;

import org.easygson.JsonEntity;
import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Upgrade implements Usecase<String, UpgradePresenter> {

    private static final String CHECK = "https://www.rozpisyzapasu.cz/api/results/upgrade/";
    private static final String DOWNLOAD = "https://www.rozpisyzapasu.cz/api/results/latest.zip?app";
    private DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public void execute(String currentVersion, UpgradePresenter presenter) {
        JsonEntity changelog = Context.webReader.read(CHECK + currentVersion).json;
        if (changelog.isArray()) {
            if (changelog.arraySize() > 0) {
                presenter.upgradeIsAvailable(DOWNLOAD, convertChangelog(changelog));
            } else {
                presenter.noNewVersionExists();
            }
        } else {
            presenter.checkFailed();
        }
    }

    private List<AvailableVersion> convertChangelog(JsonEntity changelog) {
        List<AvailableVersion> versions = new ArrayList<>();
        for (JsonEntity version : changelog) {
            AvailableVersion v = new AvailableVersion();
            v.version = version.asString("version");
            v.releaseDate = parseDate(version.asString("releaseDate"));
            for (JsonEntity ch : version.get("changelog")) {
                v.changelog.add(ch.asString());
            }
            versions.add(v);
        }
        return versions;
    }

    private Date parseDate(String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }
}
