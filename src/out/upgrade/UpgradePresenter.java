/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.upgrade;

import java.util.List;
import java.util.Map;

public interface UpgradePresenter {

    void checkFailed();

    void noNewVersionExists();

    void upgradeIsAvailable(String downloadUrl, List<AvailableVersion> newVersions);
}
