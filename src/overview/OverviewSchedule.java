/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.overview;

import sportsscheduler.results.*;
import sportsscheduler.results.match.Match;
import sportsscheduler.results.match.Team;
import java.util.*;

public class OverviewSchedule implements UsecaseGetter<PresentableSchedule> {

    public PresentableSchedule execute() {
        Schedule schedule = Context.currentSchedule;
        PresentableSchedule s = new PresentableSchedule();
        s.name = schedule.name;
        s.path = schedule.originalPath;
        s.teamsCount = schedule.matches.countTeams();
        s.matchesCount = schedule.matches.countMatches();
        s.groupsCount = schedule.matches.countGroups();
        s.settings = schedule.settings;
        s.groups = schedule.matches.mapGroupAndTeams();
        s.teams = convertTeams(schedule.matches.getTeams());
        s.matches = convertMatches(schedule.matches.getMatches());
        return s;
    }

    private Iterable<PresentableTeam> convertTeams(Iterable<Team> teams) {
        List<PresentableTeam> names = new ArrayList<>();
        for (Team t : teams) {
            names.add(new PresentableTeam(t));
        }
        return names;
    }

    private Iterable<PresentableMatch> convertMatches(Iterable<Match> matchesIterator) {
        List<PresentableMatch> matches = new ArrayList<>();
        for (Match m : matchesIterator) {
            matches.add(new PresentableMatch(m));
        }
        return matches;
    }
}
