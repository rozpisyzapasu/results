/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.overview;

import sportsscheduler.results.match.Match;
import sportsscheduler.results.match.MatchAttributes;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.TeamInMatch;

import java.util.Iterator;

public class PresentableMatch {

    public final int idMatch;
    public final boolean isPlayed, isPlayoffMatch;
    public final PresentableTeamInMatch home, away;
    public final ResultType resultType;
    public final MatchAttributes attributes;

    public PresentableMatch(Match m) {
        idMatch = m.getId();
        Iterator<TeamInMatch> teams = m.getTeams().iterator();
        home = new PresentableTeamInMatch(teams.next());
        away = new PresentableTeamInMatch(teams.next());
        isPlayoffMatch = m.isPlayoffMatch();
        isPlayed = home.goalsCount != null;
        resultType = m.getScore().getResultType();
        attributes = m.getAttributes();
    }
}
