/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.overview;

import sportsscheduler.results.ScheduleSettings;

import java.util.Map;

public class PresentableSchedule {
    public String name, path;
    public int matchesCount, teamsCount, groupsCount;
    public Iterable<PresentableTeam> teams;
    public Iterable<PresentableMatch> matches;
    public ScheduleSettings settings;
    public Map<String, String[]> groups;

    public int getGroupIndex(String team) {
        int index = 0;
        for (Map.Entry<String, String[]> group : groups.entrySet()) {
            for (String t : group.getValue()) {
                if (t.equals(team)) {
                    return index;
                }
            }
            index++;
        }
        return -1;
    }
}
