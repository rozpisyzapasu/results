/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.overview;

import sportsscheduler.results.match.Player;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.match.Team;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.util.ArrayList;
import java.util.List;

public class PresentableTeam {

    public final String name, group;
    public final Iterable<PresentablePlayer> players;
    public final Stats advancedStats;

    public PresentableTeam(Team team) {
        name = team.name;
        group = team.getGroup();
        players = getPlayers(team.getPlayers());
        advancedStats = team.advancedStats.getActive();
    }

    private Iterable<PresentablePlayer> getPlayers(Iterable<Player> players) {
        List<PresentablePlayer> rows = new ArrayList<>();
        for (Player player : players) {
            rows.add(new PresentablePlayer(player));
        }
        return rows;
    }
}
