/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.overview;

import sportsscheduler.results.match.*;
import sportsscheduler.results.score.type.ExistingScore;
import sportsscheduler.results.score.type.Scorer;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.util.*;
import java.util.stream.Collectors;

public class PresentableTeamInMatch {

    public final PresentableTeam teamDetail;
    public final String team, group;
    public final Integer goalsCount, playoffSeed;
    private final List<PresentablePlayer> scorers = new ArrayList<>();
    public final Stats advancedStats;

    public PresentableTeamInMatch(TeamInMatch team) {
        Team t = team.getTeam();
        teamDetail = new PresentableTeam(t);
        this.team = t.name;
        group = t.getGroup();
        playoffSeed = t.playoffSeed;

        Score score = team.getScore();
        Map<String, Scorer> matchScorers = new LinkedHashMap<>();
        if (score.isSet()) {
            ExistingScore s = (ExistingScore) score;
            goalsCount = s.getGoalsFor();
            loadExistingScorers(t, s, matchScorers);
            advancedStats = s.getTeamStats(t.name);
        } else {
            goalsCount = null;
            advancedStats = new Stats();
        }
        addAllPlayers(t, matchScorers);
    }

    private void loadExistingScorers(Team t, ExistingScore score, Map<String, Scorer> matchScorers) {
        for (Scorer sc : score.getTeamScorers(t).collect(Collectors.toList())) {
            matchScorers.put(sc.getName(), sc);
        }
    }

    private void addAllPlayers(Team t, Map<String, Scorer> matchScorers) {
        for (Player p : t.getPlayers()) {
            Scorer s = matchScorers.containsKey(p.name) ? matchScorers.get(p.name) : noGoal(p);
            scorers.add(new PresentablePlayer(s));
        }
    }

    private Scorer noGoal(Player p) {
        return new Scorer(p, 0, new Stats());
    }

    public List<PresentablePlayer> getAllPlayers() {
        return scorers;
    }

    public List<PresentablePlayer> getScorers() {
        return scorers.stream().filter(s -> s.hasScored()).collect(Collectors.toList());
    }
}
