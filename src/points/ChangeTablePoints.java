/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.Usecase;

public class ChangeTablePoints implements Usecase<TablePointsRequest, TablePointsPresenter> {

    private PointsParser parser = new PointsParser();

    public void execute(TablePointsRequest request, TablePointsPresenter presenter) {
        TablePoints newPoints = buildPoints(request);
        if (newPoints.areValid()) {
            Schedule schedule = Context.currentSchedule;
            ScheduleSettings settings = schedule.settings;
            settings.points = newPoints;
            schedule.matches.recalculatePoints(newPoints);
            presenter.pointsChanged();
        } else {
            presenter.pointsAreNotInDescendingOrder();
        }
    }

    private TablePoints buildPoints(TablePointsRequest request) {
        TablePoints newPoints = new TablePoints();
        newPoints.win = request.win;
        newPoints.draw = request.draw;
        newPoints.loss = request.loss;
        newPoints.matchScores = parser.parse(request.matchScores);
        newPoints.resultScores = parser.parse(request.resultScores);
        return newPoints;
    }
}
