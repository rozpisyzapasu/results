/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import sportsscheduler.results.match.ResultType;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PointsParser {

    private Pattern matchScorePattern = Pattern.compile("(\\d+):(\\d+) (\\d+)(?:-(\\d+))?");
    private Pattern resultScorePattern = Pattern.compile("(\\d+)-(\\d+)");

    public static String convert(List<TablePoints.MatchScorePoints> matchScores) {
        String text = "";
        for (TablePoints.MatchScorePoints matchScore : matchScores) {
            if (!text.isEmpty()) {
                text += "\n";
            }
            text += String.format("%d:%d ", matchScore.goals[0], matchScore.goals[1]);
            if (matchScore.points.win == matchScore.points.loss) {
                text += matchScore.points.draw;
            } else {
                text += String.format("%d-%d", matchScore.points.win, matchScore.points.loss);
            }
        }
        return text;
    }

    public static String convert(ResultType type, TablePoints points) {
        if (points.resultScores.containsKey(type)) {
            TablePoints resultPoints = points.resultScores.get(type);
            return String.format("%s-%s", resultPoints.win, resultPoints.loss);
        }
        return "";
    }

    public Map<ResultType, TablePoints> parse(Map<ResultType, String> types) {
        TablePoints points = TablePoints.defaultPoints();
        for (Map.Entry<ResultType, String> entry : types.entrySet()) {
            Matcher m = resultScorePattern.matcher(entry.getValue());
            if (m.matches()) {
                TablePoints p  = TablePoints.defaultPoints();
                p.win = parseInt(m, 1);
                p.loss = parseInt(m, 2);
                p.draw = p.loss;
                points.addResultScore(entry.getKey(), p);
            }
        }
        return points.resultScores;
    }

    public List<TablePoints.MatchScorePoints> parse(String lines) {
        List<TablePoints.MatchScorePoints> points = new ArrayList<>();
        for (String line : splitLines(lines)) {
            Matcher m = matchScorePattern.matcher(line);
            if (m.matches()) {
                points.add(regexToPoints(m));
            }
        }
        return points.stream().filter(p -> p.points.areValid()).collect(Collectors.toList());
    }

    private String[] splitLines(String lines) {
        return lines == null ? new String[] {} : lines.split("\\r?\\n");
    }

    private TablePoints.MatchScorePoints regexToPoints(Matcher m) {
        TablePoints.MatchScorePoints p = new TablePoints.MatchScorePoints(parseInt(m, 1), parseInt(m, 2));
        p.points.win = parseInt(m, 3);
        if (m.group(4) != null) {
            p.points.loss = parseInt(m, 4);
            p.points.draw = p.points.loss;
        } else {
            p.points.loss = p.points.win;
            p.points.draw = p.points.win;
        }
        return p;
    }

    private int parseInt(Matcher m, int group) {
        return Integer.parseInt(m.group(group));
    }
}
