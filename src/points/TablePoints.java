/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import sportsscheduler.results.match.ResultType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TablePoints {
    public int win = 3, draw = 1, loss = 0;
    public List<MatchScorePoints> matchScores = new ArrayList<>();
    public Map<ResultType, TablePoints> resultScores = new HashMap<>();

    public static TablePoints defaultPoints() {
        return new TablePoints();
    }

    public void addMatchScore(MatchScorePoints points) {
        if (points.points.areValid()) {
            matchScores.add(points);
        }
    }

    public void addResultScore(ResultType resultType, TablePoints points) {
        if (resultType != ResultType.NORMAL && points.areValid() && points.win > points.loss) {
            resultScores.put(resultType, points);
        }
    }

    public boolean areValid() {
        return win >= draw && draw >= loss && loss >= 0;
    }

    public static class MatchScorePoints {
        public final int[] goals;
        public TablePoints points = defaultPoints();

        public MatchScorePoints(int... goals) {
            this.goals = goals;
        }
    }
}
