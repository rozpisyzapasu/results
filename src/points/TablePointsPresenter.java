/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

public interface TablePointsPresenter {

    void pointsAreNotInDescendingOrder();

    void pointsChanged();
}
