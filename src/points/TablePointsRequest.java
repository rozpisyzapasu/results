/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import java.util.HashMap;
import java.util.Map;
import sportsscheduler.results.match.ResultType;

public class TablePointsRequest {
    public int win, draw, loss;
    public String matchScores;
    public Map<ResultType, String> resultScores = new HashMap<>();
}
