/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.repository;

import sportsscheduler.results.Schedule;
import sportsscheduler.results.ScheduleRepository;

import java.util.HashMap;
import java.util.Map;

public class InMemorySchedules implements ScheduleRepository {

    private final Map<String, Schedule> schedules = new HashMap<>();

    public void save(Schedule schedule) {
        schedules.put(schedule.name, schedule);
    }

    public Schedule find(String name) {
        return schedules.get(name);
    }

    public boolean areAllSchedulesSaved() {
        for (Schedule s : schedules.values()) {
            if (!s.isSaved) {
                return false;
            }
        }
        return true;
    }
}
