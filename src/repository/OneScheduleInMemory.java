/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.repository;

import sportsscheduler.results.Schedule;
import sportsscheduler.results.ScheduleRepository;

public class OneScheduleInMemory implements ScheduleRepository {

    private Schedule schedule;

    public void save(Schedule schedule) {
        this.schedule = schedule;
    }

    public Schedule find(String name) {
        return schedule;
    }

    public boolean areAllSchedulesSaved() {
        return schedule.isSaved;
    }
}
