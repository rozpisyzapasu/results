/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score;

import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.match.*;
import sportsscheduler.results.score.type.ExistingScore;
import sportsscheduler.results.score.type.ScoreFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ChangeScore implements Usecase<ScoreRequest, ScorePresenter> {

    public void execute(ScoreRequest request, ScorePresenter presenter) {
        Matches matches = Context.currentSchedule.matches;
        Match match = matches.getMatch(request.idMatch);
        if (match == null) {
            presenter.nonExistentMatch();
            return;
        }

        Iterator<TeamInMatch> iterator = match.getTeams().iterator();
        String[] teams = new String[] { iterator.next().getTeam().name, iterator.next().getTeam().name };
        Score score = ScoreFactory.buildScore(
                request.resultType, request.goalsHome, request.goalsAway, request.players,
                request.statsHome, request.statsAway, teams
        );

        if (score.isValid()) {
            if (match.isScoreChanged(score) || !match.getAttributes().note.equals(request.note)) {
                match.setScore(score);
                match.getAttributes().note = request.note;
                updatePlayoffSchedule(matches, match, score);
                presenter.scoreChanged();
            } else {
                presenter.noChangeInScore();
            }
        } else {
            presenter.negativeScore();
        }
    }

    public void updatePlayoffSchedule(Matches matches, Match match, Score score) {

        if (match.isPlayoffMatch() && score instanceof ExistingScore) {
            ExistingScore s = (ExistingScore)score;
            if (s.getGoalsFor() == s.getGoalsAgainst()) {
                return;
            }
            Iterator<TeamInMatch> teams = match.getTeams().iterator();
            Team home = teams.next().getTeam(), away = teams.next().getTeam();
            Team winner = s.getGoalsFor() > s.getGoalsAgainst() ? home : away;
            Team loser = winner == away ? home : away;
            for (Match nextMatch : matches.getFutureMatches(match)) {
                nextMatch.replaceTeam(loser, winner);
            }
        }
    }
}
