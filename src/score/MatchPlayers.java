/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score;

import java.util.ArrayList;
import java.util.List;

public class MatchPlayers {

    private final List<ScorerRequest> scorers = new ArrayList<>();

    public void addGoals(String player, String team, int goalsCount) {
        getScorer(player, team).goalsCount = goalsCount;
    }

    public void addStat(String player, String team, String type, int value) {
        getScorer(player, team).addPlayerStat(type, value);
    }

    private ScorerRequest getScorer(String player, String team) {
        for (ScorerRequest scorer : scorers) {
            if (scorer.player.equals(player) && scorer.team.equals(team)) {
                return scorer;
            }
        }
        ScorerRequest n = new ScorerRequest(player, team);
        scorers.add(n);
        return n;
    }

    public int size() {
        return scorers.size();
    }

    public Iterable<ScorerRequest> iterate() {
        return scorers;
    }
}
