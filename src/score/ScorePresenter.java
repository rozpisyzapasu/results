/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score;

public interface ScorePresenter {
    void nonExistentMatch();

    void negativeScore();

    void scoreChanged();

    void noChangeInScore();
}
