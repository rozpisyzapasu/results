/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score;

import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.Stats;

import java.util.ArrayList;
import java.util.List;

public class ScoreRequest {
    public int idMatch;
    public Integer goalsHome;
    public Integer goalsAway;
    public ResultType resultType = ResultType.NORMAL;
    public final MatchPlayers players = new MatchPlayers();
    public final Stats statsHome = new Stats(), statsAway = new Stats();

    public String note = "";

    public void addTeamStat(String type, int home, int away) {
        statsHome.put(type, home);
        statsAway.put(type, away);
    }
}
