/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score;

import sportsscheduler.results.match.Stats;

public class ScorerRequest {
    public String player, team;
    public int goalsCount;
    public final Stats advancedStats = new Stats();

    public ScorerRequest(String player, String team) {
        this.player = player;
        this.team = team;
    }

    public void addPlayerStat(String type, int value) {
        advancedStats.put(type, value);
    }
}
