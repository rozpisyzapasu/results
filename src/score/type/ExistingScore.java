/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score.type;

import sportsscheduler.results.match.*;
import sportsscheduler.results.score.MatchPlayers;
import sportsscheduler.results.score.ScorerRequest;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExistingScore implements Score {

    private final int goalsFor, goalsAgainst;
    private List<Scorer> scorers = new ArrayList<>();
    private MatchPlayers scorersInput;
    private Map<String, Stats> teamStats;
    private ResultType resultType;

    public static ExistingScore score(Team team, ResultType resultType, int goalsFor, int goalsAgainst, MatchPlayers scorers) {
        ExistingScore score = new ExistingScore(resultType, goalsFor, goalsAgainst, scorers, new LinkedHashMap<>());
        score.loadTeams(team, Team.test("irrelevant away team", ""));
        return score;
    }

    public static ExistingScore givenScore(Team team, int goalsFor, int goalsAgainst, MatchPlayers scorers) {
        return score(team, ResultType.NORMAL, goalsFor, goalsAgainst, scorers);
    }

    public static ExistingScore givenScore(int goalsFor, int goalsAgainst) {
        return givenScore(ResultType.NORMAL, goalsFor, goalsAgainst);
    }

    public static ExistingScore givenScore(ResultType resultType,int goalsFor, int goalsAgainst) {
        return new ExistingScore(resultType, goalsFor, goalsAgainst, new MatchPlayers(), new LinkedHashMap<>());
    }

    public ExistingScore(ResultType resultType, int goalsFor, int goalsAgainst, MatchPlayers scorers, Map<String, Stats> stats) {
        this.goalsFor = goalsFor;
        this.goalsAgainst = goalsAgainst;
        this.scorersInput = scorers;
        this.teamStats = stats;
        this.resultType = resultType;
    }

    private ExistingScore(ResultType resultType, int goalsFor, int goalsAgainst, List<Scorer> scorers, Map<String, Stats> stats, boolean javaHack) {
        this.goalsFor = goalsFor;
        this.goalsAgainst = goalsAgainst;
        this.scorers = scorers;
        this.teamStats = stats;
        this.resultType = resultType;
    }

    public boolean isSet() {
        return true;
    }

    public ResultType getResultType() {
        return this.resultType;
    }

    public boolean isValid() {
        return this.goalsFor >= 0 && this.goalsAgainst >= 0;
    }

    public boolean isDifferent(Score s) {
        if (s.isSet()) {
            ExistingScore score = (ExistingScore)s;
            return this.goalsFor != score.goalsFor
                    || this.goalsAgainst != score.goalsAgainst
                    || scorers.size() != score.scorersInput.size()
                    || areScorersDifferent(score)
                    || this.resultType != score.resultType
                    || teamStats.size() != score.teamStats.size()
                    || areTeamStatsDifferent(score);
        } else {
            return true;
        }
    }

    private boolean areScorersDifferent(ExistingScore score) {
        for (ScorerRequest thatScorer : score.scorersInput.iterate()) {
            Scorer thisScorer = findScorer(thatScorer);
            if (thisScorer == null || thatScorer.goalsCount != thisScorer.goalsCount || thatScorer.advancedStats.isDifferent(thisScorer.advancedStats)) {
                return true;
            }
        }
        return false;
    }

    private boolean areTeamStatsDifferent(ExistingScore score) {
        for (Map.Entry<String, Stats> statsEntry : score.teamStats.entrySet()) {
            if (statsEntry.getValue().isDifferent(teamStats.get(statsEntry.getKey()))) {
                return true;
            }
        }
        return false;
    }

    private Scorer findScorer(ScorerRequest thatScorer) {
        for (Scorer thisScorer : scorers) {
            if (thisScorer.hasSamePlayer(thatScorer)) {
                return thisScorer;
            }
        }
        return null;
    }

    public Score reverse() {
        return new ExistingScore(resultType, goalsAgainst, goalsFor, scorers, teamStats, true);
    }

    public Stream<Scorer> getTeamScorers(Team team) {
        return scorers.stream().filter(
                scorer -> team.hasPlayer(scorer.getPlayer().name)
        );
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public Iterable<Scorer> getScorersFromAllTeams() {
        return scorers;
    }

    public List<Scorer> getTeamScores(String team) {
        return scorers.stream().filter(scorer -> scorer.getTeam().equals(team)).collect(Collectors.toList());
    }

    public int countGoals() {
        return goalsFor + goalsAgainst;
    }

    public Stats getTeamStats(String team) {
        return teamStats.getOrDefault(team, new Stats());
    }

    public Map<String, Map.Entry<Integer, Integer>> getTeamStats() {
        Map<String, Map.Entry<Integer, Integer>> map = new LinkedHashMap<>();
        Iterator<Stats> teams = teamStats.values().iterator();
        if (!teams.hasNext()) {
            return map;
        }
        Stats home = teams.next();
        Stats away = teams.next();
        for (Map.Entry<String, Integer> entry : home.iterate()) {
            map.put(entry.getKey(), new AbstractMap.SimpleEntry<>(entry.getValue(), away.get(entry.getKey())));
        }
        return map;
    }

    public void loadTeams(Team home, Team away) {
        scorers.clear();
        for (ScorerRequest s : scorersInput.iterate()) {
            Player player = findTeam(s, home, away).getPlayer(s.player);
            if (player != null) {
                scorers.add(new Scorer(player, s.goalsCount, s.advancedStats));
            }
        }
    }

    private Team findTeam(ScorerRequest s, Team home, Team away) {
        if (home.name.equals(s.team)) {
            return home;
        } else if (away.name.equals(s.team)) {
            return away;
        } else {
            return home.hasPlayer(s.player) ? home : away;
        }
    }
}
