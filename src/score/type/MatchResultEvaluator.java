/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score.type;

import sportsscheduler.results.match.*;
import sportsscheduler.results.points.TablePoints;

public class MatchResultEvaluator {

    private final TablePoints points;

    public MatchResultEvaluator(TablePoints points) {
        this.points = points;
    }

    public void add(Team team, Score score) {
        updateTeam(team, score, 1);
    }

    public void subtract(Team team, Score score) {
        updateTeam(team, score, -1);
    }

    private void updateTeam(Team team, Score s, int one) {
        if (s instanceof ExistingScore) {
            ExistingScore score = (ExistingScore)s;
            Points matchPoints = getPoints(score);
            if (score.getGoalsFor() > score.getGoalsAgainst()) {
                team.wins.update(s.getResultType(), one);
                team.points += one * matchPoints.points.win;
            } else if (score.getGoalsFor() < score.getGoalsAgainst()) {
                team.losses.update(s.getResultType(), one);
                team.points += one * matchPoints.points.loss;
            } else if (score.getGoalsFor() == score.getGoalsAgainst()) {
                team.drawsCount += one;
                team.points += one * matchPoints.points.draw;
            }
            if (matchPoints.areGoalsUpdated) {
                team.goalsFor += one * score.getGoalsFor();
                team.goalsAgainst += one * score.getGoalsAgainst();
                score.getTeamScorers(team).forEach(scorer -> {
                    scorer.getPlayer().goalsCount += one * scorer.goalsCount;
                });
            }
            score.getTeamScorers(team).forEach(scorer -> {
                scorer.getPlayer().advancedStats.update(scorer.advancedStats, one);
            });
            team.advancedStats.update(score.getTeamStats(team.name), one);
        }
    }

    private Points getPoints(ExistingScore score) {
        for (ResultType resultType : points.resultScores.keySet()) {
            if (resultType.equals(score.getResultType())) {
                return Points.fromResult(points, resultType);
            }
        }
        for (TablePoints.MatchScorePoints matchScore : points.matchScores) {
            if (hasMatchScore(score, matchScore, 0, 1) || hasMatchScore(score, matchScore, 1, 0)) {
                return Points.create(matchScore.points);
            }
        }
        return Points.create(points);
    }

    private boolean hasMatchScore(ExistingScore score, TablePoints.MatchScorePoints matchScore, int goalsFor, int goalsAgainst) {
        return score.getGoalsFor() == matchScore.goals[goalsFor] && score.getGoalsAgainst() == matchScore.goals[goalsAgainst];
    }

    private static class Points {
        public TablePoints points;
        public boolean areGoalsUpdated;

        public static Points fromResult(TablePoints p, ResultType resultType) {
            return new Points(p.resultScores.get(resultType), resultType != ResultType.WIN_BY_DEFAULT);
        }

        public static Points create(TablePoints p) {
            return new Points(p, true);
        }

        private Points(TablePoints p, boolean areGoalsUpdated) {
            this.points = p;
            this.areGoalsUpdated = areGoalsUpdated;
        }
    }
}
