/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score.type;

import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.Score;

public class NoScore implements Score {

    public final static Score noScore = new NoScore();

    private NoScore() {}

    public boolean isSet() {
        return false;
    }

    public ResultType getResultType() {
        return ResultType.NORMAL;
    }

    public boolean isValid() {
        return true;
    }

    public boolean isDifferent(Score s) {
        return s.isSet();
    }

    public Score reverse() {
        return this;
    }

    public int countGoals() {
        return 0;
    }
}
