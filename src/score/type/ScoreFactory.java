/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score.type;

import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.Score;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.score.MatchPlayers;

import java.util.LinkedHashMap;
import java.util.Map;

public class ScoreFactory {

    public static Score buildScore(
            ResultType resultType, Integer home, Integer away, MatchPlayers players,
            Stats statsHome, Stats statsAway, String[] teams
    ) {
        if (home == null || away == null) {
            return NoScore.noScore;
        } else {
            Map<String, Stats> stats = new LinkedHashMap<>();
            stats.put(teams[0], statsHome);
            stats.put(teams[1], statsAway);
            return new ExistingScore(resultType, home, away, players, stats);
        }
    }
}
