/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.score.type;

import sportsscheduler.results.match.Player;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.score.ScorerRequest;

import java.util.LinkedHashMap;
import java.util.Map;

public class Scorer {
    private Player player;
    public int goalsCount;
    public final Stats advancedStats;

    public Scorer(Player player, int goalsCount, Stats advancedStats) {
        this.player = player;
        this.goalsCount = goalsCount;
        this.advancedStats = advancedStats;
    }

    public String getName() {
        return player.name;
    }

    public String getTeam() {
        return player.getTeam();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player p) {
        this.player = p;
    }

    public boolean hasSamePlayer(ScorerRequest that) {
        return this.getName().equals(that.player)
            && this.getTeam().equals(that.team);
    }
}
