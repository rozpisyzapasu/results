/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.settings;

import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EditSettings implements Usecase<EditSettingsRequest, EditSettingsPresenter> {

    public void execute(EditSettingsRequest request, EditSettingsPresenter presenter) {
        Context.currentSchedule.settings.matchUnit = request.matchUnit;
        Context.currentSchedule.settings.teamUnit = request.teamUnit;
        Context.currentSchedule.settings.hasDefaultUnits = false;
        Context.currentSchedule.settings.matchPeriods = request.matchPeriods;
        Context.currentSchedule.settings.hasMatchNote = request.hasMatchNote;
        if (request.hasAdvancedStats) {
            Context.currentSchedule.settings.teamStatsNames = normalize(request.teamStatsNames, request.matchPeriods);
            Context.currentSchedule.settings.playerStatsNames = normalize(request.playerStatsNames, new ArrayList<>());
        } else {
            Context.currentSchedule.settings.teamStatsNames = new ArrayList<>();
            Context.currentSchedule.settings.playerStatsNames = new ArrayList<>();
        }
        presenter.settingsSaved();
    }

    private List<String> normalize(List<String> names, List<String> ignored) {
        Map<String, String> normalized = new LinkedHashMap<>();
        if (names != null) {
            for (String rawName : names) {
                String name = rawName.trim();
                if (!name.isEmpty() && !ignored.contains(name)) {
                    normalized.put(name.toLowerCase(), name);
                }
            }
        }
        return normalized.values().stream().collect(Collectors.toList());
    }
}
