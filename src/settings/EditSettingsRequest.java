/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.settings;

import sportsscheduler.results.ScheduleSettings;

import java.util.ArrayList;
import java.util.List;

public class EditSettingsRequest {
    public ScheduleSettings.Units.Team teamUnit;
    public ScheduleSettings.Units.Match matchUnit;

    public List<String> matchPeriods, teamStatsNames, playerStatsNames;
    public boolean hasMatchNote;
    public boolean hasAdvancedStats;
}
