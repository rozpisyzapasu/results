/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.goalkeepers;

import java.util.*;

public class Goalkeepers {

    public final Iterable<PresentableGoalkeeper> all;

    public Goalkeepers(Iterable<PresentableGoalkeeper> goalkeepers) {
        all = goalkeepers;
    }

    public int getMinGoalsCount() {
        Iterator<PresentableGoalkeeper> iterator = all.iterator();
        if (iterator.hasNext()) {
            return all.iterator().next().goalsAgainst;
        }
        return 0;
    }

    public Iterable<String> getTop() {
        int minGoals = getMinGoalsCount();
        List<String> goalkeepers = new ArrayList<>();
        for (PresentableGoalkeeper e : all) {
            if (e.goalsAgainst != minGoals) {
                break;
            }
            goalkeepers.add(e.team);
        }
        return goalkeepers;
    }
}
