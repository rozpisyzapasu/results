/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.goalkeepers;

import sportsscheduler.results.Context;
import sportsscheduler.results.UsecaseGetter;
import sportsscheduler.results.match.Team;

import java.util.*;

public class GoalkeepersStats implements UsecaseGetter<Goalkeepers> {

    public Goalkeepers execute() {
        Iterable<Team> teams = sortTeamsByGoalsAgainst();
        return new Goalkeepers(convertAll(teams));
    }

    private Iterable<Team> sortTeamsByGoalsAgainst() {
        List<Team> teams = new ArrayList<>();
        for (Team t : Context.currentSchedule.matches.getTeams()) {
            teams.add(t);
        }
        Collections.sort(teams, (a, b) -> Integer.compare(a.goalsAgainst, b.goalsAgainst));
        return teams;
    }

    private Iterable<PresentableGoalkeeper> convertAll(Iterable<Team> teams) {
        List<PresentableGoalkeeper> all = new ArrayList<>();
        for (Team t : teams) {
            all.add(new PresentableGoalkeeper(t));
        }
        return all;
    }
}
