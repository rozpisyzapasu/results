/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.goalkeepers;

import sportsscheduler.results.match.Team;

public class PresentableGoalkeeper {

    public final String team;
    public final int matchesCount, goalsAgainst;

    public PresentableGoalkeeper(Team t) {
        team = t.name;
        matchesCount = t.countMatches();
        goalsAgainst = t.goalsAgainst;
    }
}
