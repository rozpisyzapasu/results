/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.players;

import sportsscheduler.results.match.Player;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.score.type.Scorer;

public class PresentablePlayer {

    public final String name;
    public final String team;
    public final int goalsCount;
    public final Stats advancedStats;

    public PresentablePlayer(Player player) {
        this.name = player.name;
        this.team = player.getTeam();
        this.goalsCount = player.goalsCount;
        advancedStats = player.advancedStats.getActive();
    }

    public PresentablePlayer(Scorer s) {
        Player player = s.getPlayer();
        this.name = player.name;
        this.team = player.getTeam();
        this.goalsCount = s.goalsCount;
        advancedStats = s.advancedStats.getActive();
    }

    public boolean hasScored() {
        return goalsCount > 0;
    }
}
