/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.players;

import sportsscheduler.results.Context;
import sportsscheduler.results.UsecaseGetter;
import sportsscheduler.results.match.Player;
import sportsscheduler.results.match.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScorersStats implements UsecaseGetter<Iterable<PresentablePlayer>> {

    public Iterable<PresentablePlayer> execute() {
        List<PresentablePlayer> rows = new ArrayList<>();
        for (Team team : Context.currentSchedule.matches.getTeams()) {
            for (Player player : team.getPlayers()) {
                rows.add(new PresentablePlayer(player));
            }
        }
        Collections.sort(rows, (a, b) -> Integer.compare(b.goalsCount, a.goalsCount));
        return rows;
    }
}
