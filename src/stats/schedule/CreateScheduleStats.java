/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.schedule;

import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.UsecaseGetter;
import sportsscheduler.results.match.Match;
import sportsscheduler.results.match.Score;
import sportsscheduler.results.match.Team;

public class CreateScheduleStats implements UsecaseGetter<ScheduleStats> {

    public ScheduleStats execute() {
        Schedule schedule = Context.currentSchedule;

        ScheduleStats response = new ScheduleStats();
        response.groupsCount = schedule.matches.countGroups();
        response.teamsCount = schedule.matches.countTeams();
        response.fieldsCount = schedule.settings.fieldsCount;
        response.matchesCount = schedule.matches.countMatches();
        response.playersCount = countPlayers(schedule.matches.getTeams());
        countGoalsAndPlayedMatches(response, schedule.matches.getMatches());
        response.averageGoalsInMatch = response.goalsCount / (double) response.matchesCount;
        return response;
    }

    private int countPlayers(Iterable<Team> teams) {
        int count = 0;
        for (Team t : teams) {
            count += t.countPlayers();
        }
        return count;
    }

    private void countGoalsAndPlayedMatches(ScheduleStats response, Iterable<Match> matches) {
        response.goalsCount = 0;
        response.playedMatchesCount = 0;
        for (Match m : matches) {
            Score score = m.getScore();
            response.goalsCount += score.countGoals();
            if (score.isSet()) {
                response.playedMatchesCount++;
            }
        }
    }
}
