/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.schedule;

public class ScheduleStats {
    public int groupsCount, teamsCount, fieldsCount, playersCount, matchesCount, playedMatchesCount, goalsCount;
    public double averageGoalsInMatch;

    public int countUpcomingMatches() {
        return matchesCount - playedMatchesCount;
    }
}
