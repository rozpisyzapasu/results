/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.UsecaseGetter;
import sportsscheduler.results.match.Team;

import java.util.*;

public class CreateTable implements UsecaseGetter<Tables> {

    private Tables tables;
    private HeadToHeadMatchesComparator comparer = new HeadToHeadMatchesComparator();;

    public Tables execute() {
        Schedule schedule = Context.currentSchedule;
        comparer.setMatches(schedule.matches);
        tables = new Tables(schedule.matches.getGroupNames());
        classicTable(schedule);
        crossTable();
        return tables;
    }

    private void classicTable(Schedule schedule) {
        for (Team team : schedule.matches.getTeams()) {
            tables.groups.get(team.getGroup()).classicTable.addTeam(team);
            tables.fullTable.addTeam(team);
        }

        for (Tables.GroupTable t : tables.groups.values()) {
            comparer.sort(t.classicTable);
        }
        comparer.sort(tables.fullTable);
    }

    private void crossTable() {
        for (Map.Entry<String, Tables.GroupTable> e : tables.groups.entrySet()) {
            Map<PresentableTeamResult, Map<String, List<String>>> crossTable = e.getValue().crossTable;
            for (PresentableTeamResult team : e.getValue().classicTable.teams) {
                crossTable.put(team, new LinkedHashMap<>());
                for (PresentableTeamResult opponent : e.getValue().classicTable.teams) {
                    List<String> scores = comparer.getResults(team.team, opponent.team);
                    crossTable.get(team).put(opponent.team, scores);
                }
            }
        }
    }
}
