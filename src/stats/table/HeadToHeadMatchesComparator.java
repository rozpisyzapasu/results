/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import sportsscheduler.results.match.Match;
import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.Team;
import sportsscheduler.results.match.TeamInMatch;

import java.util.*;
import java.util.stream.Collectors;

class HeadToHeadMatchesComparator implements Comparator<PresentableTeamResult> {

    private final Comparator<PresentableTeamResult> comparator = new TeamResultComparator();
    private final Map<String, Map<String, HeadToHeadResults>> teams = new HashMap<>();
    private final Map<Integer, Map<String, PresentableTeamResult>> miniTables = new HashMap<>();
    private static final ScoreFormatter formatter = new ScoreFormatter();

    public void setMatches(Matches matches) {
        teams.clear();
        for (Team team : matches.getTeams()) {
            Map<String, HeadToHeadResults> opponents = new HashMap<>();
            for (Team opponent : matches.getTeams()) {
                opponents.put(opponent.name, new HeadToHeadResults());
            }
            teams.put(team.name, opponents);
        }

        for (Match m : matches.getMatches()) {
            for (TeamInMatch teamInMatch : m.getTeams()) {
                Map<String, HeadToHeadResults> opponents = teams.get(teamInMatch.getTeam().name);
                for (TeamInMatch opponent : m.getTeams()) {
                    opponents.get(opponent.getTeam().name).addMatch(teamInMatch, opponent);
                }
            }
        }
    }


    public void sort(Tables.ClassicTable table) {
        loadMiniTablesForTeamsWithSamePoints(table);
        table.sort(this);
    }

    public void loadMiniTablesForTeamsWithSamePoints(Tables.ClassicTable table) {
        Map<Integer, List<String>> teamsWithSamePoints = new HashMap<>();
        for (PresentableTeamResult team : table.teams) {
            if (!teamsWithSamePoints.containsKey(team.points)) {
                teamsWithSamePoints.put(team.points, new ArrayList<>());
            }
            teamsWithSamePoints.get(team.points).add(team.team);
        }

        miniTables.clear();
        for (Map.Entry<Integer, List<String>> entry : teamsWithSamePoints.entrySet()) {
            miniTables.put(entry.getKey(), new HashMap<>());
            for (PresentableTeamResult team : buildMiniTable(entry.getValue()).teams) {
                miniTables.get(entry.getKey()).put(team.team, team);
            }
        }
    }

    public Tables.ClassicTable buildMiniTable(List<String> teamNames) {
        Tables.ClassicTable miniTable = new Tables.ClassicTable();
        for (String teamName : teamNames) {
            Team team = Team.test(teamName, "table");
            teamNames.stream()
                    .filter(opponentName -> teamName != opponentName)
                    .forEach(opponentName -> teams.get(teamName).get(opponentName).loadTeamPoints(team));
            miniTable.addTeam(team);
        }
        miniTable.sort(this.comparator);
        return miniTable;
    }

    public int compare(PresentableTeamResult a, PresentableTeamResult b) {
        if (a.points == b.points) {
            int result = comparator.compare(
                    miniTables.get(a.points).get(a.team),
                    miniTables.get(b.points).get(b.team)
            );
            if (result != 0) {
                return result;
            }
        }
        return comparator.compare(a, b);
    }

    public List<String> getResults(String team, String opponent) {
        return teams.get(team).get(opponent).formatScores();
    }

    private static class HeadToHeadResults {
        private List<TeamInMatch> matches = new ArrayList<>();

        public void addMatch(TeamInMatch team, TeamInMatch opponent) {
            if (team != opponent) {
                matches.add(team);
            }
        }

        public void loadTeamPoints(Team team) {
            for (TeamInMatch matchResult : matches) {
                matchResult.initPoints(team);
            }
        }

        public List<String> formatScores() {
            return matches.stream().map(teamInMatch -> formatter.matchScoreToString(teamInMatch)).collect(Collectors.toList());
        }
    }
}
