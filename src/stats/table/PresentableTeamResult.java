/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import sportsscheduler.results.match.Team;

public class PresentableTeamResult {

    public final String team;
    public String group;
    public int winsCount, winsExtraCount, lossesCount, lossesExtraCount;
    public int matchesCount, drawsCount, goalsFor, goalsAgainst;
    public int rank, points;

    public PresentableTeamResult(Team team) {
        this.team = team.name;
        this.group = team.getGroup();
        this.winsCount = team.wins.countNormal();
        this.winsExtraCount = team.wins.countExtra();
        this.drawsCount = team.drawsCount;
        this.lossesCount = team.losses.countNormal();
        this.lossesExtraCount = team.losses.countExtra();
        this.goalsFor = team.goalsFor;
        this.goalsAgainst = team.goalsAgainst;
        this.points = team.points;
        this.matchesCount = team.countMatches();
    }

    public int totalWins() {
        return winsCount + winsExtraCount;
    }

    public int totalLosses() {
        return lossesCount + lossesExtraCount;
    }
}
