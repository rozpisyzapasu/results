/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.match.TeamInMatch;
import sportsscheduler.results.score.type.ExistingScore;

class ScoreFormatter {

    public String matchScoreToString(TeamInMatch team)
    {
        if (team.getScore().isSet()) {
            ExistingScore score = (ExistingScore) team.getScore();
            return scoreToString(
                    score.getGoalsFor(),
                    score.getGoalsAgainst(),
                    score.getResultType()
            );
        } else {
            return ":";
        }
    }

    public String scoreToString(Integer home, Integer away, ResultType resultType) {
        return String.format(
                "%s:%s%s",
                formatScore(home),
                formatScore(away),
                formatResultType(resultType)
        );
    }

    private String formatScore(Integer score) {
        return score != null ? score.toString() : "";
    }

    public String formatResultType(ResultType resultType) {
        return resultType == ResultType.NORMAL ? "" : " " + resultType.getAbbrevation();
    }
}
