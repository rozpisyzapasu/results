/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import sportsscheduler.results.Context;
import sportsscheduler.results.match.Team;

import java.util.*;

public class Tables {

    public ClassicTable fullTable = new ClassicTable();
    public final Map<String, GroupTable> groups = new HashMap<>();

    public Tables(Set<String> groups) {
        if (groups.isEmpty()) {
            this.groups.put(Context.defaultGroup, new GroupTable());
        } else {
            for (String group : groups) {
                this.groups.put(group, new GroupTable());
            }
        }
    }

    public GroupTable getFirstTable() {
        return groups.values().iterator().next();
    }

    public static class GroupTable {
        public ClassicTable classicTable = new ClassicTable();
        public Map<PresentableTeamResult, Map<String, List<String>>> crossTable = new LinkedHashMap<>();
    }

    public static class ClassicTable {
        public boolean hasExtraWin = false, hasDraw = false, hasExtraLoss;
        public List<PresentableTeamResult> teams = new ArrayList<>();

        public void addTeam(Team team) {
            PresentableTeamResult row = new PresentableTeamResult(team);
            reloadOptionalResultTypes(row);
            teams.add(row);
        }

        private void reloadOptionalResultTypes(PresentableTeamResult team) {
            hasDraw = hasDraw ? true : team.drawsCount > 0;
            hasExtraWin = hasExtraWin ? true : team.winsExtraCount > 0;
            hasExtraLoss = hasExtraLoss ? true : team.lossesExtraCount > 0;
        }

        public void sort(Comparator<? super PresentableTeamResult> comparer) {
            Collections.sort(teams, comparer);
            int rank = 1;
            for (PresentableTeamResult row : teams) {
                row.rank = rank++;
            }
        }
    }
}
