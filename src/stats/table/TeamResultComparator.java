/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import java.util.Comparator;

class TeamResultComparator implements Comparator<PresentableTeamResult> {

    public int compare(PresentableTeamResult a, PresentableTeamResult b) {
        int[][] criterions = {
                {a.points, b.points},
                {b.matchesCount, a.matchesCount},
                {a.goalsFor, b.goalsFor},
                {b.goalsAgainst, a.goalsAgainst}
        };
        for (int[] criterion : criterions) {
            if (criterion[0] != criterion[1]) {
                return criterion[0] > criterion[1] ? -1 : 1;
            }
        }
        return 0;
    }
}
