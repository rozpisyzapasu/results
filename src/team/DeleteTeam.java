/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import sportsscheduler.results.Context;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.Usecase;

public class DeleteTeam implements Usecase<String, DeleteTeamPresenter> {

    public void execute(String team, DeleteTeamPresenter presenter) {
        Schedule schedule = Context.currentSchedule;
        if (schedule.matches.hasTeam(team)) {
            schedule.matches.deleteTeam(team);
            presenter.successfulDelete();
        } else {
            presenter.nonExistentTeam();
        }
    }
}
