/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

public interface DeleteTeamPresenter {
    void nonExistentTeam();

    void successfulDelete();
}
