/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.Player;
import sportsscheduler.results.match.Team;

import java.util.*;

class TeamUpdater {

    private Team team;
    private UpdateTeamResponse response;
    private Matches matches;

    public void setTeam(Matches matches, String teamName) {
        this.matches = matches;
        this.team = matches.getTeam(teamName);
        response = new UpdateTeamResponse();
    }

    public void rename(String newName) {
        if (newName != null && !matches.hasTeam(newName)) {
            matches.renameTeam(team.name, newName);
            response.wasNameChanged = true;
        }
    }

    public Set<String> getExistingPlayers() {
        Set<String> allPlayers = new LinkedHashSet<>();
        for (Player player : team.getPlayers()) {
            allPlayers.add(player.name);
        }
        return allPlayers;
    }

    public void addPlayer(String player) {
        response.addedPlayersCount++;
        team.addPlayer(player);
    }

    public void deletePlayer(String player) {
        response.deletedPlayersCount++;
        team.deletePlayer(player);
    }

    public void sortPlayers(String[] players) {
        checkIfPlayersOrderIsChanged(players);
        if (response.werePlayersSorted) {
            List<String> order = Arrays.asList(players);
            team.sortPlayers((a, b) -> order.indexOf(a.getKey()) - order.indexOf(b.getKey()));
        }
    }

    private void checkIfPlayersOrderIsChanged(String[] players) {
        int index = 0;
        for (Player player : team.getPlayers()) {
            if (!player.name.equals(players[index++])) {
                response.werePlayersSorted = true;
                return;
            }
        }
    }

    public boolean isChanged() {
        return response.wasNameChanged ||
                response.werePlayersSorted ||
                response.addedPlayersCount > 0 ||
                response.deletedPlayersCount > 0;
    }

    public UpdateTeamResponse buildChangeSummary() {
        return response;
    }
}
