/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.match.Matches;

import java.util.LinkedHashSet;
import java.util.Set;

public class UpdateTeam implements Usecase<UpdateTeamRequest, UpdateTeamPresenter> {

    private final TeamUpdater team = new TeamUpdater();

    public void execute(UpdateTeamRequest request, UpdateTeamPresenter presenter) {
        Matches matches = Context.currentSchedule.matches;
        if (matches.hasTeam(request.team)) {
            team.setTeam(matches, request.team);
            team.rename(request.newName);
            addPlayers(request.players);
            deletePlayers(request.players);
            team.sortPlayers(request.players);
            if (team.isChanged()) {
                presenter.successfulChange(team.buildChangeSummary());
            } else {
                presenter.nothingChanged();
            }
        } else {
            presenter.nonExistentTeam();
        }
    }

    private void addPlayers(String[] players) {
        Set<String> oldPlayers = team.getExistingPlayers();
        Set<String> newPlayers = getNewPlayers(players);
        newPlayers.removeAll(oldPlayers);
        newPlayers.forEach(team::addPlayer);
    }

    private void deletePlayers(String[] players) {
        Set<String> oldPlayers = team.getExistingPlayers();
        Set<String> newPlayers = getNewPlayers(players);
        oldPlayers.removeAll(newPlayers);
        oldPlayers.forEach(team::deletePlayer);
    }

    private Set<String> getNewPlayers(String[] players) {
        Set<String> set = new LinkedHashSet<>();
        for (String player : players) {
            set.add(player);
        }
        return set;
    }
}
