/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

public interface UpdateTeamPresenter {

    void nonExistentTeam();

    void nothingChanged();

    void successfulChange(UpdateTeamResponse response);
}
