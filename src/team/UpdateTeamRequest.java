/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

public class UpdateTeamRequest {

    public String team;
    public String newName;
    public String[] players = {};
}
