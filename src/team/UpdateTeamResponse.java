/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

public class UpdateTeamResponse {

    public boolean wasNameChanged, werePlayersSorted;
    public int addedPlayersCount, deletedPlayersCount;
}
