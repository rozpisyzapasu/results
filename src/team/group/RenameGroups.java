/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team.group;

import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.match.Group;
import sportsscheduler.results.match.Matches;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class RenameGroups implements Usecase<String[], RenameGroupsPresenter> {

    public void execute(String[] newGroups, RenameGroupsPresenter presenter) {
        Matches matches = Context.currentSchedule.matches;
        Group[] groups = matches.getGroups();
        if (isAtLeastOneGroupChanged(groups, newGroups)) {
            if (areNamesUnique(newGroups)) {
                matches.renameGroups(newGroups);
                presenter.groupsRenamed();
            } else {
                presenter.nonUniqueGroupNames();
            }
        } else {
            presenter.nothingChanged();
        }
    }

    private boolean isAtLeastOneGroupChanged(Group[] groups, String[] newNames) {
        for (int i = 0; i < groups.length; i++) {
            if (!groups[i].name.equals(newNames[i])) {
                return true;
            }
        }
        return false;
    }

    private boolean areNamesUnique(String[] groups) {
        Set<String> uniqueNames = Arrays.stream(groups).collect(Collectors.toSet());
        return uniqueNames.size() == groups.length;
    }
}
