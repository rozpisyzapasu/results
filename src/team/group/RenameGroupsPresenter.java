/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team.group;

public interface RenameGroupsPresenter {

    void nothingChanged();

    void nonUniqueGroupNames();

    void groupsRenamed();
}
