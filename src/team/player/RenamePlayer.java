/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team.player;

import sportsscheduler.results.Context;
import sportsscheduler.results.Usecase;
import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.Team;

public class RenamePlayer implements Usecase<RenamePlayerRequest, RenamePlayerPresenter> {

    public void execute(RenamePlayerRequest request, RenamePlayerPresenter presenter) {
        Matches matches = Context.currentSchedule.matches;
        if (matches.hasTeam(request.team)) {
            Team t = matches.getTeam(request.team);
            if (t.hasPlayer(request.player)) {
                if (!t.hasPlayer(request.newName)) {
                    t.renamePlayer(request.player, request.newName);
                    presenter.playerRenamed();
                } else {
                    presenter.nameAlreadyExists();
                }
            } else {
                presenter.nonExistentPlayer();
            }
        } else {
            presenter.nonExistentTeam();
        }
    }
}
