/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team.player;

public interface RenamePlayerPresenter {

    void nonExistentTeam();

    void nonExistentPlayer();

    void nameAlreadyExists();

    void playerRenamed();
}
