/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team.player;

public class RenamePlayerRequest {
    public String team;
    public String player;
    public String newName;
}
