/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.out.exporter.ExportPresenter;
import sportsscheduler.results.out.exporter.JsonWriter.WriteResult;

public class ExportSpy implements ExportPresenter {

    public enum ActionResult {FAILURE, SUCCESS}

    public ActionResult result;

    public void scheduleNotSaved(WriteResult result) {
        this.result = ActionResult.FAILURE;
    }

    public void scheduleExported() {
        result = ActionResult.SUCCESS;
    }
}
