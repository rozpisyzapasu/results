/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.team.group.RenameGroupsPresenter;

public class GroupsSpy implements RenameGroupsPresenter {

    public enum ActionResult {NO_CHANGE, NON_UNIQUE_GROUPS, SUCCESS};
    public ActionResult result;

    public void nothingChanged() {
        result = ActionResult.NO_CHANGE;
    }

    public void nonUniqueGroupNames() {
        result = ActionResult.NON_UNIQUE_GROUPS;
    }

    public void groupsRenamed() {
        result = ActionResult.SUCCESS;
    }
}
