/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.team.player.RenamePlayerPresenter;

public class PlayerSpy implements RenamePlayerPresenter {

    public enum ActionResult {NON_EXISTENT_TEAM, NON_EXISTENT_PLAYER, SUCCESS, EXISTING_NAME}

    public ActionResult result;

    public void nonExistentTeam() {
        result = ActionResult.NON_EXISTENT_TEAM;
    }

    public void nonExistentPlayer() {
        result = ActionResult.NON_EXISTENT_PLAYER;
    }

    public void nameAlreadyExists() {
        result = ActionResult.EXISTING_NAME;
    }

    public void playerRenamed() {
        result = ActionResult.SUCCESS;
    }
}
