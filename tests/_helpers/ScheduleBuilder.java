/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.out.importer.build.MatchesBuilder;
import sportsscheduler.results.points.TablePoints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class ScheduleBuilder extends MatchesBuilder {

    public static ScheduleBuilder empty() {
        List<String> noTeams = new ArrayList<>();
        return teams(noTeams);
    }

    public static ScheduleBuilder teams(String... teams) {
        ArrayList<String> names = new ArrayList<>(asList(teams));
        return teams(names);
    }

    public static ScheduleBuilder teams(TablePoints points, String... teams) {
        ArrayList<String> names = new ArrayList<>(asList(teams));
        return new ScheduleBuilder(names, points);
    }

    public static ScheduleBuilder teams(List<String> names) {
        return new ScheduleBuilder(names);
    }

    private final Schedule schedule = new Schedule();

    public ScheduleBuilder(List<String> teams) {
        this(teams, TablePoints.defaultPoints());
    }

    public ScheduleBuilder(List<String> teams, TablePoints points) {
        super(points);
        createSchedule(points);
        loadTeams(teams);
    }

    private void createSchedule(TablePoints points) {
        schedule.name = "";
        schedule.matches = super.build();
        schedule.settings = new ScheduleSettings();
        schedule.settings.points = points;
        Context.currentSchedule = schedule;
    }

    private void loadTeams(List<String> teams) {
        String groupName = "Test group";
        for (String team : teams) {
            addTeam(team, groupName);
        }
    }

    public ScheduleBuilder name(String name) {
        schedule.name = name;
        return this;
    }

    public ScheduleBuilder type(String type) {
        schedule.settings.scheduleType = type;
        return this;
    }

    public ScheduleBuilder periods(int periodsCount) {
        schedule.settings.periodsCount = periodsCount;
        return this;
    }

    public ScheduleBuilder fields(int fieldsCount) {
        schedule.settings.fieldsCount = fieldsCount;
        return this;
    }

    public ScheduleBuilder referees() {
        schedule.settings.hasReferees = true;
        return this;
    }

    public ScheduleBuilder dates() {
        schedule.settings.hasMatchDate = true;
        return this;
    }

    public ScheduleBuilder matchPeriods(String[] names) {
        schedule.settings.matchPeriods = Arrays.asList(names);
        return this;
    }

    public ScheduleBuilder teamStatsNames(String[] names) {
        schedule.settings.teamStatsNames = Arrays.asList(names);
        return this;
    }

    public ScheduleBuilder playerStatsNames(String[] names) {
        schedule.settings.playerStatsNames = Arrays.asList(names);
        return this;
    }

    public ScheduleBuilder groups(Map<String, String[]> groups) {
        for (Map.Entry<String, String[]> e : groups.entrySet()) {
            for (String team : e.getValue()) {
                schedule.matches.changeTeamGroup(team, e.getKey());
            }
        }
        return this;
    }
}