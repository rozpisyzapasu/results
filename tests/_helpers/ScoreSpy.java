/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.score.ScorePresenter;

public class ScoreSpy implements ScorePresenter {

    public enum ActionResult {NEGATIVE_SCORE, SCORE_CHANGED, NO_CHANGE, NON_EXISTENT}

    public ActionResult result;

    public void nonExistentMatch() {
        result = ActionResult.NON_EXISTENT;
    }

    public void negativeScore() {
        result = ActionResult.NEGATIVE_SCORE;
    }

    public void scoreChanged() {
        result = ActionResult.SCORE_CHANGED;
    }

    public void noChangeInScore() {
        result = ActionResult.NO_CHANGE;
    }
}
