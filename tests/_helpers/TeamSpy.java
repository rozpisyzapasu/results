/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.team.DeleteTeamPresenter;
import sportsscheduler.results.team.UpdateTeamPresenter;
import sportsscheduler.results.team.UpdateTeamResponse;

public class TeamSpy implements UpdateTeamPresenter, DeleteTeamPresenter {

    public boolean nothingChanged, wasNameChanged, werePlayersSorted, wasTeamDeleted;
    public int addedPlayersCount, deletedPlayersCount;

    public void nonExistentTeam() {
        wasTeamDeleted = false;
    }

    public void nothingChanged() {
        nothingChanged = true;
    }

    public void successfulChange(UpdateTeamResponse response) {
        wasNameChanged = response.wasNameChanged;
        addedPlayersCount = response.addedPlayersCount;
        deletedPlayersCount = response.deletedPlayersCount;
        werePlayersSorted = response.werePlayersSorted;
    }

    public void successfulDelete() {
        wasTeamDeleted = true;
    }
}
