/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.overview.PresentableTeam;
import sportsscheduler.results.stats.table.PresentableTeamResult;

import java.util.List;
import java.util.ArrayList;

public class ToArray {

    public static String[] extractTeamNames() {
        List<String> names = new ArrayList<>();
        for (PresentableTeam t : Context.schedule.teams) {
            names.add(t.name);
        }
        return names.toArray(new String[names.size()]);
    }

    public static String[] extractTeamsInTable() {
        List<String> names = new ArrayList<>();
        for (PresentableTeamResult t : Context.firstClassicTable()) {
            names.add(t.team);
        }
        return names.toArray(new String[names.size()]);
    }

    public static String[] extractPlayers() {
        List<String> names = new ArrayList<>();
        for (PresentablePlayer t : Context.players) {
            names.add(t.name);
        }
        return names.toArray(new String[names.size()]);
    }

    public static String[] extractPlayersAndTeams() {
        List<String> names = new ArrayList<>();
        for (PresentablePlayer t : Context.players) {
            names.add(String.format("%s (%s)", t.name, t.team));
        }
        return names.toArray(new String[names.size()]);
    }
}
