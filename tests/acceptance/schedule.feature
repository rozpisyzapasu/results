
Feature: Schedule

  Scenario: Schedule with no match results
    Given schedule with two teams with one player, two periods, matches without score
    When I access schedule details
    Then detail contains team names and number of matches
    And matches contains all matches
    And table contains two team rows with no data
    And players contain two players with zero goals

  Scenario: Add match score
    Given match without result
    When home team won the match
    Then matches contain non drawn match
    And home team has one win in table

  Scenario: Edit match score
    Given existing drawn match
    When away team won the match
    Then matches contain non drawn match
    And home team has one loss and zero draw in table

  Scenario: Delete match score
    Given existing drawn match
    When match result is deleted
    Then matches contain match without score
    And table contains two team rows with no data

  Scenario: Match with scorers
    Given schedule with two teams with two players and one match
    When one home player scores two goals and away player one goal
    And I access schedule details
    Then chart is sorted by goals count
    And match also contains scorers in team

  Scenario: Delete team
    Given schedule with three teams
    When first team is deleted
    And last match score is filled
    Then all matches are played
