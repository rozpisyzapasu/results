/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package acceptance.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sportsscheduler.results.*;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.score.ScoreRequest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ToArray.extractPlayersAndTeams;

public class MatchPlayersSteps {

    @Given("^schedule with two teams with two players and one match$")
    public void givenScheduleWithTeamAndPlayers() throws Throwable {
        ScheduleBuilder
                .teams("A", "B")
                .players("A", new String[]{"2G player", "0G player"})
                .players("B", new String[]{"1G player", "0G player"})
                .match("A", "B");
    }

    @When("^one home player scores two goals and away player one goal$")
    public void whenPlayersScoreGoals() throws Throwable {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;
        r.goalsHome = 2;
        r.goalsAway = 1;
        r.players.addGoals("2G player", "A", 2);
        r.players.addGoals("1G player", "B", 1);
        r.players.addGoals("0G player", "A", 0);

        ScoreSpy spy = new ScoreSpy();
        UseCaseInvoker.execute(UseCaseInvoker.MATCH_SCORE, r, spy);
    }

    @Then("^chart is sorted by goals count$")
    public void thenChartIsSorted() throws Throwable {
        String[] players = extractPlayersAndTeams();
        assertThat(players, is(new String[] {
                "2G player (A)",
                "1G player (B)",
                "0G player (A)",
                "0G player (B)"
        }));
    }

    @And("^match also contains scorers in team$")
    public void thenMatchHasAllPlayersInEachTeam() throws Throwable {
        PresentableMatch match = Context.schedule.matches.iterator().next();
        assertThat(match.home.getScorers(), hasSize(1));
        assertThat(match.away.getAllPlayers(), hasSize(2));
    }
}
