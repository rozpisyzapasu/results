/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package acceptance.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sportsscheduler.results.*;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.score.ScoreRequest;

import java.util.Iterator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ToArray.extractTeamNames;
import static sportsscheduler.results.ToArray.extractTeamsInTable;

public class ScheduleSteps {

    @Given("^schedule with two teams with one player, two periods, matches without score$")
    public void basicTwoTeamsSchedule() throws Throwable {
        ScheduleBuilder.teams("Team A", "Team B")
                .match("Team A", "Team B").match("Team A", "Team B")
                .players("Team A", new String[]{"Player A"})
                .players("Team B", new String[]{"Player B"});
    }

    @Given("^match without result$")
    public void givenMatchWithoutResult() throws Throwable {
        ScheduleBuilder.teams("Team A", "Team B").match("Team A", "Team B");
    }

    @Given("^existing drawn match$")
    public void givenDrawnMatch() throws Throwable {
        ScheduleBuilder.teams("Team A", "Team B")
                .playedMatch("Team A", "Team B", 1, 1);
    }

    @Given("^schedule with three teams$")
    public void scheduleWithThreeTeams() {
        ScheduleBuilder
                .teams("Team C", "Team A", "Team B")
                .match("Team A", "Team C")
                .match("Team B", "Team C")
                .match("Team A", "Team B");
    }

    @When("^first team is deleted$")
    public void firstTeamIsDeleted() {
        String team = Context.currentSchedule.matches.getTeams().iterator().next().name;
        UseCaseInvoker.execute(UseCaseInvoker.TEAM_DELETE, team, new TeamSpy());
    }

    @When("^I access schedule details$")
    public void accessScheduleDetails() throws Throwable {
        Context.reload();
    }

    @Then("^detail contains team names and number of matches$")
    public void scheduleInformation() throws Throwable {
        assertThat(extractTeamNames(), is(new String[]{"Team A", "Team B"}));
        assertThat(Context.schedule.matchesCount, is(2));
    }

    @Then("^matches contains all matches$")
    public void matches() throws Throwable {
        for (PresentableMatch m : Context.schedule.matches) {
            assertFalse(m.isPlayed);
        }
    }

    @Then("^all matches are played$")
    public void allMatchesArePlayed() throws Throwable {
        for (PresentableMatch m : Context.schedule.matches) {
            assertTrue(m.isPlayed);
        }
    }

    @Then("^table contains two team rows with no data$")
    public void tableWithTeamsWithNoData() throws Throwable {
        assertThat(extractTeamsInTable(), is(new String[] {"Team A", "Team B"}));
    }

    @Then("^players contain two players with zero goals$")
    public void noActivePlayers() throws Throwable {
        for (PresentablePlayer p : Context.players) {
            assertThat(p.goalsCount, is(0));
        }
    }

    @When("^home team won the match$")
    public void addScore() throws Throwable {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;
        r.goalsHome = 5;
        r.goalsAway = 0;

        executeAndAssert(r);
    }

    @When("^away team won the match$")
    public void editScore() throws Throwable {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;
        r.goalsHome = 0;
        r.goalsAway = 5;

        executeAndAssert(r);
    }

    @When("^match result is deleted$")
    public void deleteScore() throws Throwable {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;

        executeAndAssert(r);
    }

    @When("^last match score is filled$")
    public void lastMatchScoreIsFilled() throws Throwable {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 2;
        r.goalsHome = 0;
        r.goalsAway = 0;

        executeAndAssert(r);
    }

    private void executeAndAssert(ScoreRequest r) throws Throwable {
        UseCaseInvoker.execute(UseCaseInvoker.MATCH_SCORE, r, new ScoreSpy());
        accessScheduleDetails();
    }

    @Then("^matches contain non drawn match$")
    public void matchesHaveMatchWithScore() throws Throwable {
        PresentableMatch match = Context.schedule.matches.iterator().next();
        assertTrue(match.isPlayed);
        assertThat(match.home.goalsCount, not(match.away.goalsCount));
    }

    @Then("^matches contain match without score$")
    public void noScoreInMatch() throws Throwable {
        assertFalse(Context.schedule.matches.iterator().next().isPlayed);
    }

    @And("^home team has one win in table$")
    public void teamsInTable() throws Throwable {
        assertThat(Context.firstClassicTable().iterator().next().totalWins(), is(1));
    }

    @And("^home team has one loss and zero draw in table$")
    public void updatedTable() throws Throwable {
        PresentableTeamResult homeTeam = getHomeTeamResult();
        assertThat(homeTeam.totalLosses(), is(1));
        assertThat(homeTeam.drawsCount, is(0));
    }

    private PresentableTeamResult getHomeTeamResult() {
        Iterator<PresentableTeamResult> iterator = Context.firstClassicTable().iterator();
        iterator.next();
        return iterator.next();
    }
}
