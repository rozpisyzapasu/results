/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package acceptance.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sportsscheduler.results.*;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.team.UpdateTeamRequest;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ToArray.extractTeamNames;
import static sportsscheduler.results.ToArray.extractTeamsInTable;

import static org.junit.Assert.assertTrue;

public class TeamNameSteps {

    private final String oldName = "Team A", newName = "New A";

    @Given("^team in schedule, matches and table$")
    public void givenExistingTeam() throws Throwable {
        MatchBuilder match = MatchBuilder.match(oldName, "Team B");
        match.referee(oldName);

        ScheduleBuilder.teams(oldName, "Team B")
                .match(match)
                .playedMatch("Team B", oldName, 5, 2);
    }

    @When("^team name is successfully changed to new name$")
    public void whenNameIsChanged() throws Throwable {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = oldName;
        r.newName = newName;

        TeamSpy spy = new TeamSpy();
        UseCaseInvoker.execute(UseCaseInvoker.TEAM_UPDATE, r, spy);
        assertTrue(spy.wasNameChanged);
    }

    @Then("^new name is used everywhere$")
    public void thenNewNameIsUsedEverywhere() throws Throwable {
        assertSettingsAreUpdated();
        assertTeamsAreUpdated();
        assertMatchesAreUpdated();
        assertRefereesAreUpdated();
        assertTableIsUpdated();
    }

    private void assertSettingsAreUpdated() {
        String[] teamsInGroup = Context.schedule.groups.values().iterator().next();
        assertNameWasChanged(teamsInGroup);
    }

    private void assertTeamsAreUpdated() {
        String[] names = extractTeamNames();
        assertNameWasChanged(names);
    }

    private void assertMatchesAreUpdated() {
        for (PresentableMatch m : Context.schedule.matches) {
            assertNameWasChanged(new String[]{m.home.team, m.away.team});
        }
    }

    private void assertRefereesAreUpdated() {
        for (PresentableMatch m : Context.schedule.matches) {
            if (!m.attributes.referee.isEmpty()) {
                assertNameWasChanged(new String[]{m.attributes.referee});
            }
        }
    }

    private void assertTableIsUpdated() {
        String[] teams = extractTeamsInTable();
        assertNameWasChanged(teams);
    }

    private void assertNameWasChanged(String[] teamsArray) {
        List<String> teams = Arrays.asList(teamsArray);
        assertThat(teams, not(hasItem(oldName)));
        assertThat(teams, hasItem(newName));
    }
}
