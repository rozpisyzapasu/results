/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package acceptance.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sportsscheduler.results.*;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.overview.PresentableTeam;
import sportsscheduler.results.team.UpdateTeamRequest;
import sportsscheduler.results.team.player.RenamePlayerRequest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ToArray.extractPlayers;

public class TeamPlayersSteps {

    private final TeamSpy spy = new TeamSpy();
    private final String oldName = "Player", newName = "New Player";

    @Given("^team with no players$")
    public void givenEmptyTeam() throws Throwable {
        ScheduleBuilder.teams("Team A");
    }

    @Given("^team with one player$")
    public void givenTeamWithOnePlayer() throws Throwable {
        ScheduleBuilder
                .teams("Team A", "Team B")
                .players("Team A", new String[]{oldName})
                .match(
                    MatchBuilder.match("Team A", "Team B")
                        .scorer(oldName, 1)
                        .score(1, 0)
                );
    }

    @When("^I add two unique players to team$")
    public void whenNewPlayersAreCreated() throws Throwable {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = "Team A";
        r.players = new String[] {"Player 1", "Player 2"};

        UseCaseInvoker.execute(UseCaseInvoker.TEAM_UPDATE, r, spy);
    }

    @When("^all players are deleted$")
    public void whenPlayerIsDeleted() throws Throwable {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = "Team A";
        r.players = new String[0];

        UseCaseInvoker.execute(UseCaseInvoker.TEAM_UPDATE, r, spy);
    }

    @When("^player's name is renamed")
    public void whenPlayerIsRenamed() throws Throwable {
        RenamePlayerRequest r = new RenamePlayerRequest();
        r.team = "Team A";
        r.player = oldName;
        r.newName = newName;

        UseCaseInvoker.execute(UseCaseInvoker.PLAYER_RENAME, r, new PlayerSpy());
    }

    @Then("^result contains how many players were added$")
    public void thenPlayersCountsAreReturned() throws Throwable {
        assertThat(spy.addedPlayersCount, is(2));
    }

    @And("^new players are in chart$")
    public void thenNewPlayersAreInChart() throws Throwable {
        assertThat(extractPlayers(), is(new String[]{"Player 1", "Player 2"}));
    }

    @Then("^team has no players$")
    public void thenTeamHasNoPlayers() throws Throwable {
        for (PresentableTeam team : Context.schedule.teams) {
            assertThat(team.players, emptyIterable());
        }
    }

    @Then("^new player name is used everywhere$")
    public void thenNewPlayerNameIsUsedEverywhere() throws Throwable {
        assertTeamWasChanged();
        assertChartWasChanged();
        assertMatchWasChanged();
    }

    private void assertTeamWasChanged() {
        PresentableTeam team = Context.schedule.teams.iterator().next();
        String player = team.players.iterator().next().name;
        assertNameWasChanged(player);
    }

    private void assertChartWasChanged() {
        PresentablePlayer player = Context.players.iterator().next();
        assertNameWasChanged(player.name);
    }

    private void assertMatchWasChanged() {
        PresentableMatch match = Context.schedule.matches.iterator().next();
        PresentablePlayer scorer = match.home.getScorers().iterator().next();
        assertNameWasChanged(scorer.name);
    }

    private void assertNameWasChanged(String currentName) {
        assertThat(currentName, is(not(oldName)));
        assertThat(currentName, is(newName));
    }
}
