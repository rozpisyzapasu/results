Feature: Team

  Scenario: Update team name
    Given team in schedule, matches and table
    When team name is successfully changed to new name
    And I access schedule details
    Then new name is used everywhere

  Scenario: Create team players
    Given team with no players
    When I add two unique players to team
    And I access schedule details
    Then result contains how many players were added
    And new players are in chart

  Scenario: Delete player
    Given team with one player
    When all players are deleted
    And I access schedule details
    Then team has no players

  Scenario: Update player
    Given team with one player
    When player's name is renamed
    And I access schedule details
    Then new player name is used everywhere