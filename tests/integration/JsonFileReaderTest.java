/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

public class JsonFileReaderTest extends JsonReaderTest {

    protected String[] getValidPaths() {
        return new String[] {
                "tests/_json/groups.json"
        };
    }

    protected String[] getInvalidPaths() {
        return new String[] {
                "non-existing-file",
                "tests/_json/",
                "license"
        };
    }
}
