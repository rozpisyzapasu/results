/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;
import org.junit.Test;
import sportsscheduler.results.out.exporter.JsonWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JsonFileWriterTest {

    @Test
    public void shouldFailForExistingDirectory() {
        String existingDirectory = "tests/";
        boolean wasSaved = writeFile(existingDirectory);
        assertFalse(wasSaved);
    }

    @Test
    public void shouldCreateNewFileInUTF8() throws IOException {
        String newFile = "file.json";
        assertFileWasCreated(newFile);
        assertThat(getEncoding(newFile), is("UTF8"));
        deleteCreatedFile(newFile);
    }

    @Test
    public void shouldOverwriteExistingFile() {
        String existingFile = "file.json";
        createFile(existingFile);
        assertFileWasCreated(existingFile);
        deleteCreatedFile(existingFile);
    }

    private void createFile(String path) {
        try {
            Files.write(Paths.get(path), new byte[0]);
        } catch (IOException e) {
            throw new RuntimeException("Temp file was not created", e);
        }
    }

    private void assertFileWasCreated(String existingFile) {
        assertTrue(writeFile(existingFile));
    }

    private String getEncoding(String newFile) throws IOException {
        File in = Paths.get(newFile).toFile();
        InputStreamReader r = new InputStreamReader(new FileInputStream(in));
        String encoding = r.getEncoding();
        r.close();
        return encoding;
    }

    private boolean writeFile(String path) {
        JsonWriter gateway = new JsonFileWriter();
        return gateway.write(path, JsonEntity.emptyObject()) == JsonWriter.WriteResult.OK;
    }

    private void deleteCreatedFile(String newFilePath) {
        try {
            Files.delete(Paths.get(newFilePath));
        } catch (IOException e) {
            throw new RuntimeException("Temp file was not annihilated", e);
        }
    }
}
