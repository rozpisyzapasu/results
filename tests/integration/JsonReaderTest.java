/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

import org.easygson.JsonEntity;
import org.hamcrest.Matcher;
import org.junit.Test;
import sportsscheduler.results.out.importer.JsonReader;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public abstract class JsonReaderTest {

    @Test
    public void whenInputIsValidThenReturnsObject() {
        for (String path : getValidPaths()) {
            assertJson(path, not("{}"));
        }
    }

    @Test
    public void whenInputIsInvalidThenReturnsEmptyObject() {
        for (String path : getInvalidPaths()) {
            assertJson(path, is("{}"));
        }
    }

    private void assertJson(String path, Matcher<String> matcher) {
        JsonReader reader = new FileWebReader();
        JsonEntity json = reader.read(path).json;
        assertThat(json.toString(), matcher);
    }

    protected abstract String[] getValidPaths();

    protected abstract String[] getInvalidPaths();
}
