/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.system;

public class JsonWebReaderTest extends JsonReaderTest {

    protected String[] getValidPaths() {
        return new String[] {
                "https://www.rozpisyzapasu.cz/api/",
                "http://www.rozpisyzapasu.cz/api/", // follow redirect to https
        };
    }

    protected String[] getInvalidPaths() {
        return new String[] {
                "http://www.invalid-page.404page",
                "http://www.google.com"
        };
    }
}
