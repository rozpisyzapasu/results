/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results;

import org.junit.Test;
import sportsscheduler.results.points.TablePoints;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TablePointsTest {

    @Test
    public void defaultPointsAreValid() {
        TablePoints p = TablePoints.defaultPoints();
        assertThat(p.areValid(), is (true));
    }

    @Test
    public void invalidWhenPointsAreNotInDescendingOrder() {
        TablePoints p = new TablePoints();
        p.win = 2;
        p.loss = p.win + 1;
        assertThat(p.areValid(), is (false));
    }

    @Test
    public void invalidWhenLossIsNegative() {
        TablePoints p = TablePoints.defaultPoints();
        p.loss = -1;
        assertThat(p.areValid(), is (false));
    }
}
