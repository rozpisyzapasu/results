/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import org.junit.Test;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.ScoreSpy;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.score.ScoreRequest;

import java.util.ArrayList;
import java.util.Iterator;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ChangePlayoffScoreTest {

    @Test
    public void favoriteWon() {
        givenPlayoff(MatchBuilder.match("A", "D").seed(1, 4));
        whenFirstMatchHasScore(1, 0);
        finaleIsPlayedBy("A", "B");
    }

    @Test
    public void awayOutsiderWon() {
        givenPlayoff(MatchBuilder.match("A", "D").seed(1, 4));
        whenFirstMatchHasScore(0, 1);
        finaleIsPlayedBy("D", "B");
    }

    @Test
    public void homeOutsiderWon() {
        givenPlayoff(MatchBuilder.match("D", "A").seed(4, 1));
        whenFirstMatchHasScore(1, 0);
        finaleIsPlayedBy("D", "B");
    }

    @Test
    public void favoriteWonAfterProtest() {
        givenPlayoff(MatchBuilder.match("A", "D").seed(1, 4));
        whenFirstMatchHasScore(0, 1);
        whenFirstMatchHasScore(5, 0);
        finaleIsPlayedBy("A", "B");
    }

    @Test
    public void noWinnerIsKnown() {
        givenPlayoff(MatchBuilder.match("A", "D").seed(1, 4));
        whenFirstMatchHasScore(0, 0);
        finaleIsPlayedBy("A", "B");
    }

    @Test
    public void recalculateStatsWhenTeamIsChanged() {
        givenPlayoff(MatchBuilder.match("A", "D").seed(1, 4));
        addScore(0, 5, 4);
        addScore(1, 4, 5);
        addScore(2, 5, 3);
        assertThat(countPlayedMatches(), is("A2, B2, C1, D1"));
        addScore(0, 5, 8);
        assertThat(countPlayedMatches(), is("A1, B2, C1, D2"));
    }

    private String countPlayedMatches() {
        ArrayList<String> matches = new ArrayList<>();
        for (Team t : Context.currentSchedule.matches.getTeams()) {
            matches.add(t.name + t.countMatches());
        }
        return String.join(", ", matches);
    }

    public void givenPlayoff(MatchBuilder firstMatch) {
        ScheduleBuilder
                .teams("A", "B", "C", "D")
                .match(firstMatch)
                .match(MatchBuilder.match("C", "B").seed(3, 2))
                .match(MatchBuilder.match("A", "B").seed(1, 2));
    }

    private void finaleIsPlayedBy(String expectedHome, String expectedAway) {
        Match finale = Context.currentSchedule.matches.getMatch(2);
        Iterator<TeamInMatch> teams = finale.getTeams().iterator();
        Team home = teams.next().getTeam();
        Team away = teams.next().getTeam();
        assertThat(home.name, is(expectedHome));
        assertThat(away.name, is(expectedAway));
    }

    private void whenFirstMatchHasScore(int goalsHome, int goalsAway) {
        addScore(0, goalsHome, goalsAway);
    }

    private void addScore(int match, int goalsHome, int goalsAway) {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = match;
        r.goalsHome = goalsHome;
        r.goalsAway = goalsAway;
        r.resultType = ResultType.NORMAL;
        ScoreSpy spy = new ScoreSpy();
        UseCaseInvoker.execute(UseCaseInvoker.MATCH_SCORE, r, spy);
    }
}
