/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.ScoreSpy;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.score.ScoreRequest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ScoreSpy.ActionResult;

public class ChangeScoreTest {

    private ActionResult result;

    @Test
    public void whenMatchNotExists() {
        givenEmptySchedule();
        addScore(0, 0);
        assertResult(ActionResult.NON_EXISTENT);
    }

    @Test
    public void whenScoreIsNegative() {
        givenScheduleWithOneMatch();
        addScore(-2, 5);
        assertResult(ActionResult.NEGATIVE_SCORE);
    }

    @Test
    public void whenScoreIsSuccessfullyAdded() {
        givenScheduleWithOneMatch();
        addScore(1, 1);
        assertResult(ActionResult.SCORE_CHANGED);
    }

    @Test
    public void whenScoreIsNotChanged() {
        givenScheduleWithOneMatch();
        addScore(1, 1);
        addScore(1, 1);
        assertResult(ActionResult.NO_CHANGE);
    }

    @Test
    public void whenScoreIsSuccessfullyUpdated() {
        givenScheduleWithOneMatch();
        addScore(1, 1);
        addScore(2, 5);
        assertResult(ActionResult.SCORE_CHANGED);
    }

    @Test
    public void whenNonExistentScoreIsDeleted() {
        givenScheduleWithOneMatch();
        deleteScore();
        assertResult(ActionResult.NO_CHANGE);
    }

    @Test
    public void whenScoreIsSuccessfullyDeleted() {
        givenScheduleWithOneMatch();
        addScore(1, 1);
        deleteScore();
        assertResult(ActionResult.SCORE_CHANGED);
    }

    @Test
    public void whenResultTypeIsChanged() {
        givenScheduleWithOneMatch();
        addScore(1, 1, ResultType.NORMAL);
        addScore(1, 1, ResultType.EXTRA_TIME);
        assertResult(ActionResult.SCORE_CHANGED);
    }

    private void givenEmptySchedule() {
        ScheduleBuilder.empty();
    }

    private void givenScheduleWithOneMatch() {
        ScheduleBuilder.teams("Team A", "Team B").match("Team A", "Team B");
    }

    private void addScore(int scoreHome, int scoreAway) {
        addScore(scoreHome, scoreAway, ResultType.NORMAL);
    }

    private void addScore(int scoreHome, int scoreAway, ResultType result) {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;
        r.goalsHome = scoreHome;
        r.goalsAway = scoreAway;
        r.resultType = result;
        executeUseCase(r);
    }

    private void deleteScore() {
        ScoreRequest r = new ScoreRequest();
        r.idMatch = 0;
        executeUseCase(r);
    }

    private void executeUseCase(ScoreRequest r) {
        ScoreSpy spy = new ScoreSpy();
        UseCaseInvoker.execute(UseCaseInvoker.MATCH_SCORE, r, spy);
        result = spy.result;
    }

    private void assertResult(ActionResult expectedResult) {
        assertThat(result, is(expectedResult));
    }
}
