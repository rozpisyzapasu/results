/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import org.junit.Test;
import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.score.MatchPlayers;
import sportsscheduler.results.score.type.MatchResultEvaluator;
import sportsscheduler.results.score.type.NoScore;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static sportsscheduler.results.score.type.ExistingScore.givenScore;

public class MatchResultEvaluatorTest {

    private Team team = Team.test("irrelevant name", "irrelevant group");
    private TablePoints points = TablePoints.defaultPoints();
    private MatchResultEvaluator evaluator = new MatchResultEvaluator(points);

    @Test
    public void shouldDoNothingWithNoScore() {
        evaluator.add(team, NoScore.noScore);
        assertThat(team.points, is(0));
        assertThat(team.countMatches(), is(0));
        assertThat(team.wins.count(), is(0));
        assertThat(team.goalsFor, is(0));;
        assertThat(team.goalsAgainst, is(0));
    }

    @Test
    public void shouldAddPointsForWin() {
        evaluator.add(team, givenScore(2, 0));
        assertThat(team.countMatches(), is(1));
        assertThat(team.points, is(3));
        assertThat(team.wins.count(ResultType.NORMAL), is(1));
        assertThat(team.goalsFor, is(2));;
        assertThat(team.goalsAgainst, is(0));
    }

    @Test
    public void shouldSubtractPoints() {
        evaluator.subtract(team, givenScore(1, 1));
        assertThat(team.points, is(-1));
        assertThat(team.drawsCount, is(-1));
        assertThat(team.goalsFor, is(-1));;
        assertThat(team.goalsAgainst, is(-1));
    }

    @Test
    public void shouldNotUpdateGoalsForWinByDefault() {
        points.addResultScore(ResultType.WIN_BY_DEFAULT, TablePoints.defaultPoints());
        evaluator.add(team, givenScore(ResultType.WIN_BY_DEFAULT, 5, 2));
        assertThat(team.goalsFor, is(0));;
        assertThat(team.goalsAgainst, is(0));
    }

    @Test
    public void shouldAddPointsForWinInOvertimeOrShootout() {
        evaluator.add(team, givenScore(ResultType.EXTRA_TIME, 2, 0));
        assertThat(team.countMatches(), is(1));
        assertThat(team.wins.count(ResultType.EXTRA_TIME), is(1));
        assertThat(team.wins.count(ResultType.NORMAL), is(0));
    }

    @Test
    public void shouldSubtractPointsForWinInOvertimeOrShootout() {
        evaluator.subtract(team, givenScore(ResultType.SHOOTOUT, 1, 2));
        assertThat(team.losses.count(ResultType.SHOOTOUT), is(-1));
        assertThat(team.losses.count(ResultType.NORMAL), is(0));
    }

    @Test
    public void shouldResultScoreHashHighestPriorityThanMatchScore() {
        ResultType resultType = ResultType.EXTRA_TIME;
        points.addResultScore(resultType, TablePoints.defaultPoints());
        givenTeamGetsExtraPointsWhenScoreIsFiveTwo();
        evaluator.add(team, givenScore(resultType, 5, 2));
        assertThat(team.points, is(3));
    }

    @Test
    public void shouldUseMatchScorePointsWhenGoalsAreEqual() {
        givenTeamGetsExtraPointsWhenScoreIsFiveTwo();
        evaluator.add(team, givenScore(5, 2));
        assertThat(team.points, is(10));
    }

    @Test
    public void shouldUseMatchScorePointsWhenGoalsAreEqualInAnyOrder() {
        givenTeamGetsExtraPointsWhenScoreIsFiveTwo();
        evaluator.add(team, givenScore(2, 5));
        assertThat(team.points, is(2));
    }

    @Test
    public void shouldSumGoalsForEachPlayer() {
        String playerName = "player";
        team.addPlayer(playerName);
        team.getPlayer(playerName).goalsCount = 1;
        MatchPlayers players = new MatchPlayers();
        players.addGoals(playerName, team.name, 1);;
        evaluator.add(team, givenScore(team, 1, 1, players));
        assertThat(team.getPlayer(playerName).goalsCount, is(2));
    }

    private void givenTeamGetsExtraPointsWhenScoreIsFiveTwo() {
        TablePoints.MatchScorePoints p = new TablePoints.MatchScorePoints(5, 2);
        p.points.win = 10;
        p.points.draw = 2;
        p.points.loss = 2;
        points.addMatchScore(p);
    }
}
