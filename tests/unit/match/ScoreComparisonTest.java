/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.match;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import sportsscheduler.results.score.MatchPlayers;

import static sportsscheduler.results.score.type.ExistingScore.givenScore;
import static sportsscheduler.results.score.type.NoScore.noScore;

public class ScoreComparisonTest {

    @Test
    public void basicSameScoresWithoutPlayers() {
        assertSameScores(givenScore(5, 5), givenScore(5, 5));
        assertSameScores(noScore, noScore);
    }

    @Test
    public void basicDifferentScoresWithoutPlayers() {
        assertDifferentScores(noScore, givenScore(0, 0));
        assertDifferentScores(givenScore(5, 5), givenScore(0, 0));
    }

    @Test
    public void sameScoreWithPlayers() {
        assertSameScores(players(), players());
        assertSameScores(players("A", "B"), players("A", "B"));
        assertSameScores(players("A", "B"), players("B", "A"));
    }

    @Test
    public void differentScoreWithPlayers() {
        assertDifferentScores(players(), players("A", "B"));
        assertDifferentScores(players("A-0", "B-1"), players("A-1", "B-1"));
        assertDifferentScores(players("A-0", "B-1"), players("B-1", "A-1"));
    }

    public static Score players(String... players) {
        MatchPlayers scorers = new MatchPlayers();
        Team team = Team.test("irrelevant home team", "");
        for (String player : players) {
            if (player.contains("-")) {
                String[] data = player.split("-");
                String name = data[0];
                int goalsCount = Integer.parseInt(data[1]);
                scorers.addGoals(name, team.name, goalsCount);
                team.addPlayer(name);
            } else {
                scorers.addGoals(player, team.name, 0);
                team.addPlayer(player);
            }
        }
        return givenScore(team, 5, 5, scorers);
    }

    public static void assertSameScores(Score a, Score b) {
        assertFalse(a.isDifferent(b));
        assertFalse(b.isDifferent(a));
    }

    public static void assertDifferentScores(Score a, Score b) {
        assertTrue(a.isDifferent(b));
        assertTrue(b.isDifferent(a));
    }
}
