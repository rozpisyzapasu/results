/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out;

import org.easygson.JsonEntity;
import org.junit.Before;
import org.junit.Test;
import sportsscheduler.results.*;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.out.exporter.JsonWriter.WriteResult;
import sportsscheduler.results.points.TablePoints;

import java.util.HashMap;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static sportsscheduler.results.ExportSpy.ActionResult;
import static sportsscheduler.results.out.importer.build.MatchBuilder.match;

public class ExportScheduleTest {

    private JsonEntity json;
    private ScheduleBuilder builder;

    @Before
    public void setUp() {
        TablePoints points = TablePoints.defaultPoints();
        points.addMatchScore(new TablePoints.MatchScorePoints(10,1));
        points.addResultScore(ResultType.EXTRA_TIME, TablePoints.defaultPoints());
        builder = (ScheduleBuilder) ScheduleBuilder
                .teams(points, "Team A", "Team B")
                .name("football")
                .type("scheduleType").periods(2).fields(1)
                .referees().dates().groups(
                        new HashMap<String, String[]>(){{
                            put("", new String[]{"Team A", "Team B"});
                        }}
                )
                .matchPeriods(new String[]{"1.set"})
                .teamStatsNames(new String[]{"Corner"})
                .playerStatsNames(new String[]{"Foul"})
                .players("Team A", new String[]{"P1", "P2"})
                .players("Team B", new String[]{"P3", "P4"})
                .match(
                        match("Team A", "Team B")
                                .score(2, 1)
                                .result(ResultType.EXTRA_TIME)
                                .scorer("P1", 2, "Team A").scorer("P3", 1)
                                .referee("Referee 1")
                                .date("1388571010")
                                .note("irrelevant text")
                                .teamStat("1.set", 2, 1)
                                .playerStat("P1", "Team A", "Foul", 3)

                )
                .match(match("Team A", "Team B").seed(1, 2));
    }

    @Test
    public void shouldExportDescription() {
        String expectedJson = "{\"scheduleName\":\"football\"}";
        assertThat(successfulExport("description").toString(), is(expectedJson));
    }

    @Test
    public void shouldExportScheduleInfo() {
        String expectedJson = "{" +
                    "\"scheduleType\":\"scheduleType\"," +
                    "\"teams\":[\"Team A\",\"Team B\"]," +
                    "\"periodsCount\":2," +
                    "\"fields\":{\"count\":1}," +
                    "\"duration\":{}," +
                    "\"referees\":{}," +
                    "\"points\":{" +
                        "\"win\":3," +
                        "\"draw\":1," +
                        "\"loss\":0," +
                        "\"scores\":[{" +
                            "\"win\":3," +
                            "\"draw\":1," +
                            "\"loss\":0," +
                            "\"goals\":[10,1]" +
                        "},{" +
                            "\"win\":3," +
                            "\"draw\":1," +
                            "\"loss\":0," +
                            "\"resultType\":\"EXTRA_TIME\"" +
                        "}]" +
                    "}," +
                    "\"units\":{" +
                        "\"team\":\"team\"," +
                        "\"match\":\"goal\"" +
                    "}," +
                    "\"stats\":{" +
                        "\"matchPeriods\":[\"1.set\"]," +
                        "\"team\":[\"Corner\"]," +
                        "\"player\":[\"Foul\"]" +
                    "}" +
                "}";
        assertThat(successfulExport("schedule").toString(), is(expectedJson));
    }

    @Test
    public void shouldExportPlayers() {
        String expectedJson = "{" +
                    "\"Team A\":[\"P1\",\"P2\"]," +
                    "\"Team B\":[\"P3\",\"P4\"]" +
                "}";
        assertThat(successfulExport("players").toString(), is(expectedJson));
    }

    @Test
    public void shouldExportMatch() {
        assertThat(exportMatch(0), is(
                "{" +
                    "\"teams\":[\"Team A\",\"Team B\"]," +
                    "\"seed\":[1,2]," +
                    "\"timestamp\":1388571010," +
                    "\"referee\":\"Referee 1\"," +
                    "\"result\":{" +
                        "\"type\":\"EXTRA_TIME\"," +
                        "\"goals\":[2,1]," +
                        "\"scorers\":[{" +
                            "\"player\":\"P1\"," +
                            "\"team\":\"Team A\"," +
                            "\"goals\":2" +
                        "}," + "{" +
                            "\"player\":\"P3\"," +
                            "\"team\":\"Team B\"," +
                            "\"goals\":1" +
                        "}]" +
                    "}," +
                    "\"stats\":[" +
                        "{" +
                            "\"type\":\"note\"," +
                            "\"match\":\"irrelevant text\"" +
                        "}," +
                        "{" +
                            "\"type\":\"1.set\"," +
                            "\"teams\":[2,1]" +
                        "}," +
                        "{" +
                            "\"type\":\"Foul\"," +
                            "\"player\":\"P1\"," +
                            "\"team\":\"Team A\"," +
                            "\"value\":3" +
                        "}" +
                    "]" +
                "}"
        ));
        assertThat(exportMatch(1), is("{\"teams\":[\"Team A\",\"Team B\"],\"seed\":[1,2]}"));
    }

    @Test
    public void shouldExportGroups() {
        builder = ScheduleBuilder.teams("Team A", "Team B", "Team C", "Team D");
        builder.groups(new HashMap<String, String[]>(){{
            put("A", new String[]{"Team A", "Team B"});
            put("B", new String[]{"Team C", "Team D"});
        }});
        assertThat(successfulExport("schedule").get("groups").toString(), is(
                "{" +
                    "\"A\":[\"Team A\",\"Team B\"]," +
                    "\"B\":[\"Team C\",\"Team D\"]" +
                "}"
        ));
    }

    @Test
    public void whenScheduleIsNotSaved() {
        Context.jsonWriter = (path, json) -> WriteResult.IO_ERROR;
        export(ActionResult.FAILURE);
    }

    private String exportMatch(int matchIndex) {
        JsonEntity firstRound = successfulExport("periods").get(0).get(0).get("matches");
        return firstRound.get(matchIndex).toString();
    }

    private JsonEntity successfulExport(String property) {
        Context.jsonWriter = (path, json) -> {this.json = json; return WriteResult.OK;};
        export(ActionResult.SUCCESS);
        return json.get(property);
    }

    private void export(ActionResult expectedResult) {
        ExportSpy presenter = new ExportSpy();
        UseCaseInvoker.execute(UseCaseInvoker.SCHEDULE_EXPORT, "path", presenter);
        assertThat(presenter.result, is(expectedResult));
        assertThat(Context.currentSchedule.isSaved, is(expectedResult == ActionResult.SUCCESS));
    }
}
