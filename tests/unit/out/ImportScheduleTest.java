/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out;

import org.junit.Test;
import sportsscheduler.results.*;
import sportsscheduler.results.out.system.Dates;
import sportsscheduler.results.out.system.JsonFileReader;
import sportsscheduler.results.out.system.JsonResult.JsonError;
import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.repository.InMemorySchedules;
import sportsscheduler.results.out.importer.ImportPresenter;
import sportsscheduler.results.match.*;
import sportsscheduler.results.score.type.ExistingScore;
import sportsscheduler.results.score.type.Scorer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ImportScheduleTest {

    private final String results = "results.json";
    private final String[] files = {
            "round-robin.json", "round-robin-advanced.json",
            "groups.json", "two-conferences.json",
            "playoff-consolation.json",
            "playoff-de.json",
            "playoff.json"
    };
    private Presenter presenter = new Presenter();

    @Test
    public void shouldLoadScheduleInfo() {
        ScheduleSettings s = importSchedule(files[1]).settings;
        assertThat(s.scheduleType, is("roundRobin/BalancedFields"));
        assertThat(s.periodsCount, is(1));
        assertThat(s.fieldsCount, is(2));
        assertTrue(s.hasReferees);
        assertTrue(s.hasMatchDate);
    }

    @Test
    public void customizeDatetimeFormat() {
        LocalDateTime date = Dates.timestampToDate("1404554400");
        assertThat(importSchedule(files[0]).settings.formatDate(date), is("05.07.2014 12:00"));
        assertThat(importSchedule(files[1]).settings.formatDate(date), is("12:00"));
    }

    @Test
    public void shouldCountPeriods() {
        ScheduleSettings s = importSchedule(files[5]).settings;
        assertThat(s.scheduleType, is("playoff/DoubleElimination"));
        assertThat(s.periodsCount, is(3));
    }

    @Test
    public void shouldLoadGroups() {
        Context.defaultGroup = "default";
        assertThat(importSchedule(files[1]).matches.getGroupNames(), contains("default"));
        assertThat(importSchedule(files[2]).matches.getGroupNames(), contains("A", "B"));
    }

    @Test
    public void shouldLoadResultType() {
        Matches matches = importSchedule(files[1]).matches;
        assertResultTypeIs(matches, 0, ResultType.NORMAL);
        assertResultTypeIs(matches, 1, ResultType.NORMAL);
        assertResultTypeIs(matches, 2, ResultType.NORMAL);
        assertResultTypeIs(matches, 3, ResultType.EXTRA_TIME);
        assertResultTypeIs(matches, 4, ResultType.SHOOTOUT);
        assertResultTypeIs(matches, 5, ResultType.WIN_BY_DEFAULT);
    }

    private void assertResultTypeIs(Matches matches, int idMatch, ResultType expectedType) {
        assertThat(matches.getMatch(idMatch).getScore().getResultType(), is(expectedType));
    }

    @Test
    public void shouldLoadPoints() {
        String defaultPoints = "3-1-0";
        assertThat(importTablePoints(0), is(defaultPoints)); // when schedule has no points
        assertThat(importTablePoints(2), is(defaultPoints)); // when points are not in descending order
        assertThat(importTablePoints(1), is("3-2-1")); // load valid points
    }

    @Test
    public void shouldLoadValidMatchPoints() {
        TablePoints points = importSchedule(files[1]).settings.points;
        assertThat(points.matchScores, hasSize(1));
        assertThat(pointsToString(points.matchScores.get(0).points), is("10-0-0"));
        assertThat(points.resultScores, aMapWithSize(1));
    }

    private String importTablePoints(int index) {
        TablePoints s = importSchedule(files[index]).settings.points;
        return pointsToString(s);
    }

    private String pointsToString(TablePoints s) {
        return String.format("%d-%d-%d", s.win, s.draw, s.loss);
    }

    @Test
    public void shouldLoadPlayers() {
        Iterable<Team> teams = importSchedule(results).matches.getTeams();
        for (Team t : teams) {
            assertThat(t.getPlayers(), iterableWithSize(2));
        }
    }

    @Test
    public void allExamplesHaveSixTeams() {
        for (String filename : files) {
            Matches matches = importSchedule(filename).matches;
            assertThat(matches.countTeams(), is(6));
        }
    }

    @Test
    public void allExamplesHaveAtLeastOneMatch() {
        for (String filename : files) {
            Matches matches = importSchedule(filename).matches;
            assertThat(matches.countMatches(), greaterThan(0));
        }
    }

    @Test
    public void shouldLoadMatchResult() {
        Match firstMatch = importSchedule(results).matches.getMatch(0);
        TeamInMatch home = firstMatch.getTeams().iterator().next();

        ExistingScore score = (ExistingScore) home.getScore();
        assertThat(score.getGoalsFor(), is(1));
        assertThat(score.getGoalsAgainst(), is(2));
        assertThat(score.getScorersFromAllTeams(), iterableWithSize(2));
    }

    @Test
    public void shouldLoadScorersWithSameNameFromArray() {
        Iterator<TeamInMatch> teamsInSecondMatch = importSchedule(results).matches.getMatch(1).getTeams().iterator();

        assertThat(countGoalsOfFirstPlayerFromTeam(teamsInSecondMatch, "Tým 2"), is(0));
        assertThat(countGoalsOfFirstPlayerFromTeam(teamsInSecondMatch, "Tým 1"), is(3));
    }

    private Integer countGoalsOfFirstPlayerFromTeam(Iterator<TeamInMatch> teamsInSecondMatch, String team) {
        return ((ExistingScore) teamsInSecondMatch.next().getScore()).getTeamScores(team).iterator().next().goalsCount;
    }

    @Test
    public void loadMatchAttributes() {
        Match firstMatch = importSchedule(files[1]).matches.getMatch(0);
        MatchAttributes attributes = firstMatch.getAttributes();
        assertThat(attributes.referee, is("Tým 3"));
        assertThat(attributes.date.format(DateTimeFormatter.ISO_DATE_TIME), is("2014-07-05T12:00:00"));
        assertThat(attributes.period, is(1));
        assertThat(attributes.round, is(1));
        assertThat(attributes.field, is(1));
        assertThat(attributes.note, is("irrelevant text..."));
    }

    @Test
    public void loadStats() {
        Schedule schedule = importSchedule(results);
        ExistingScore score = (ExistingScore) schedule.matches.getMatch(1).getScore();
        assertThat(schedule.settings.matchPeriods.toString(), is("[1st set]"));
        assertThat(schedule.settings.teamStatsNames.toString(), is("[Corner, Unused]"));
        assertThat(schedule.settings.playerStatsNames.toString(), is("[Foul]"));
        assertThat(score.getTeamStats().toString(), is("{1st set=1=2, Corner=2=0}"));
        for (Scorer scorer : score.getTeamScores("Tým 1")) {
            Iterator<Map.Entry<String, Integer>> iterate = scorer.advancedStats.iterate().iterator();
            assertTrue(iterate.hasNext());
            assertThat(iterate.next().getKey(), is("Foul"));
            assertFalse(iterate.hasNext());
        }
    }

    @Test
    public void loadPlayoffSeed() {
        Schedule s = importSchedule(files[6]);
        for (Match m : s.matches.getMatches()) {
            for (TeamInMatch t : m.getTeams()) {
                assertThat(t.getTeam().playoffSeed, greaterThan(0));
            }
        }
    }

    @Test
    public void defaultUnitsAreTeamAndGoal() {
        ScheduleSettings settings = importSchedule(files[0]).settings;
        assertThat(settings.teamUnit, is(ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS));
        assertThat(settings.matchUnit, is(ScheduleSettings.Units.Match.GOAL));
        assertThat(settings.matchPeriods.size(), is(0));
        assertThat(settings.hasDefaultUnits, is(true));
    }

    @Test
    public void loadUnitsAndPeriods() {
        ScheduleSettings settings = importSchedule(files[3]).settings;
        assertThat(settings.teamUnit, is(ScheduleSettings.Units.Team.PLAYER));
        assertThat(settings.matchUnit, is(ScheduleSettings.Units.Match.SET));
        assertThat(settings.matchPeriods.size(), is(3));
        assertThat(settings.hasDefaultUnits, is(false));
    }

    @Test
    public void shouldInformAboutInvalidfile() {
        importSchedule("non existent json file");
        assertFalse(presenter.exists);
    }

    private Schedule importSchedule(String filename) {
        Context.repository = new InMemorySchedules();
        Context.jsonReader = path -> new JsonFileReader().read("tests/_json/" + path);
        UseCaseInvoker.execute(UseCaseInvoker.SCHEDULE_IMPORT, filename, presenter);
        return Context.repository.find(presenter.importedName);
    }

    private static class Presenter implements ImportPresenter {

        public boolean exists = true;
        public String importedName;

        public void nonExistentSchedule(JsonError error) {
            exists = false;
        }

        public void scheduleImported(String scheduleName) {
            importedName = scheduleName;
        }
    }
}
