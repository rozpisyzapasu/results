/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out;

import org.junit.Before;
import org.junit.Test;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.out.summary.SummaryResponse;

import static java.util.Collections.emptyMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

public class SummaryScheduleTest {

    private SummaryResponse response;

    @Before
    public void setUp() {
        ScheduleBuilder
                .teams("A", "B", "C", "D")
                .fields(2)
                .type("groups")
                .groups(new HashMap<String, String[]>() {{
                    put("G1", new String[]{"A", "B"});
                    put("G2", new String[]{"C", "D"});
                }})
                .players("A", new String[]{"P1", "P2"})
                .players("B", new String[]{"P3"})
                .playedMatch("C", "D", 1, 0)
                .match(
                        MatchBuilder.match("A", "B").score(4, 2)
                                .scorer("P1", 4)
                                .scorer("P2", 0)
                                .scorer("P3", 2)
                );
        Context.reload();
        response = UseCaseInvoker.get(UseCaseInvoker.SCHEDULE_SUMMARY);
    }

    @Test
    public void scheduleInfo() {
        assertThat(response.scheduleType, is("groups"));
        assertThat(response.teams.get("G1"), is("A, B"));
        assertThat(response.teams.get("G2"), is("C, D"));
    }

    @Test
    public void generalStats() {
        assertThat(response.groupsCount, is(2));
        assertThat(response.teamsCount, is(4));
        assertThat(response.fieldsCount, is(2));
        assertThat(response.playersCount, is(3));
        assertThat(response.matchesCount, is(2));
        assertThat(response.goalsCount, is(7));
        assertThat(response.averageGoalsInMatch, is(3.5));
    }

    @Test
    public void results() {
        assertThat(response.matches, not(emptyIterable()));
        assertThat(response.players, not(emptyMap()));
        assertThat(response.tables.groups.values(), hasSize(2));
    }

    @Test
    public void winners() {
        assertThat(response.topTeams, iterableWithSize(3));
        assertThat(response.topScorers.entrySet(), iterableWithSize(3));
        assertThat(response.topGoalkeepers, iterableWithSize(1));
    }
}
