/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.out.upgrade;

import org.easygson.JsonEntity;
import org.junit.Test;
import sportsscheduler.results.Context;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.system.JsonResult;
import sportsscheduler.results.out.system.JsonWebReader;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class UpgradeTest {

    private final Reader reader = new Reader();
    private final Presenter presenter = new Presenter();
    private enum Results {ERROR, NOTHING_NEW, UPGRADE_IS_AVAILABLE};

    @Test
    public void whenUpgradeFailed() {
        reader.json = JsonEntity.emptyObject();
        executeUsecase();
        assertThat(presenter.result, is(Results.ERROR));
    }

    @Test
    public void whenNoNewVersionExists() {
        reader.json = JsonEntity.emptyArray();
        executeUsecase();
        assertThat(presenter.result, is(Results.NOTHING_NEW));
    }

    @Test
    public void shouldParseChangelog() {
        JsonEntity version = JsonEntity.emptyObject();
        version.create("version", "new version");
        version.create("releaseDate", "2015-02-14");
        version.create("changelog", JsonEntity.emptyArray().create("some new feature"));
        reader.json = JsonEntity.emptyArray().create(version).parent();
        executeUsecase();
        assertThat(presenter.result, is(Results.UPGRADE_IS_AVAILABLE));
    }

    private void executeUsecase() {
        String irrelevantVersion = "1.0";
        Context.webReader = reader;
        UseCaseInvoker.execute(UseCaseInvoker.APP_UPGRADE, irrelevantVersion, presenter);
    }

    private class Reader extends JsonWebReader {

        private JsonEntity json;

        public JsonResult read(String path) {
            return JsonResult.valid(json);
        }
    }

    private static class Presenter implements UpgradePresenter {

        public Results result;

        public void checkFailed() {
            result = Results.ERROR;
        }

        public void noNewVersionExists() {
            result = Results.NOTHING_NEW;
        }

        public void upgradeIsAvailable(String downloadUrl, List<AvailableVersion> newVersions) {
            result = Results.UPGRADE_IS_AVAILABLE;
        }
    }
}
