/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import org.junit.Before;
import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class ChangeTablePointsTest {

    private Boolean arePointsChanged;
    private TablePointsRequest request;

    @Before
    public void whenTeamNotExists() {
        request = new TablePointsRequest();
        ScheduleBuilder.empty();
    }

    @Test
    public void noChangeWhenPointsAreInvalid() {
        request.win = 2;
        request.draw = 3;
        executeUseCase();
        assertThat(arePointsChanged, is(false));
    }

    @Test
    public void whenPointsAreSuccessfullyDeleted() {
        request.win = 3;
        request.draw = 1;
        request.loss = 0;
        executeUseCase();
        assertThat(arePointsChanged, is(true));
    }

    private void executeUseCase() {
        UseCaseInvoker.execute(UseCaseInvoker.TABLE_POINTS, request, new TablePointsPresenter() {
            public void pointsAreNotInDescendingOrder() {
                arePointsChanged = false;
            }

            public void pointsChanged() {
                arePointsChanged = true;
            }
        });
    }
}
