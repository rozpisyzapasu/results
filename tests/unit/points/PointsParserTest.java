/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.points;

import org.junit.Test;
import sportsscheduler.results.match.ResultType;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PointsParserTest {

    private PointsParser parser = new PointsParser();

    @Test
    public void shouldIgnoreInvalidMatchScorePoints() {
        assertThat(parser.parse("3 2 3"), hasSize(0));
        assertThat(parser.parse("3:2 2-3"), hasSize(0));
    }

    @Test
    public void shouldParseMatchScorePointsFromString() {
        assertThat(parser.parse("3:2 2-1"), hasSize(1));
        assertThat(parser.parse("3:0 3-1\n3:2 1"), hasSize(2));
    }

    @Test
    public void shouldFillMissingMatchScorePoints() {
        assertThat(parseFirstMatchScorePoints("3:0 20-1"), is("3:0 20-1-1"));
        assertThat(parseFirstMatchScorePoints("1:1 2"), is("1:1 2-2-2"));
    }

    private String parseFirstMatchScorePoints(String result) {
        TablePoints.MatchScorePoints points = parser.parse(result).get(0);
        return String.format(
                "%d:%d %s",
                points.goals[0], points.goals[1], pointsToString(points.points)
        );
    }

    private String pointsToString(TablePoints points) {
        return String.format(
                "%d-%d-%d",
                points.win, points.draw, points.loss
        );
    }

    @Test
    public void shouldConvertMatchScorePointsToString() {
        TablePoints.MatchScorePoints win = new TablePoints.MatchScorePoints(2, 0);
        win.points.win = 10;
        win.points.loss = 1;

        TablePoints.MatchScorePoints draw = new TablePoints.MatchScorePoints(1, 1);
        draw.points.win = 5;
        draw.points.draw = 5;
        draw.points.loss = 5;

        TablePoints points = new TablePoints();
        points.matchScores.add(win);
        points.matchScores.add(draw);

        assertThat(parser.convert(points.matchScores), is("2:0 10-1\n1:1 5"));
    }

    @Test
    public void shouldConvertResultScorePointsToString() {
        TablePoints points = new TablePoints();
        points.addResultScore(ResultType.EXTRA_TIME, TablePoints.defaultPoints());
        assertThat(parser.convert(ResultType.EXTRA_TIME, points), is("3-0"));
        assertThat(parser.convert(ResultType.SHOOTOUT, points), is(""));
    }

    @Test
    public void shouldIgnoreInvalidResultScorePoints() {
        Map<ResultType, String> types = new HashMap<>();
        types.put(ResultType.EXTRA_TIME, "1-5");
        types.put(ResultType.SHOOTOUT, "1");
        types.put(ResultType.WIN_BY_DEFAULT, "");
        assertThat(parser.parse(types), aMapWithSize(0));
    }

    @Test
    public void shouldParseResultScorePointsFromString() {
        Map<ResultType, String> types = new HashMap<>();
        types.put(ResultType.EXTRA_TIME, "2-0");
        types.put(ResultType.SHOOTOUT, "1-1");
        Map<ResultType, TablePoints> scores = parser.parse(types);
        assertThat(scores, aMapWithSize(1));
        assertThat(pointsToString(scores.get(ResultType.EXTRA_TIME)), is("2-0-0"));
    }
}
