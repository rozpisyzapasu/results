/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.repository;

import org.junit.Test;
import sportsscheduler.results.Schedule;
import sportsscheduler.results.ScheduleRepository;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class InMemorySchedulesTest {

    private ScheduleRepository repository = new InMemorySchedules();

    @Test
    public void shouldSaveAndFindScheduleByName() {
        Schedule schedule = givenSchedule("irrelevant name");
        assertThat(repository.find("irrelevant name"), equalTo(schedule));
    }

    @Test
    public void createdSchedulesAreSavedByDefault() {
        givenSchedule("A");
        Schedule b = givenSchedule("B");
        assertThat(repository.areAllSchedulesSaved(), equalTo(true));
        b.isSaved = false;
        assertThat(repository.areAllSchedulesSaved(), equalTo(false));
    }

    private Schedule givenSchedule(String name) {
        Schedule schedule = new Schedule();
        schedule.name = name;
        repository.save(schedule);
        return schedule;
    }
}
