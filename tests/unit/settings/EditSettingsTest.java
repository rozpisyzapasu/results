/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.settings;

import org.junit.Test;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.ScheduleSettings.Units.Match.SET;

public class EditSettingsTest {

    private String matchPeriod = "irrelevant match period";

    @Test
    public void uniqueNames() {
        givenStats("ace", "ACE");
        oneStatShouldExist("ACE");
    }

    @Test
    public void removeExtraSpaces() {
        givenStats(" name ");
        oneStatShouldExist("name");
    }

    @Test
    public void ignoreEmptyNames() {
        givenStats(" ");
        noStatsShouldExist();
    }

    @Test
    public void ignoreMatchPeriods() {
        givenStats(String.format(" %s", matchPeriod));//, matchPeriod.toUpperCase());
        noStatsShouldExist();
    }

    private void givenStats(String... names) {
        ScheduleBuilder.empty();
        EditSettingsRequest r = new EditSettingsRequest();
        r.matchPeriods = Arrays.asList(new String[] { matchPeriod });
        r.hasAdvancedStats = true;
        r.teamStatsNames = Arrays.asList(names);
        EditSettingsPresenter presenter = () -> {};
        UseCaseInvoker.execute(UseCaseInvoker.EDIT_SETTINGS, r, presenter);
    }

    private void oneStatShouldExist(String expectedName) {
        assertThat(Context.currentSchedule.settings.teamStatsNames.size(), is(1));
        assertThat(Context.currentSchedule.settings.teamStatsNames.get(0), is(expectedName));
    }

    private void noStatsShouldExist() {
        assertThat(Context.currentSchedule.settings.teamStatsNames.size(), is(0));
    }
}
