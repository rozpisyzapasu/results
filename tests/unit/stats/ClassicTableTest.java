/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.Tables;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class ClassicTableTest {

    private Iterator<PresentableTeamResult> classicTable;

    @Test
    public void whenScheduleIsEmptyThenTableIsEmpty() {
        ScheduleBuilder.empty();
        executeUseCase();
        assertFalse(classicTable.hasNext());
    }

    @Test
    public void matchesWithoutScoreThenTableHasEmptyRows() {
        ScheduleBuilder.teams("Team A", "Team B")
                .match("Team A", "Team B").match("Team A", "Team B");

        executeUseCase();
        assertTeamRowsAreEmpty();
    }

    private void assertTeamRowsAreEmpty() {
        while(classicTable.hasNext()) {
            PresentableTeamResult team = classicTable.next();
            assertThat(team.totalWins(), is(0));
            assertThat(team.drawsCount, is(0));
            assertThat(team.totalLosses(), is(0));
            assertThat(team.goalsFor, is(0));
            assertThat(team.goalsAgainst, is(0));
        }
    }

    @Test
    public void matchesWithScoreThenTableContainsSortedResults() {
        ScheduleBuilder.teams("Team B", "Team A")
                .playedMatch("Team A", "Team B", 3, 1)
                .playedMatch("Team A", "Team B", 0, 0);

        executeUseCase();

        PresentableTeamResult teamA = classicTable.next();
        assertThat(teamA.rank, is(1));
        assertThat(teamA.totalWins(), is(1));
        assertThat(teamA.drawsCount, is(1));
        assertThat(teamA.totalLosses(), is(0));
        assertThat(teamA.goalsFor, is(3));
        assertThat(teamA.goalsAgainst, is(1));
        assertThat(teamA.points, is(4));

        PresentableTeamResult teamB = classicTable.next();
        assertThat(teamB.rank, is(2));
        assertThat(teamB.totalWins(), is(0));
        assertThat(teamB.drawsCount, is(1));
        assertThat(teamB.totalLosses(), is(1));
        assertThat(teamB.goalsFor, is(1));
        assertThat(teamB.goalsAgainst, is(3));
        assertThat(teamB.points, is(1));
    }

    @Test
    public void useMinitableForMultipleTeamsWithSamePoints() {
        ScheduleBuilder.teams("R", "F", "B", "E", "A")
                // head to head matches
                .playedMatch("R", "F", 2, 1)
                .playedMatch("R", "B", 2, 2)
                .playedMatch("R", "E", 0, 0)
                .playedMatch("F", "B", 1, 0)
                .playedMatch("F", "E", 0, 0)
                .playedMatch("B", "E", 3, 3)
                // other matches, so every team has 5 points
                .playedMatch("F", "A", 0, 0)
                .playedMatch("E", "A", 0, 0)
                .playedMatch("E", "A", 0, 0)
                .playedMatch("B", "A", 1, 0);
        executeUseCase();
        assertThat(getTeamNames(), contains("R", "F", "E", "B", "A"));
    }

    @Test
    public void useTotalScoreIfMinitableHasJustDrawsWithSameScore() {
        ScheduleBuilder.teams("C", "B", "A")
                .playedMatch("A", "B", 1, 1)
                .playedMatch("C", "A", 0, 10)
                .playedMatch("C", "B", 1, 2);
        executeUseCase();
        assertThat(getTeamNames(), contains("A", "B", "C"));
    }

    public List<String> getTeamNames() {
        Iterable<PresentableTeamResult> iterable = () -> classicTable;
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(presentableTeamResult -> presentableTeamResult.team)
                .collect(Collectors.toList());
    }

    private void executeUseCase() {
        Tables tables = UseCaseInvoker.get(UseCaseInvoker.STATS_TABLE);
        classicTable = tables.getFirstTable().classicTable.teams.iterator();
    }
}
