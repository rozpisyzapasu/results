/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.stats.schedule.ScheduleStats;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CreateScheduleStatsTest {

    @Test
    public void shouldSortPlayersByGoalsCount() {
        ScheduleBuilder
                .teams("A", "B", "C", "D")
                .fields(2)
                .groups(new HashMap<String, String[]>() {{
                    put("G1", new String[]{"A", "B"});
                    put("G2", new String[]{"C", "D"});
                }})
                .players("A", new String[]{"P1", "P2"})
                .playedMatch("C", "D", 1, 0)
                .playedMatch("A", "B", 4, 2);

        ScheduleStats response = UseCaseInvoker.get(UseCaseInvoker.STATS_SCHEDULE);
        assertThat(response.groupsCount, is(2));
        assertThat(response.teamsCount, is(4));
        assertThat(response.fieldsCount, is(2));
        assertThat(response.playersCount, is(2));
        assertThat(response.matchesCount, is(2));
        assertThat(response.playedMatchesCount, is(2));
        assertThat(response.countUpcomingMatches(), is(0));
        assertThat(response.goalsCount, is(7));
        assertThat(response.averageGoalsInMatch, is(3.5));
    }
}
