/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.Tables;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CrossTableTest {

    private Map.Entry<PresentableTeamResult, Map<String, List<String>>> firstTeam, secondTeam;

    @Test
    public void matchesWithScoreThenTableContainsSortedResults() {
        ScheduleBuilder.teams("Team B", "Team A")
                .playedMatch("Team A", "Team B", 3, 1)
                .match("Team A", "Team B");

        executeUseCase();

        assertThat(firstTeam.getKey().rank, is(1));
        assertThat(firstTeam.getValue().get("Team B"), contains("3:1", ":"));
        assertThat(secondTeam.getValue().get("Team A"), contains("1:3", ":"));
    }

    private void executeUseCase() {
        Tables tables = UseCaseInvoker.get(UseCaseInvoker.STATS_TABLE);
        Iterator<Map.Entry<PresentableTeamResult, Map<String, List<String>>>> crossTable =
                tables.getFirstTable().crossTable.entrySet().iterator();
        firstTeam = crossTable.next();
        secondTeam = crossTable.next();
    }
}
