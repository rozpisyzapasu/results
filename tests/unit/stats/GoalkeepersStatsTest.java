/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.stats.goalkeepers.PresentableGoalkeeper;
import sportsscheduler.results.stats.goalkeepers.Goalkeepers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class GoalkeepersStatsTest {

    private Goalkeepers goalkeepers;

    @Test
    public void whenScheduleIsEmpty() {
        ScheduleBuilder.empty();
        executeUseCase();
        assertFalse(goalkeepers.all.iterator().hasNext());
        assertFalse(goalkeepers.getTop().iterator().hasNext());
        assertEquals(0, goalkeepers.getMinGoalsCount());
    }

    @Test
    public void shortSortTeamsByGoalsAgainst() {
        ScheduleBuilder
                .teams("A", "B")
                .playedMatch("A", "B", 1, 2);
        executeUseCase();
        assertThat(allToString(), contains("B=1", "A=2"));
    }

    @Test
    public void shouldFindGoalkeepersWithLowestGoalsAgainst() {
        int irrelevantMinGoals = 1;
        ScheduleBuilder
                .teams("A", "B", "C")
                .playedMatch("A", "C", 2, irrelevantMinGoals)
                .playedMatch("B", "C", 2, irrelevantMinGoals);

        executeUseCase();
        assertThat(goalkeepers.getTop(), contains("A", "B"));
        assertEquals(irrelevantMinGoals, goalkeepers.getMinGoalsCount());
    }

    private void executeUseCase() {
        goalkeepers = UseCaseInvoker.get(UseCaseInvoker.STATS_GOALKEEPERS);
    }

    private Iterable<String> allToString() {
        List<String> result = new ArrayList<>();
        for (PresentableGoalkeeper entry : goalkeepers.all) {
            result.add(entry.team + "=" + entry.goalsAgainst);
        }
        return result;
    }
}
