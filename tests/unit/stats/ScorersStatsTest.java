/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.importer.build.MatchBuilder;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class ScorersStatsTest {

    @Test
    public void shouldSortPlayersByGoalsCount() {
        ScheduleBuilder
                .teams("A", "B")
                .players("A", new String[] {"Peter", "John"})
                .match(
                        MatchBuilder
                                .match("A", "B")
                                .score(3, 0)
                                .scorer("Peter", 1)
                                .scorer("John", 2)
                );

        assertThat(getPlayers(), contains("John=2", "Peter=1"));
    }

    private Iterable<String> getPlayers() {
        Iterable<PresentablePlayer> players = UseCaseInvoker.get(UseCaseInvoker.STATS_SCORERS);
        List<String> result = new ArrayList<>();
        for (PresentablePlayer p : players) {
            result.add(p.name + "=" + p.goalsCount);
        }
        return result;
    }
}
