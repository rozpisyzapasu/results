/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import org.junit.Test;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.match.Matches;
import sportsscheduler.results.match.Team;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class HeadToHeadComparatorTest {

    @Test
    public void teamWhoWonMatchIsBetter() {
        PresentableTeamResult a = createTeam("A");
        PresentableTeamResult b = createTeam("B");
        ScheduleBuilder.teams("A", "B", "C")
                .playedMatch("A", "B", 5, 0)
                .playedMatch("C", "B", 0, 10)
                .playedMatch("C", "B", 0, 1);
        assertAisBetterThanB(a, b);
    }

    @Test
    public void teamWhenBothWonSameNumberOfMatchesThenGoalsForIsCriterion() {
        PresentableTeamResult a = createTeam("A");
        PresentableTeamResult b = createTeam("B");
        ScheduleBuilder.teams("A", "B")
                .playedMatch("A", "B", 2, 0)
                .playedMatch("A", "B", 0, 1);
        assertAisBetterThanB(a, b);
    }

    @Test
    public void teamWhenAllMatchesAreDrawThenFullTableIsCriterion() {
        PresentableTeamResult a = createTeam("A");
        PresentableTeamResult b = createTeam("B");
        ScheduleBuilder.teams("A", "B")
                .playedMatch("A", "B", 0, 0)
                .playedMatch("A", "B", 1, 1);
        assertHeadToHeadIsEqual(a, b);
    }

    @Test
    public void teamWhenNoMatchThenFullTableIsCriterion() {
        PresentableTeamResult a = createTeam("A");
        PresentableTeamResult b = createTeam("B");
        ScheduleBuilder.teams("A", "B");
        assertHeadToHeadIsEqual(a, b);
    }

    private PresentableTeamResult createTeam(String name) {
        Team t = Team.test(name, "group");
        return new PresentableTeamResult(t);
    }

    private void assertAisBetterThanB(PresentableTeamResult a, PresentableTeamResult b) {
        assertThat(compare(a, b), is(-1));
        assertThat(compare(b, a), is(1));
    }

    private void assertHeadToHeadIsEqual(PresentableTeamResult a, PresentableTeamResult b) {
        assertThat(compare(a, b), is(0));
        assertThat(compare(b, a), is(0));
    }

    private int compare(PresentableTeamResult a, PresentableTeamResult b) {
        Matches m = Context.currentSchedule.matches;
        HeadToHeadMatchesComparator comparator = new HeadToHeadMatchesComparator();
        comparator.setMatches(m);
        Tables.ClassicTable table = new Tables.ClassicTable();
        table.teams.add(a);
        table.teams.add(b);
        comparator.sort(table);
        return comparator.compare(a, b);
    }
}
