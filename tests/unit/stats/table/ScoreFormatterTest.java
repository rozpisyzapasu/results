/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import org.junit.Test;
import sportsscheduler.results.match.ResultType;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ScoreFormatterTest {

    private ScoreFormatter formatter = new ScoreFormatter();

    @Test
    public void shouldConvertGoalsToStringAndNullToEmptyString() {
        assertThat(formatter.scoreToString(1, null, ResultType.NORMAL), is("1:"));
    }

    @Test
    public void shouldAppendResultAfterNonNormalScore() {
        assertThat(formatter.scoreToString(1, 2, ResultType.EXTRA_TIME), is("1:2 pr."));
    }
}
