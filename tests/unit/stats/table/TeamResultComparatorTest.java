/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.stats.table;

import org.junit.Test;
import sportsscheduler.results.match.Team;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.TeamResultComparator;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TeamResultComparatorTest {

    @Test
    public void noDifference() {
        PresentableTeamResult a = createTeam();
        assertThat(compare(a, a), is(0));
    }

    @Test
    public void topCriterionIsPointsCount() {
        PresentableTeamResult a = createTeam();
        a.points = 8;

        PresentableTeamResult b = createTeam();
        b.points = 5;
        assertAisBetterThanB(a, b);
    }

    @Test
    public void teamWithLessMatchesIsBetter() {
        PresentableTeamResult a = createTeam();
        a.matchesCount = 1;

        PresentableTeamResult b = createTeam();
        b.matchesCount = 2;
        assertAisBetterThanB(a, b);
    }

    @Test
    public void teamWithMoreGoalsForIsBetter() {
        PresentableTeamResult a = createTeam();
        a.goalsFor = 10;

        PresentableTeamResult b = createTeam();
        b.goalsFor = 5;
        assertAisBetterThanB(a, b);
    }

    @Test
    public void teamWithLessGoalsAgainstIsBetter() {
        PresentableTeamResult a = createTeam();
        a.goalsAgainst = 5;

        PresentableTeamResult b = createTeam();
        b.goalsAgainst = 10;
        assertAisBetterThanB(a, b);
    }

    private void assertAisBetterThanB(PresentableTeamResult a, PresentableTeamResult b) {
        assertThat(compare(a, b), is(-1));
        assertThat(compare(b, a), is(1));
    }

    private PresentableTeamResult createTeam() {
        Team t = Team.test("name", "group");
        return new PresentableTeamResult(t);
    }

    private int compare(PresentableTeamResult a, PresentableTeamResult b) {
        return new TeamResultComparator().compare(a, b);
    }
}
