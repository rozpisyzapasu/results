/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import org.junit.Test;
import sportsscheduler.results.*;
import sportsscheduler.results.match.Matches;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;

public class DeleteTeamTest {

    private final String deletedTeam = "irrelevant team";
    private final TeamSpy presenter = new TeamSpy();

    @Test
    public void whenTeamNotExists() {
        ScheduleBuilder.empty();
        executeUsecase();
        assertFalse(presenter.wasTeamDeleted);
    }

    @Test
    public void shouldDeleteTeamAndMatches() {
        ScheduleBuilder.teams(deletedTeam, "otherTeam")
                        .match(deletedTeam, "otherTeam")
                        .players(deletedTeam, new String[] {"irrelevat player"});
        Matches matches = executeUsecase();
        assertTrue(presenter.wasTeamDeleted);
        assertThat(matches.countTeams(), is(1));
        assertThat(matches.countMatches(), is(0));
    }

    private Matches executeUsecase() {
        UseCaseInvoker.execute(UseCaseInvoker.TEAM_DELETE, deletedTeam, presenter);
        return Context.currentSchedule.matches;
    }
}
