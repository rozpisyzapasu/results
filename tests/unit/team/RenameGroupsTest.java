/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import org.junit.Before;
import org.junit.Test;
import sportsscheduler.results.GroupsSpy;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;

import java.util.HashMap;

import static sportsscheduler.results.GroupsSpy.ActionResult;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class RenameGroupsTest {

    private ActionResult result;

    @Before
    public void setUp() {
        ScheduleBuilder.teams("T1", "T2").groups(new HashMap<String, String[]>() {{
            put("A", new String[]{"T1"});
            put("B", new String[]{"T2"});
        }});
    }

    @Test
    public void whenNothingIsChanged() {
        renameTo("A", "B");
        assertResult(ActionResult.NO_CHANGE);
    }

    @Test
    public void whenGroupNamesAreDuplicated() {
        renameTo("Group A", "Group A");
        assertResult(ActionResult.NON_UNIQUE_GROUPS);
    }

    @Test
    public void whenGroupsAreRenamed() {
        renameTo("Group A", "B");
        assertResult(ActionResult.SUCCESS);
    }

    private void renameTo(String... newGroups) {
        GroupsSpy spy = new GroupsSpy();
        UseCaseInvoker.execute(UseCaseInvoker.GROUPS_RENAME, newGroups, spy);
        result = spy.result;
    }

    private void assertResult(ActionResult expectedResult) {
        assertThat(result, is(expectedResult));
    }
}
