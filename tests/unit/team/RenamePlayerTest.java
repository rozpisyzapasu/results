/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import org.junit.Test;
import sportsscheduler.results.PlayerSpy;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.team.player.RenamePlayerRequest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static sportsscheduler.results.PlayerSpy.ActionResult;

public class RenamePlayerTest {

    private ActionResult result;

    @Test
    public void whenTeamNotExists() {
        givenEmptySchedule();
        renameTo("new name");
        assertResult(ActionResult.NON_EXISTENT_TEAM);
    }

    @Test
    public void whenPlayerNotExists() {
        givenScheduleWithNoPlayers();
        renameTo("new name");
        assertResult(ActionResult.NON_EXISTENT_PLAYER);
    }

    @Test
    public void whenNewNameIsExistingPlayer() {
        givenScheduleWithPlayer();
        renameTo("Player");
        assertResult(ActionResult.EXISTING_NAME);
    }

    @Test
    public void whenPlayerIsRenamed() {
        givenScheduleWithPlayer();
        renameTo("new name");
        assertResult(ActionResult.SUCCESS);
    }

    @Test
    public void whenRenameIsRepeated() {
        ScheduleBuilder.teams("Team A").players("Team A", new String[]{"P1", "P2"});
        renamePlayer("P1", "NEW");
        renamePlayer("P2", "NEW");
        assertResult(ActionResult.EXISTING_NAME);
    }

    private void givenEmptySchedule() {
        ScheduleBuilder.empty();
    }

    private void givenScheduleWithNoPlayers() {
        ScheduleBuilder.teams("Team A");
    }

    private void givenScheduleWithPlayer() {
        ScheduleBuilder
                .teams("Team A")
                .players("Team A", new String[]{"Player"});
    }

    private void renameTo(String newName) {
        renamePlayer("Player", newName);
    }

    private void renamePlayer(String oldName, String newName) {
        RenamePlayerRequest r = new RenamePlayerRequest();
        r.team = "Team A";
        r.player = oldName;
        r.newName = newName;

        PlayerSpy spy = new PlayerSpy();
        UseCaseInvoker.execute(UseCaseInvoker.PLAYER_RENAME, r, spy);
        result = spy.result;
    }

    private void assertResult(ActionResult expectedResult) {
        assertThat(result, is(expectedResult));
    }
}