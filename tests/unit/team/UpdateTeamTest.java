/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.team;

import org.junit.Test;
import sportsscheduler.results.ScheduleBuilder;
import sportsscheduler.results.TeamSpy;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.team.UpdateTeamRequest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class UpdateTeamTest {

    private TeamSpy spy;
    private final String team = "Team A";

    @Test
    public void uniquePlayersAreAddedAndMissingPlayersAreDeleted() {
        givenTeam("Player 1", "Player 2", "Player 3");
        updatePlayers("Player 1", "New player", "New player");
        assertThat(spy.addedPlayersCount, is(1));
        assertThat(spy.deletedPlayersCount, is(2));
        assertThat(spy.werePlayersSorted, is(false));
    }

    @Test
    public void sortPlayersWithoutChangeOfExistingPlayers() {
        givenTeam("Player 1", "Player 2", "Player 3");
        updatePlayers("Player 3", "Player 1", "Player 2");
        assertThat(spy.werePlayersSorted, is(true));
        assertThat(spy.addedPlayersCount, is(0));
        assertThat(spy.deletedPlayersCount, is(0));
    }

    @Test
    public void whenRenamedTeamNotExists() {
        givenNoTeams();
        assertRenameFailure("Non existent team");
    }

    @Test
    public void whenNewNameIsAlreadyInSchedule() {
        givenTeam();
        assertRenameFailure(team);
        assertTrue(spy.nothingChanged);
    }

    private void assertRenameFailure(String newName) {
        renameTeam(team, newName);
        assertFalse(spy.wasNameChanged);
    }

    @Test
    public void whenTeamIsRenamed() {
        givenTeam();
        renameTeam(team, "NEW");
        assertTrue(spy.wasNameChanged);
    }

    @Test
    public void whenRenameIsRepeated() {
        ScheduleBuilder.teams("Team A", "Team B");
        renameTeam("Team A", "NEW");
        renameTeam("Team B", "NEW");
        assertFalse(spy.wasNameChanged);
    }

    private void givenNoTeams() {
        ScheduleBuilder.empty();
    }

    private void givenTeam(String... players) {
        ScheduleBuilder.teams(team).players(team, players);
    }

    private void renameTeam(String oldName, String newName) {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = oldName;
        r.newName = newName;
        r.players = new String[0];

        executeUsecase(r);
    }

    private void updatePlayers(String... newPlayers) {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = team;
        r.newName = team;
        r.players = newPlayers;

        executeUsecase(r);
    }

    private void executeUsecase(UpdateTeamRequest r) {
        spy = new TeamSpy();
        UseCaseInvoker.execute(UseCaseInvoker.TEAM_UPDATE, r, spy);
    }
}
