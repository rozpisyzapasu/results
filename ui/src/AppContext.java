/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx;

import sportsscheduler.results.fx.matches.MatchFilter;

public class AppContext {
    public static MatchFilter matchFilter = new MatchFilter();

    public static void reset() {
        matchFilter = new MatchFilter();
    }
}
