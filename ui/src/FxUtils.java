/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx;

import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.util.Callback;
import sportsscheduler.results.fx.components.Converters;

public class FxUtils {

    public static void deselectableToggleGroup(ToggleGroup group) {
        group.getToggles().get(0).setSelected(true);
        group.selectedToggleProperty().addListener((value, old, newt) -> {
            if (newt == null) {
                group.selectToggle(old);
            }
        });
    }

    public static void initTableView(TableView t, ObservableList rows) {
        initTableView(t, rows, "");
    }

    public static void initTableView(TableView t, String placeholder) {
        if (t != null) {
            initTableView(t, t.getItems(), placeholder);
        }
    }

    public static void initTableView(TableView t, ObservableList rows, String placeholder) {
        t.setItems(rows);
        t.setPlaceholder(new Text(placeholder));
        t.getSelectionModel().clearSelection();
    }

    public static void resetMaxColumnWith(TableView t) {
        ObservableList<TableColumn> columns = t.getColumns();
        for (TableColumn c : columns) {
            if (!c.getText().equals("#")) {
                double oldMaxWidth = c.getMaxWidth();
                double newMaxWidth = oldMaxWidth <= 300 ? oldMaxWidth * 3 : oldMaxWidth;
                c.setMaxWidth(newMaxWidth);
            }
        }
    }

    public static void tableColumnCell(TableView table, int index, Callback<TableColumn, TableCell> factory) {
        TableColumn column = (TableColumn) table.getColumns().get(table.getColumns().size() - index);
        column.setCellFactory(factory);
    }

    public static void tableColumnVisibility(TableView table, int index, boolean isVisible) {
        TableColumn column = (TableColumn) table.getColumns().get(index);
        column.setVisible(isVisible);
    }

    public static void incrementTextField(TextField label, int change) {
        int currentValue = Converters.parseInt(label.getText());
        int newValue = currentValue + change;
        label.setText(newValue + "");
    }
}
