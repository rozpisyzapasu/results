/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx;

public abstract class Openable {

    public void onOpen() {
    }

    abstract public void refresh();
}
