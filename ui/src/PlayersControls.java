/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import sportsscheduler.results.Context;

import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class PlayersControls {

    public static void show(Pane p) {
        for (Node n : p.getChildren()) {
            toggleNodeVisibility(n);
        }
    }

    private static void toggleNodeVisibility(Node n) {
        for (String cssClass : n.getStyleClass()) {
            if (cssClass.startsWith("cssToggle-hasPlayers")) {
                n.setVisible(Context.hasPlayers());
            }
        }
    }

    public static ResourceBundle buildResourceBundle(String teamType, String matchType) {
        ResourceBundle[] bundles = {getResource(teamType), getResource(matchType)};
        Enumeration<String> keys = new CompoundEnumeration<String>(new Enumeration[]{ bundles[0].getKeys(), bundles[1].getKeys()});
        return new ResourceBundle() {
            @Override
            protected Object handleGetObject(String key) {
                MissingResourceException exception = null;
                for (ResourceBundle bundle : bundles) {
                    try {
                        Object result = bundle.getObject(key);
                        if (result != null) {
                            return result;
                        }
                    } catch (MissingResourceException e) {
                        exception = e;
                    }
                }
                throw exception;
            }

            @Override
            public Enumeration<String> getKeys() {
                return keys;
            }
        };
    }

    private static ResourceBundle getResource(String type) {
        String path = "sportsscheduler.results.fx.assets.langs." + type;
        return ResourceBundle.getBundle(path, new Locale("cs"));
    }
}
