/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.matches.score.ScoreController;
import sportsscheduler.results.fx.window.SidebarController;
import sportsscheduler.results.fx.window.StatusType;
import sportsscheduler.results.fx.window.ToolbarController;
import sportsscheduler.results.fx.window.WindowController;
import sportsscheduler.results.fx.window.confirm.ConfirmController;
import sportsscheduler.results.fx.window.nonmodal.NonModalWindowController;
import sportsscheduler.results.out.system.FileWebReader;
import sportsscheduler.results.out.system.JsonFileWriter;
import sportsscheduler.results.out.system.JsonWebReader;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.repository.InMemorySchedules;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static sportsscheduler.results.fx.window.StatusType.ERROR;

public class ResultsFx extends Application {

    private static String currentWindow = "";
    private static String lastPage = "teams/teams.fxml";
    private static String importPage = "importer/import.fxml";
    private static String settingsPage = "settings/settings.fxml";
    private static HostServices services;

    private static Map<String, String> menu = new HashMap<String, String>() {{
        put("matches/matches.fxml", "matches");
        put("matches/score/score.fxml", "matches");
        put("teams/teams.fxml", "teams");
        put("teams/team/team.fxml", "teams");
        put("stats/stats.fxml", "stats");
        put("table/tables.fxml", "tables");
        put("summary/summary.fxml", "summary");
        put("settings/settings.fxml", "settings");
    }};

    private static WindowController windowController;
    public static ToolbarController toolbarController;
    public static SidebarController sidebarController;
    public static List<Openable> openedWindows = new ArrayList<>();
    private static ResourceBundle resources;

    public void start(Stage stage) throws Exception{
        Context.repository = new InMemorySchedules();
        Context.jsonReader = new FileWebReader();
        Context.webReader = new JsonWebReader();
        Context.jsonWriter = new JsonFileWriter();
        Context.defaultGroup = "Týmy";
        services = getHostServices();
        loadResources();

        FXMLLoader fxmlLoader = buildFxmlLoader("window/window.fxml");
        Scene scene = new Scene(fxmlLoader.load());
        windowController = fxmlLoader.getController();
        stage.setScene(scene);
        windowController.setStage(stage);

        fxmlLoader = buildFxmlLoader("window/sidebar.fxml");
        windowController.setSidebar(fxmlLoader.load());
        sidebarController = fxmlLoader.getController();

        fxmlLoader = buildFxmlLoader("window/toolbar.fxml");
        windowController.setToolbar(fxmlLoader.load());
        toolbarController = fxmlLoader.getController();

        stage.show();

        replaceWindow(importPage);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void replaceWindow(String fxmlFile) {
        try {
            currentWindow = fxmlFile;
            Node main = buildFxmlLoader(fxmlFile).load();
            setWindowContent(main);
            reloadLastPage(fxmlFile);
            setActiveMenu(fxmlFile);
        } catch (Exception e) {
            windowException(e);
        }
    }

    private static void setWindowContent(Node main) {
        main.setId("main");
        windowController.setContent(main);
        windowController.clearStatusBar();
    }

    public static FXMLLoader buildFxmlLoader(String file) {
        URL location = ResultsFx.class.getResource(file);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        fxmlLoader.setResources(resources);
        return fxmlLoader;
    }

    private static void reloadLastPage(String fxmlFile) {
        lastPage = fxmlFile.equals(importPage) || fxmlFile.equals(settingsPage) ? lastPage : fxmlFile;
    }

    private static void setActiveMenu(String fxmlFile) {
        if (menu.containsKey(fxmlFile)) {
            String activeButton = menu.get(fxmlFile);
            sidebarController.activateButton(activeButton);
        } else {
            sidebarController.deactivateButton();
        }
    }

    public static void reloadData()
    {
        reloadData(true);
    }

    public static void reloadData(boolean isChanged)
    {
        Context.reload();
        ResultsFx.toolbarController.updateToolbar(Context.schedule, isChanged);
        if (ScoreController.match != null) {
            int id = ScoreController.match.idMatch;
            ScoreController.match = null;
            for (PresentableMatch match : Context.schedule.matches) {
                if (match.idMatch == id) {
                    ScoreController.match = match;
                    break;
                }
            }
        }
        openedWindows.forEach(sportsscheduler.results.fx.Openable::refresh);
    }

    private static void windowException(Exception e) {
        if (Context.currentSchedule == null) {
            setStatus("Nejprve musíte načíst rozpis, až poté budou fungovat tlačítka v menu", StatusType.ERROR);
        } else {
            showException(e.getCause().getCause());
        }
    }

    public static void showException(Throwable cause) {
        try {
            Files.write(Paths.get("error.log"), exceptionToWrite(cause));
            setStatus("Jejda, došlo k chybě. Informace byly uloženy do souboru error.log", StatusType.ERROR);
        } catch (IOException e) {
            setStatus("Nepodařilo se uložit informace o chybě " + cause.getMessage(), StatusType.ERROR);
        }
    }

    private static ArrayList<CharSequence> exceptionToWrite(Throwable cause) {
        StringWriter w = new StringWriter();
        cause.printStackTrace(new PrintWriter(w));

        ArrayList<CharSequence> writeList = new ArrayList<>();
        writeList.add(w.toString());
        return writeList;
    }

    public static void setStatus(String status, StatusType type) {
        windowController.setStatus(status, type);
    }

    // after import this method was called twice (warning is not saved), so check that schedule is really changed
    public static void changeSchedule(String scheduleName) {
        if (Context.currentSchedule == null || !Context.currentSchedule.name.equals(scheduleName)) {
            AppContext.reset();
            Context.loadSchedule(scheduleName);
            ResultsFx.reloadData(false);
            String page = Context.currentSchedule.settings.hasDefaultUnits ? settingsPage : ResultsFx.lastPage;
            replaceTranslatedWindow(page);
        }
    }

    public static void replaceTranslatedWindow(String nullablePage) {
        String page = nullablePage == null ? lastPage : nullablePage;
        loadResources();
        sidebarController.reloadButtonsTexts();
        // openedWindows should be reloaded too, but it's not simple, so ignore this edge case for now (https://bugs.openjdk.java.net/browse/JDK-8090719)
        replaceWindow(page);
    }

    public static String trans(String rawKey) {
        String key = rawKey.substring(1); // use % notation like in fxml
        return resources.getString(key);
    }

    private static void loadResources() {
        resources = PlayersControls.buildResourceBundle(
            Context.getTeamUnit().toString(),
            Context.getMatchUnit().toString()
        );
    }

    public static void redirectToWeb(String url) {
        services.showDocument(url);
    }

    public static void showNonModalWindow() throws IOException {
        FXMLLoader content = buildFxmlLoader(currentWindow);
        Node contentNode = content.load();
        Object controller = content.getController();

        if (controller instanceof Openable) {
            Openable openable = (Openable) controller;
            FXMLLoader nonModalWindow = buildFxmlLoader("window/nonmodal/window.fxml");
            Scene scene = new Scene(nonModalWindow.load());
            NonModalWindowController nonModalController = nonModalWindow.getController();
            nonModalController.setContent(contentNode);
            openable.onOpen();
            openedWindows.add(openable);

            Stage stage = windowController.openWindow(nonModalController, scene, Modality.NONE);
            stage.setOnCloseRequest(event -> openedWindows.remove(controller));
        } else {
            setStatus("Aktuální obrazovku nejde zobrazit v novém okně", ERROR);
        }
    }

    public static void showConfirmDialog() {
        try {
            FXMLLoader dialog = buildFxmlLoader("window/confirm/confirm.fxml");
            Scene scene = new Scene(dialog.load());
            ConfirmController controller = dialog.getController();
            windowController.openWindow(controller, scene, Modality.APPLICATION_MODAL);
        } catch (IOException e) {
        }
    }
}
