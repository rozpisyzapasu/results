/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.components;

public class Converters {

    public static int parseInt(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException ee) {
            return 0;
        }
    }
}
