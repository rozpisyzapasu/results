/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.components;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableRow;
import sportsscheduler.results.fx.ResultsFx;

import java.io.IOException;

public abstract class EditButtonCell<T> extends TableCell<T, T> {

    private Button button;

    public EditButtonCell() {
        try {
            button = ResultsFx.buildFxmlLoader("components/edit-button.fxml").load();
            button.setText(getButtonText());
            addListener();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void addListener() {
        button.setOnAction(actionEvent -> {
            if (isNonEmptyRow()) {
                TableRow tableRow = getTableRow();
                onAction((T) tableRow.getItem());
            }
        });
    }

    protected void updateItem(T o, boolean empty) {
        super.updateItem(o, empty);
        if (isNonEmptyRow()) {
            setGraphic(button);
        } else {
            setGraphic(null);
        }
    }

    protected boolean isNonEmptyRow() {
        return getTableRow() != null &&
                getTableRow().getItem() != null;
    }

    protected abstract String getButtonText();

    protected abstract void onAction(T row);
}
