/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.components;

import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Paths;

public class FileDialog {

    private TextField field;
    private boolean isOpenDialog;

    public FileDialog(TextField field) {
        this.field = field;
        isOpenDialog = true;
    }

    public void canCreateNewFile() {
        this.isOpenDialog = false;
    }

    public void openFileDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(getInitialDirectory());
        fileChooser.getExtensionFilters().add(getExtensionFilter());
        File file = getFile(fileChooser);
        if (file != null) {
            field.setText(file.getPath());
        }
    }

    private File getFile(FileChooser fileChooser) {
        File file;
        if (isOpenDialog) {
            file = fileChooser.showOpenDialog(null);
        } else {
            file = fileChooser.showSaveDialog(null);
        }
        return file;
    }

    private File getInitialDirectory() {
        String currentPath = field.getText();
        if (!currentPath.isEmpty()) {
            File f = new File(currentPath);
            if (f.exists()) {
                return f.getParentFile();
            }
        }
        return Paths.get("./").toFile();
    }

    private FileChooser.ExtensionFilter getExtensionFilter() {
        return new FileChooser.ExtensionFilter("JSON soubory (*.json)", "*.json");
    }
}
