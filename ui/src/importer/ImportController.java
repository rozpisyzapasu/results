/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.importer;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.components.FileDialog;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.fx.window.StatusType;
import sportsscheduler.results.out.importer.ImportPresenter;
import sportsscheduler.results.out.system.JsonResult.JsonError;

import static sportsscheduler.results.fx.ResultsFx.*;

public class ImportController implements ImportPresenter {

    @FXML
    private TextField path;
    private FileDialog fileDialog;
    private static String lastPath = "";

    public void initialize() {
        path.setText(lastPath);
        fileDialog = new FileDialog(path);
    }

    public void openFileDialog() {
        fileDialog.openFileDialog();
        lastPath = path.getText();
    }

    public void openWebScheduler() {
        redirectToWeb("https://www.rozpisyzapasu.cz/rozpisy/rozlosovat/");
    }

    public void importSchedule() {
        try {
            UseCaseInvoker.execute(UseCaseInvoker.SCHEDULE_IMPORT, path.getText(), this);
        } catch (Exception e) {
            showException(e);
        }
    }

    public void nonExistentSchedule(JsonError error) {
        setStatus(translateErrorMessage(error), StatusType.ERROR);
    }

    private String translateErrorMessage(JsonError error) {
        switch (error) {
            case NO_SCHEDULE:
                return "Načtěte rozpis z počítače nebo vložte odkaz na rozpis z www.rozpisyzapasu.cz";
            case ALREADY_IMPORTED:
                return "Rozpis už je načtený! Zvolte jiný rozpis nebo vygenerujte nový rozpis na www.rozpisyzapasu.cz";
            case INVALID_RESPONSE_CODE:
                return "Rozpis už není dostupný. Vygenerujte nový rozpis na webu www.rozpisyzapasu.cz";
            case INVALID_JSON_CONTENT:
                return "Vložte odkaz na JSON verzi rozpisu z webu www.rozpisyzapasu.cz";
            case UNKNOWN_ERROR:
            default:
                return "Zadejte cestu k existujícímu rozpisu vygenerovanému na webu www.rozpisyzapasu.cz";
        }
    }

    public void scheduleImported(String scheduleName) {
        ResultsFx.changeSchedule(scheduleName);
    }
}
