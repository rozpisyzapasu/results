/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import sportsscheduler.results.fx.components.EditButtonCell;
import sportsscheduler.results.fx.matches.score.ScoreController;

import static sportsscheduler.results.fx.ResultsFx.replaceWindow;

public class EditScoreCell extends EditButtonCell<MatchRow> {

    protected String getButtonText() {
        return "Upravit skóre";
    }

    protected void onAction(MatchRow row) {
        ScoreController.match = row.getMatch();
        replaceWindow("matches/score/score.fxml");
    }

    protected boolean isNonEmptyRow() {
        return super.isNonEmptyRow() &&
                getTableRow().getItem() instanceof MatchRow;
    }
}
