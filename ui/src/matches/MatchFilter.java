/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import java.util.ArrayList;
import java.util.List;

public class MatchFilter implements Cloneable {

    public int period = 0, round = 0;
    public boolean showPlayedMatches = true;
    public boolean showScorers = false;
    public String team = "";
    public final List<String> visibleColumns = new ArrayList<>();

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
