/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.Context;
import sportsscheduler.results.overview.PresentableMatch;

public class MatchRow extends Row {

    public static final int DYNAMIC_DATE = 0;
    public static final int FULL_DATE = 1;

    private final int id;
    private final PresentableMatch match;
    private final SimpleStringProperty teamHome, teamAway, scoreHome, scoreAway;

    public MatchRow(PresentableMatch match, int id, int flags) {
        this.match = match;
        this.id = id;
        rank.set(match.idMatch + 1);
        field.set(match.attributes.field);
        round.set(match.attributes.round);
        period.set(match.attributes.period);
        if (match.attributes.referee != null) {
            referee.set(match.attributes.referee);
        }
        if (match.attributes.date != null) {
            date.set(Context.currentSchedule.settings.formatDate(match.attributes.date, flags == MatchRow.FULL_DATE));
        }

        if (match.isPlayoffMatch) {
            teamHome = new SimpleStringProperty(String.format("(%d) %s", match.home.playoffSeed, match.home.team));
            teamAway = new SimpleStringProperty(String.format("%s (%d)", match.away.team, match.away.playoffSeed));
        } else {
            teamHome = new SimpleStringProperty(match.home.team);
            teamAway = new SimpleStringProperty(match.away.team);
        }

        if (match.isPlayed) {
            scoreHome = new SimpleStringProperty(match.home.goalsCount.toString());
            scoreAway = new SimpleStringProperty(match.away.goalsCount.toString());
        } else {
            scoreHome = new SimpleStringProperty("");
            scoreAway = new SimpleStringProperty("");
        }
    }

    public int getId() {
        return id;
    }

    public PresentableMatch getMatch() {
        return match;
    }

    public boolean isMatch() {
        return true;
    }

    public SimpleStringProperty teamHomeProperty() {
        return teamHome;
    }

    public SimpleStringProperty teamAwayProperty() {
        return teamAway;
    }

    public SimpleStringProperty scoreHomeProperty() {
        return scoreHome;
    }

    public SimpleStringProperty scoreAwayProperty() {
        return scoreAway;
    }

    public boolean isVisible(MatchFilter filter) {
        return isFromPeriod(filter) &&
                isFromRound(filter) &&
                isPlayedVisible(filter) &&
                isTeamFromMatch(filter);
    }

    private boolean isFromPeriod(MatchFilter filter) {
        return filter.period == 0 || filter.period == match.attributes.period;
    }

    private boolean isFromRound(MatchFilter filter) {
        return filter.round == 0 || filter.round == match.attributes.round;
    }

    private boolean isPlayedVisible(MatchFilter filter) {
        return !match.isPlayed || filter.showPlayedMatches;
    }

    private boolean isTeamFromMatch(MatchFilter filter) {
        String team = filter.team;
        return team.isEmpty() ||
                containsTeam(match.home.team, team) ||
                containsTeam(match.away.team, team) ||
                containsTeam(match.attributes.referee, team);
    }

    private boolean containsTeam(String text, String team) {
        return text != null && text.toLowerCase().contains(team.toLowerCase());
    }
}
