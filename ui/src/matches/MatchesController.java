/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.AppContext;
import sportsscheduler.results.fx.PlayersControls;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.fx.matches.score.ScoreController;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.util.Iterator;
import java.util.stream.Collectors;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.resetMaxColumnWith;
import static sportsscheduler.results.fx.FxUtils.tableColumnCell;
import static sportsscheduler.results.fx.components.Converters.parseInt;

public class MatchesController extends Openable {

    @FXML
    private HBox periods, controls;
    @FXML
    private ToggleGroup periodsGroup;
    @FXML
    private TextField round, team;
    @FXML
    private CheckBox showPlayedMatches, showScorers;
    @FXML
    private TableView<Row> table;
    private final ObservableList<Row> allMatches = FXCollections.observableArrayList();
    private final ObservableList<Row> filteredMatches = FXCollections.observableArrayList();
    private MatchFilter matchFilter;

    public void initialize() {
        matchFilter = AppContext.matchFilter;
        initTable();
        setPeriodFilter();
        loadFilterState();
        setRoundFilter();
        setTeamFilter();
        setPlayedMatchesFilter();
        setScorersFilter();
        loadRows();
        checkOptionalColumns();
        PlayersControls.show(controls);
        loadColumnsVisibility();
    }

    public void onOpen() {
        try {
            matchFilter = (MatchFilter) matchFilter.clone();
            resetMaxColumnWith(table);
        } catch (CloneNotSupportedException e) {
        }
    }

    public void refresh() {
        allMatches.clear();
        filteredMatches.clear();
        loadRows();
        filter();
    }

    private void initTable() {
        table.setItems(filteredMatches);
        table.setRowFactory(r -> new StyleableTableRow());
        tableColumnCell(table, 1, tableColumn -> new EditScoreCell());
        tableColumnCell(table, 3, tableColumn -> new ResultTypeCell());
        Label rank = new Label("");
        rank.setTooltip(new Tooltip("Pořadí zápasu"));
        table.getColumns().get(0).setGraphic(rank);
    }

    private void setPeriodFilter() {
        for (int i = 1; i <= Context.schedule.settings.periodsCount; i++) {
            ToggleButton b = new ToggleButton();
            b.setText(i + "");
            b.setToggleGroup(periodsGroup);
            b.setOnAction(event -> {
                matchFilter.period = b.isSelected() ? Integer.parseInt(b.getText()) : 0;
                filter();
            });
            periods.getChildren().add(b);
        }
    }

    private void loadFilterState() {
        if (matchFilter.round > 0) {
            round.setText(matchFilter.round + "");
        }
        team.setText(matchFilter.team + "");
        showPlayedMatches.setSelected(matchFilter.showPlayedMatches);
        showScorers.setSelected(matchFilter.showScorers);
        if (matchFilter.period > 0) { // first child is Label :)
            ToggleButton selectedPeriod = (ToggleButton) periods.getChildren().get(matchFilter.period);
            selectedPeriod.setSelected(true);
        }
    }

    private void setRoundFilter() {
        round.textProperty().addListener((observable, oldValue, newValue) -> {
            matchFilter.round = parseInt(newValue);
            filter();
        });
    }

    private void setTeamFilter() {
        team.textProperty().addListener((observable, oldValue, newValue) -> {
            matchFilter.team = newValue;
            filter();
        });
    }

    private void setPlayedMatchesFilter() {
        showPlayedMatches.setOnAction(event -> {
            matchFilter.showPlayedMatches = showPlayedMatches.isSelected();
            filter();
        });
    }

    private void setScorersFilter() {
        showScorers.setOnAction(event -> {
            matchFilter.showScorers = showScorers.isSelected();
            filter();
        });
    }

    private Integer focused, index;

    private void filter() {
        focused = null;
        index = 0;
        filteredMatches.clear();
        filteredMatches.addAll(allMatches.stream().filter(r -> {
            boolean isVisible = r.isVisible(matchFilter);
            if (r.isMatch() && isVisible) {
                MatchRow row = (MatchRow) r;
                if (row.getMatch() == ScoreController.match) {
                    r.markFocused();
                    focused = index;
                }
            }
            if (isVisible) {
                index++;
            }
            return isVisible;
        }).collect(Collectors.toList()));
        initTableView(table, "Neexistuje žádný zápas");
        if (focused != null) {
            table.scrollTo(focused);
        }
    }

    private void loadRows() {
        int id = 0;
        for (PresentableMatch m : Context.schedule.matches) {
            MatchRow r = new MatchRow(m, id++, MatchRow.DYNAMIC_DATE);
            allMatches.add(r);

            Iterator<PresentablePlayer> home = m.home.getScorers().iterator();
            Iterator<PresentablePlayer> away = m.away.getScorers().iterator();
            while (home.hasNext() || away.hasNext()) {
                ScorerRow sc = new ScorerRow(r);
                if (home.hasNext()) {
                    sc.setHomeScorer(home.next());
                }
                if (away.hasNext()) {
                    sc.setAwayScorer(away.next());
                }
                allMatches.add(sc);
            }
        }
        filter();
    }

    private void checkOptionalColumns() {
        hideColumnIf(4, !Context.schedule.settings.hasReferees);
        hideColumnIf(3, !Context.schedule.settings.hasMatchDate);
    }

    private void hideColumnIf(int columnIndex, boolean dontHaveSomething) {
        if (dontHaveSomething) {
            table.getColumns().remove(columnIndex);
        }
    }

    private void loadColumnsVisibility() {
        if (matchFilter.visibleColumns.size() > 0) {
            for (TableColumn<Row, ?> c : table.getColumns()) {
                c.setVisible(matchFilter.visibleColumns.contains(c.getText()));
            }
        }
        table.getVisibleLeafColumns().addListener((ListChangeListener<TableColumn<Row, ?>>) e -> {
            matchFilter.visibleColumns.clear();
            for (TableColumn<Row, ?> c : e.getList()) {
                matchFilter.visibleColumns.add(c.getText());
            }
        });
    }
}
