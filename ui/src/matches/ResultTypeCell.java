/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;
import sportsscheduler.results.match.ResultType;

public class ResultTypeCell extends TableCell<MatchRow, String> {

    protected void updateItem(String o, boolean empty) {
        super.updateItem(o, empty);
        if (isNonEmptyRow()) {
            ResultType type = ((MatchRow)getTableRow().getItem()).getMatch().resultType;
            setText(type.getAbbrevation());
            setTooltip(new Tooltip(type.toString()));
        } else {
            setGraphic(null);
            setTooltip(null);
        }
    }

    protected boolean isNonEmptyRow() {
        return getTableRow() != null &&
                getTableRow().getItem() instanceof MatchRow;
    }
}
