/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public abstract class Row {

    private boolean isFocused = false;
    protected final SimpleIntegerProperty rank = new SimpleIntegerProperty();
    protected final SimpleIntegerProperty period = new SimpleIntegerProperty();
    protected final SimpleIntegerProperty field = new SimpleIntegerProperty();
    protected final SimpleIntegerProperty round = new SimpleIntegerProperty();
    protected final SimpleStringProperty referee = new SimpleStringProperty("");
    protected final SimpleStringProperty date = new SimpleStringProperty("");

    abstract boolean isMatch();

    boolean isFocused() {
        return isFocused;
    }

    void markFocused() {
        isFocused = true;
    }

    public SimpleIntegerProperty rankProperty() {
        return rank;
    }

    public SimpleIntegerProperty periodProperty() {
        return period;
    }

    public SimpleIntegerProperty fieldProperty() {
        return field;
    }

    public SimpleIntegerProperty roundProperty() {
        return round;
    }

    public SimpleStringProperty refereeProperty() {
        return referee;
    }

    public SimpleStringProperty dateProperty() {
        return date;
    }

    abstract SimpleStringProperty teamHomeProperty();

    abstract SimpleStringProperty teamAwayProperty();

    abstract SimpleStringProperty scoreHomeProperty();

    abstract SimpleStringProperty scoreAwayProperty();

    abstract boolean isVisible(MatchFilter filter);
}
