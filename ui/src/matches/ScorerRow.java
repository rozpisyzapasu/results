/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.stats.players.PresentablePlayer;

public class ScorerRow extends Row {

    private final MatchRow match;
    private final SimpleStringProperty scorerHome, scorerAway, goalsHome, goalsAway;

    public ScorerRow(MatchRow row) {
        match = row;
        scorerHome = new SimpleStringProperty("");
        scorerAway = new SimpleStringProperty("");
        goalsHome = new SimpleStringProperty("");
        goalsAway = new SimpleStringProperty("");
    }

    public void setHomeScorer(PresentablePlayer scorer) {
        setScorer(scorerHome, goalsHome, scorer);
    }

    public void setAwayScorer(PresentablePlayer scorer) {
        setScorer(scorerAway, goalsAway, scorer);
    }

    private void setScorer(SimpleStringProperty scorerProperty, SimpleStringProperty goals, PresentablePlayer scorer) {
        scorerProperty.set(scorer.name);
        goals.set(scorer.goalsCount + "");
    }

    public boolean isMatch() {
        return false;
    }

    public SimpleStringProperty teamHomeProperty() {
        return scorerHome;
    }

    public SimpleStringProperty teamAwayProperty() {
        return scorerAway;
    }

    public SimpleStringProperty scoreHomeProperty() {
        return goalsHome;
    }

    public SimpleStringProperty scoreAwayProperty() {
        return goalsAway;
    }

    public boolean isVisible(MatchFilter filter) {
        return match.isVisible(filter) &&
                filter.showScorers;
    }
}
