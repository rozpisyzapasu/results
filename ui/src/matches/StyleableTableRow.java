/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableRow;
import sportsscheduler.results.Context;

import java.util.ArrayList;
import java.util.List;

public class StyleableTableRow extends TableRow<Row> {

    private List<String> extraClasses = new ArrayList<>();

    protected void updateItem(Row item, boolean empty) {
        super.updateItem(item, empty);
        clearExtraClasses();
        if (!empty) {
            if (item.isMatch() && Context.schedule.groups.size() > 1) {
                extraClasses.add(getGroupClass(item.teamHomeProperty()));
                extraClasses.add(getGroupClass(item.teamAwayProperty()));
            } else if (!item.isMatch()) {
                extraClasses.add("scorers");
            }
            if (item.isFocused()) {
                extraClasses.add("focused");
            }
            addExtraClasses();
        }
    }

    private void clearExtraClasses() {
        for (String cssClass : extraClasses) {
            getStyleClass().remove(cssClass);
        }
        extraClasses.clear();
    }

    private void addExtraClasses() {
        for (String cssClass : extraClasses) {
            getStyleClass().add(cssClass);
        }
    }

    private String getGroupClass(SimpleStringProperty p) {
        String team = p.getValue();
        return String.format("g-%d", Context.schedule.getGroupIndex(team));
    }
}
