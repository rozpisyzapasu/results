/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches.score;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.fx.matches.MatchRow;
import sportsscheduler.results.fx.matches.score.stats.PlayersStatsController;
import sportsscheduler.results.fx.matches.score.stats.TeamsStatsController;
import sportsscheduler.results.fx.matches.score.stats.ValueController;
import sportsscheduler.results.fx.teams.team.TeamController;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.overview.PresentableMatch;
import sportsscheduler.results.overview.PresentableTeamInMatch;
import sportsscheduler.results.score.ScorePresenter;
import sportsscheduler.results.score.ScoreRequest;

import java.util.*;
import java.util.function.Consumer;

import static sportsscheduler.results.fx.FxUtils.incrementTextField;
import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.tableColumnVisibility;
import static sportsscheduler.results.fx.components.Converters.parseInt;
import static sportsscheduler.results.fx.ResultsFx.*;
import static sportsscheduler.results.fx.window.StatusType.*;

public class ScoreController implements ScorePresenter {

    public static PresentableMatch match;

    @FXML
    private Label teamHome, teamAway;
    @FXML
    private TextArea note;
    @FXML
    private ChoiceBox scoreType;
    @FXML
    private TableView<MatchRow> matchInfo;
    @FXML
    private FlowPane matchTabs;
    @FXML
    private ToggleGroup matchTabsToggles;
    private final ObservableList<MatchRow> rows = FXCollections.observableArrayList();
    @FXML
    private TextField scoreHome, scoreAway;
    @FXML
    private TeamsStatsController teamsStatsController;
    @FXML
    private PlayersStatsController playersStatsController;
    @FXML
    private GridPane grid;
    @FXML
    private VBox content;

    private final ObservableList<ResultType> scoreTypes = FXCollections.observableArrayList(ResultType.NORMAL, ResultType.EXTRA_TIME, ResultType.SHOOTOUT, ResultType.WIN_BY_DEFAULT);

    public void initialize() {
        loadTeam(match.home, teamHome, scoreHome);
        loadTeam(match.away, teamAway, scoreAway);
        loadMatchInfo();
        loadScoreType();
        teamsStatsController.preloadExistingStats();
        playersStatsController.preloadExistingStats();
        loadDynamicTabs();
        listenOnTabChange();
        selectFirstTab();
    }

    private void loadTeam(PresentableTeamInMatch team, Label label, TextField field) {
        String score = match.isPlayed ? team.goalsCount + "" : "";
        field.setText(score);
        label.setText(team.team);
        label.setOnMouseClicked(event -> {
            TeamController.team = team.teamDetail;
            TeamController.isFromMatch = true;
            replaceWindow("teams/team/team.fxml");
        });
    }

    private void loadMatchInfo() {
        rows.add(new MatchRow(match, match.idMatch + 1, MatchRow.FULL_DATE));
        initTableView(matchInfo, rows);
        tableColumnVisibility(matchInfo, 0, Context.schedule.settings.periodsCount > 1);
        tableColumnVisibility(matchInfo, 3, match.attributes.date != null);
        tableColumnVisibility(matchInfo, 4, Context.schedule.settings.hasReferees);
        note.setText(match.attributes.note);
    }

    private void loadScoreType() {
        scoreType.setTooltip(new Tooltip("Vyberte typ výsledku zápasu"));
        scoreType.setItems(scoreTypes);
        scoreType.getSelectionModel().select(match.resultType);
    }

    private void loadDynamicTabs() {
        ObservableList<Node> children = matchTabs.getChildren();
        Map<String, Node> tabs = new HashMap<>();
        for (Node n : children) {
            tabs.put((String) n.getUserData(), n);
        }
        if (Context.hasPlayers()) {
            children.remove(tabs.get("sets"));
        } else {
            children.remove(tabs.get("playersStats-"));
        }
        if (Context.schedule.settings.matchPeriods.size() <= 0 && !Context.hasPlayers()) {
            children.remove(tabs.get("sets"));
        }
        if (Context.schedule.settings.teamStatsNames.size() <= 0) {
            children.remove(tabs.get("teamsStats"));
        }
        if (!Context.schedule.settings.hasMatchNote) {
            children.remove(tabs.get("note"));
        }
        if (Context.getTeamUnit() == ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS) {
            int index = 1;
            for (String playerStatsName : Context.schedule.settings.playerStatsNames) {
                ToggleButton button = new ToggleButton();
                button.setToggleGroup(matchTabsToggles);
                button.setUserData("playersStats-" + playerStatsName);
                button.setText(playerStatsName);
                children.add(index++, button);
            }
        }
        int expectedRowsCount = 1 + children.size() / 8;
        int height = expectedRowsCount * 25;
        grid.getRowConstraints().get(4).setMaxHeight(height);
        grid.getRowConstraints().get(4).setPrefHeight(height);
    }

    private void listenOnTabChange() {
        matchTabsToggles.selectedToggleProperty().addListener((value, old, newt) -> {
            Toggle selectedToggle = newt == null ? old : newt;
            selectedToggle.setSelected(true);
            toggleTabContent(selectedToggle.getUserData());
        });
    }

    private void selectFirstTab() {
        if (matchTabs.getChildren().size() == 0) {
            grid.getChildren().remove(matchTabs);
            toggleTabContent("No tab");
            return;
        }
        Toggle toggle = (Toggle) matchTabs.getChildren().get(0);
        toggle.setSelected(true);
        toggleTabContent(toggle.getUserData());
    }

    private void toggleTabContent(Object rawTab) {
        boolean isPlayerStats = rawTab.toString().startsWith("playersStats-");
        String stat = isPlayerStats ? rawTab.toString().replace("playersStats-", "") : null;
        String activeTab = isPlayerStats ? "playersStats" : rawTab.toString();

        boolean isTeamStats = rawTab.equals("teamsStats") || rawTab.equals("sets");
        activeTab = isTeamStats ? "teamsStats" : activeTab;

        for (Node node : content.getChildren()) {
            ObservableList<String> css = node.getStyleClass();
            String cssTab = css.get(css.size() - 1).substring(7);
            boolean isTabVisible = cssTab.equals(activeTab);
            node.setVisible(isTabVisible);
            node.setManaged(isTabVisible);
            if (isTabVisible && isPlayerStats) {
                playersStatsController.initialize(stat, buildScoreUpdaters(stat));
            }
            if (isTabVisible && isTeamStats) {
                if (rawTab.equals("teamsStats")) {
                    teamsStatsController.initialize(
                            Context.currentSchedule.settings.teamStatsNames,
                            null
                    );
                } else {
                    teamsStatsController.initialize(
                            Context.currentSchedule.settings.matchPeriods,
                            createTeamScoreUpdater()
                    );
                }
            }
        }
    }

    private Map<Boolean, Consumer<ValueController>>buildScoreUpdaters(String stat) {
        Map<Boolean, Consumer<ValueController>> updaters = new HashMap<>();
        if (stat.isEmpty()) {
            updaters.put(false, c1 -> incrementTextField(scoreHome, c1.lastChange));
            updaters.put(true, c -> incrementTextField(scoreAway, c.lastChange));
        }
        return updaters;
    }

    private Consumer<ValueController> createTeamScoreUpdater() {
        return (ignored) -> {
            Iterator<ValueController> iterator = teamsStatsController.getCurrentControllers();
            int currentHome = 0, currentAway = 0;
            while (iterator.hasNext()) {
                ValueController home = iterator.next();
                ValueController away = iterator.next();
                if (home.getCurrent() > away.getCurrent()) {
                    currentHome++;
                } else if (home.getCurrent() < away.getCurrent()) {
                    currentAway++;
                }
            }
            scoreHome.setText(currentHome + "");
            scoreAway.setText(currentAway + "");
        };
    }

    public void updateScore() {
        ScoreRequest r = new ScoreRequest();
        r.goalsHome = parseInt(scoreHome.getText());
        r.goalsAway = parseInt(scoreAway.getText());
        r.resultType = (ResultType) scoreType.getValue();
        teamsStatsController.addStats(r);
        playersStatsController.addStats(r);
        executeUseCase(r);
    }

    public void deleteScore() {
        ScoreRequest r = new ScoreRequest();
        executeUseCase(r);
    }

    private void executeUseCase(ScoreRequest r) {
        r.idMatch = match.idMatch;
        r.note = note.getText();
        UseCaseInvoker.execute(UseCaseInvoker.MATCH_SCORE, r, this);
    }

    public void backToMatches() {
        replaceWindow("matches/matches.fxml");
    }

    public void nonExistentMatch() {
        setStatus("Vypadá to, že zápas neexistuje", ERROR);
    }

    public void negativeScore() {
        setStatus("Skóre nesmí být negativní", ERROR);
    }

    public void noChangeInScore() {
        setStatus("Žádná změna ve skóre", ERROR);
    }

    public void scoreChanged() {
        ResultsFx.reloadData();
        backToMatches();
        setStatus("Skóre bylo úspěšně upraveno", SUCCESS);
    }
}
