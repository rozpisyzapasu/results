/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches.score.stats;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import sportsscheduler.results.fx.matches.score.ScoreController;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.overview.PresentableTeamInMatch;
import sportsscheduler.results.score.ScoreRequest;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static sportsscheduler.results.fx.ResultsFx.buildFxmlLoader;

public class PlayersStatsController {

    @FXML
    private VBox playersHome, playersAway;
    private final List<ValueController> controllers = new ArrayList<>();
    private final Map<PresentablePlayer, Stats> values = new HashMap<>();
    private String statName;

    public void preloadExistingStats() {
        PresentableTeamInMatch[] teams = new PresentableTeamInMatch[] { ScoreController.match.home, ScoreController.match.away };
        for (PresentableTeamInMatch team : teams) {
            for (PresentablePlayer scorer : team.getAllPlayers()) {
                values.put(scorer, new Stats());
                for (Map.Entry<String, Integer> stat : scorer.advancedStats.iterate()) {
                    values.get(scorer).put(stat);
                }
                values.get(scorer).put("", scorer.goalsCount);
            }
        }
    }

    public void initialize(String statName, Map<Boolean, Consumer<ValueController>> updaters) {
        reset(statName);
        try {
            loadTeamPlayers(ScoreController.match.home, playersHome, updaters);
            loadTeamPlayers(ScoreController.match.away, playersAway, updaters);
        } catch (IOException e) {
        }
    }

    private void reset(String statName) {
        saveCurrentValues();
        controllers.clear();
        playersHome.getChildren().clear();
        playersAway.getChildren().clear();
        this.statName = statName;
    }

    private void saveCurrentValues() {
        for (ValueController controller : controllers) {
            PresentablePlayer scorer = (PresentablePlayer) controller.userData;
            values.get(scorer).put(statName, controller.getCurrent());
        }
    }

    private void loadTeamPlayers(PresentableTeamInMatch team, VBox box, Map<Boolean, Consumer<ValueController>> updaters) throws IOException {
        boolean isAwayTeam = team.team.equals(ScoreController.match.away.team);
        for (PresentablePlayer scorer : team.getAllPlayers()) {
            Stats stats = values.get(scorer);
            ValueController controller = loadCell(box);
            controller.initialize(scorer.name, stats.get(statName), updaters.getOrDefault(isAwayTeam, null), scorer);
            if (isAwayTeam) {
                controller.reverseControls();
            }
            controllers.add(controller);
        }
    }

    private ValueController loadCell(VBox box) throws IOException {
        FXMLLoader loader = buildFxmlLoader("matches/score/stats/value.fxml");
        box.getChildren().add(loader.load());
        return loader.getController();
    }

    public void addStats(ScoreRequest r) {
        saveCurrentValues();
        for (Map.Entry<PresentablePlayer, Stats> entry : values.entrySet()) {
            PresentablePlayer scorer = entry.getKey();
            for (Map.Entry<String, Integer> stat : entry.getValue().iterate()) {
                if (stat.getValue() > 0) {
                    if (isScoreEdited(stat.getKey())) {
                        r.players.addGoals(scorer.name, scorer.team, stat.getValue());
                    } else {
                        r.players.addStat(scorer.name, scorer.team, stat.getKey(), stat.getValue());
                    }
                }
            }
        }
    }

    private boolean isScoreEdited(String name) {
        return name.isEmpty();
    }
}
