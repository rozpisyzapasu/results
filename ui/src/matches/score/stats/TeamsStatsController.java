/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches.score.stats;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;
import sportsscheduler.results.fx.matches.score.ScoreController;
import sportsscheduler.results.score.ScoreRequest;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

import static sportsscheduler.results.fx.ResultsFx.buildFxmlLoader;

public class TeamsStatsController {

    @FXML
    private GridPane rows;
    private final List<ValueController> controllers = new ArrayList<>();

    private List<String> titles;
    private final Map<String, Map.Entry<Integer, Integer>> values = new LinkedHashMap<>();

    public void preloadExistingStats() {
        Iterator<Integer> awayValues = ScoreController.match.away.advancedStats.iterateValues();
        for (Map.Entry<String, Integer> stat : ScoreController.match.home.advancedStats.iterate()) {
            this.values.put(stat.getKey(), new AbstractMap.SimpleEntry<>(stat.getValue(), awayValues.next()));
        }
    }

    public void initialize(List<String> titles, Consumer<ValueController> updater) {
        reset();
        this.titles = titles;
        try {
            for (String name : titles) {
                addRow(name, updater);
            }
        } catch (IOException e) {
        }
    }

    private void reset() {
        saveCurrentValues();
        controllers.clear();
        rows.getChildren().clear();
    }

    private void saveCurrentValues() {
        if (controllers.size() == 0) {
            return;
        }
        Iterator<ValueController> iterator = controllers.iterator();
        for (String name : titles) {
            int home = iterator.next().getCurrent();
            int away = iterator.next().getCurrent();
            if (home > 0 || away > 0) {
                values.put(name, new AbstractMap.SimpleEntry<>(home, away));
            }
        }
    }

    private void addRow(String name, Consumer<ValueController> updater) throws IOException {
        loadCell(name, false, updater);
        loadCell(name, true, updater);
    }

    private void loadCell(String text, boolean isAwayTeam, Consumer<ValueController> updater) throws IOException {
        FXMLLoader loader = buildFxmlLoader("matches/score/stats/value.fxml");
        rows.add(
            loader.load(),
            isAwayTeam ? 1: 0,
            rows.getChildren().size() / 2
        );
        Map.Entry<Integer, Integer> stat = values.getOrDefault(text, new AbstractMap.SimpleEntry<>(0, 0));
        int value = isAwayTeam ? stat.getValue() : stat.getKey();
        ValueController controller = loader.getController();
        controller.initialize(text, value, updater, null);
        if (isAwayTeam) {
            controller.reverseControls();
            controller.hideLabel();
        }
        controllers.add(controller);
    }

    public void addStats(ScoreRequest r) {
        saveCurrentValues();
        for (Map.Entry<String, Map.Entry<Integer, Integer>> entry : values.entrySet()) {
            Map.Entry<Integer, Integer> stat = entry.getValue();
            if (stat.getKey() > 0 || stat.getValue() > 0) {
                r.addTeamStat(entry.getKey(), stat.getKey(), stat.getValue());
            }
        }
    }

    public Iterator<ValueController> getCurrentControllers() {
        return controllers.iterator();
    }
}
