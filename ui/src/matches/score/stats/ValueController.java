/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.matches.score.stats;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;

import java.util.function.Consumer;

import static sportsscheduler.results.fx.FxUtils.incrementTextField;
import static sportsscheduler.results.fx.components.Converters.parseInt;

public class ValueController {

    @FXML
    private HBox hbox, space;
    @FXML
    private Label label;
    @FXML
    private Button increment, decrement;
    @FXML
    private TextField value;
    private Consumer<ValueController> updateListener;

    public int lastChange;
    public Object userData;

    public void initialize(String name, int value, Consumer<ValueController> updater, Object userData) {
        loadControl(name, value);
        this.userData = userData;
        updateListener = updater != null ? updater : (c) -> {};
        highlightScorerOnHover(increment, userData != null);
        highlightScorerOnHover(decrement, userData != null);
    }

    private void loadControl(String name, int value) {
        this.label.setText(name);
        if (value != 0) {
            this.value.setText(value + "");
        }
    }

    public void highlightScorerOnHover(Button button, boolean isFieldChangeIgnored) {
        button.setFocusTraversable(false);
        button.setOnMouseEntered(t -> label.setStyle(" -fx-font-weight: bold"));
        button.setOnMouseExited(t -> label.setStyle(" -fx-font-weight: normal"));
        if (isFieldChangeIgnored) {
            return;
        }
        value.textProperty().addListener((observable, oldValue, newValue) -> {
            updateListener.accept(this);
        });
    }

    public void reverseControls() {
        hbox.getChildren().clear();
        hbox.getChildren().addAll(value, label, space, decrement, increment);
        hbox.setAlignment(Pos.TOP_LEFT);
        label.setTextAlignment(TextAlignment.LEFT);
    }

    public void hideLabel() {
        label.setVisible(false);
    }

    public void updateValue(ActionEvent e) {
        lastChange = e.getSource().equals(increment) ? 1 : -1;
        if (getCurrent() + lastChange >= 0) {
            incrementTextField(value, lastChange);
            updateListener.accept(this);
        }
    }

    public int getCurrent() {
        return parseInt(value.getText());
    }
}
