/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.settings;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.settings.EditSettingsPresenter;
import sportsscheduler.results.settings.EditSettingsRequest;

import java.util.*;
import java.util.function.Consumer;

import static sportsscheduler.results.fx.ResultsFx.reloadData;
import static sportsscheduler.results.fx.ResultsFx.replaceTranslatedWindow;
import static sportsscheduler.results.fx.components.Converters.parseInt;

public class SettingsController implements EditSettingsPresenter {

    @FXML
    private GridPane grid;
    @FXML
    private ToggleGroup teamUnit, matchUnit;
    @FXML
    private HBox teamValues, matchValues;
    @FXML
    private VBox advancedStats;
    @FXML
    private TextField matchPeriodsCount, matchPeriods, teamStats, playerStats;
    @FXML
    private Label periodsLabel;
    @FXML
    private CheckBox hasMatchNote, hasAdvancedStats;
    private String redirectedPage;

    private Map<String, String> translations = new HashMap<String, String>(){{
        put(ScheduleSettings.Units.Team.TEAM_WITH_PLAYERS.toString(), "Tým");
        put(ScheduleSettings.Units.Team.PLAYER.toString(), "Hráč");
        put(ScheduleSettings.Units.Match.GOAL.toString(), "Góly");
        put(ScheduleSettings.Units.Match.SET.toString(), "Sety");
    }};

    public void initialize() {
        loadRadioButtons(teamUnit, teamValues, ScheduleSettings.Units.Team.values(), Context.getTeamUnit());
        loadRadioButtons(matchUnit, matchValues, ScheduleSettings.Units.Match.values(), Context.getMatchUnit());
        hasMatchNote.setSelected(Context.schedule.settings.hasMatchNote);
        hasAdvancedStats.setSelected(Context.schedule.settings.hasAdvancedStats());
        initMatchPeriods();
        loadPeriodsVisibility();
        loadStatsVisibility();
        listToCsv(matchPeriods, Context.schedule.settings.matchPeriods);
        listToCsv(teamStats, Context.schedule.settings.teamStatsNames);
        listToCsv(playerStats, Context.schedule.settings.playerStatsNames);
    }

    private void loadStatsVisibility() {
        Consumer<Boolean> toggle = isSelected -> {
            grid.getRowConstraints().get(1).setPrefHeight(isSelected ? 140 : 0);
            advancedStats.setVisible(isSelected);
        };
        hasAdvancedStats.selectedProperty().addListener((observable, oldValue, newValue) -> toggle.accept(newValue));
        toggle.accept(hasAdvancedStats.isSelected());
    }

    public void toggleMatchNote() {
        hasMatchNote.setSelected(!hasMatchNote.isSelected());
    }

    public void toggleAdvancedStats() {
        hasAdvancedStats.setSelected(!hasAdvancedStats.isSelected());
    }

    private void loadRadioButtons(ToggleGroup group, HBox box, Object[] values, Object currentValue) {
        for (Object value : values) {
            addRadioButton(group, box, value, currentValue);
        }
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> saveSettings(false));
    }

    private void addRadioButton(ToggleGroup group, HBox box, Object value, Object currentValue) {
        ToggleButton b = new ToggleButton();
        b.setToggleGroup(group);
        b.setUserData(value);
        b.setSelected(value == currentValue);
        b.setText(translations.get(value.toString()));
        box.getChildren().add(b);
    }

    private void initMatchPeriods() {
        int periodsCount = Context.schedule.settings.matchPeriods.size();
        if (periodsCount != 0) {
            matchPeriodsCount.setText(periodsCount + "");
        }
        matchPeriodsCount.textProperty().addListener((observable, oldValue, newValue) -> {
            matchPeriods.setText(String.join(",", generateSets()));
        });
    }

    private String generateSets() {
        List<String> sets = new ArrayList<>();
        for (int i = 1; i <= parseInt(matchPeriodsCount.getText()); i++) {
            String period = String.format("%d.set", i);
            sets.add(period);
        }
        return String.join(",", sets);
    }

    private void listToCsv(TextField field, List<String> list) {
        field.setText(String.join(",", list));
    }

    private List<String> csvToList(TextField field) {
        if (field.getText().isEmpty()) {
            return Arrays.asList();
        }
        return Arrays.asList(field.getText().split("\\s*,\\s*"));
    }

    private void loadPeriodsVisibility() {
        boolean arePeriodsVisible = Context.getMatchUnit().equals(ScheduleSettings.Units.Match.SET);
        matchPeriods.setVisible(arePeriodsVisible);
        matchPeriodsCount.setVisible(arePeriodsVisible);
        periodsLabel.setVisible(arePeriodsVisible);
    }

    public void saveSettingsAndRedirect() {
        saveSettings(true);
    }

    public void saveSettings(boolean isRedirected) {
        redirectedPage = isRedirected ? null : "settings/settings.fxml";
        EditSettingsRequest r = new EditSettingsRequest();
        r.teamUnit = (ScheduleSettings.Units.Team) teamUnit.getSelectedToggle().getUserData();
        r.matchUnit = (ScheduleSettings.Units.Match) matchUnit.getSelectedToggle().getUserData();
        r.matchPeriods = csvToList(matchPeriods);
        r.teamStatsNames = csvToList(teamStats);
        r.playerStatsNames = csvToList(playerStats);
        r.hasMatchNote = hasMatchNote.isSelected();
        r.hasAdvancedStats = hasAdvancedStats.isSelected();
        UseCaseInvoker.execute(UseCaseInvoker.EDIT_SETTINGS, r, this);
    }

    public void settingsSaved() {
        loadPeriodsVisibility();
        replaceTranslatedWindow(redirectedPage);
        reloadData();
    }
}
