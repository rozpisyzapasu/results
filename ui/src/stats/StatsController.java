/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats;

import javafx.fxml.FXML;
import javafx.scene.control.TabPane;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.Openable;

public class StatsController extends Openable {

    @FXML
    private TabPane tabs;
    @FXML
    private Openable scorersController, goalkeepersController, scheduleController, advancedStatsController;

    public void initialize() {
        if (!Context.hasPlayers()) {
            tabs.getTabs().remove(0);
        }
        if (!Context.schedule.settings.hasAdvancedStats() && Context.schedule.settings.matchPeriods.isEmpty()) {
            tabs.getTabs().remove(tabs.getTabs().size() - 2);
        }
    }

    public void onOpen() {
        scorersController.onOpen();
        goalkeepersController.onOpen();
        advancedStatsController.onOpen();
    }

    public void refresh() {
        scorersController.refresh();
        goalkeepersController.refresh();
        advancedStatsController.refresh();
        scheduleController.refresh();
    }
}
