/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.goalkeepers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.stats.goalkeepers.PresentableGoalkeeper;

public class GoalkeeperRow {

    private final SimpleStringProperty team;
    private final SimpleIntegerProperty goals, matches;

    public GoalkeeperRow(PresentableGoalkeeper g) {
        team = new SimpleStringProperty(g.team);
        goals = new SimpleIntegerProperty(g.goalsAgainst);
        matches = new SimpleIntegerProperty(g.matchesCount);
    }

    public SimpleStringProperty teamProperty() {
        return team;
    }

    public SimpleIntegerProperty goalsProperty() {
        return goals;
    }

    public SimpleIntegerProperty matchesProperty() {
    return matches;
}
}
