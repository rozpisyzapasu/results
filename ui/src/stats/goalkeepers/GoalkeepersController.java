/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.goalkeepers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.stats.goalkeepers.PresentableGoalkeeper;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.resetMaxColumnWith;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class GoalkeepersController extends Openable {

    @FXML
    private TableView table;
    private final ObservableList<GoalkeeperRow> rows = FXCollections.observableArrayList();

    public void initialize() {
        refresh();
    }

    public void onOpen() {
        resetMaxColumnWith(table);
    }

    public void refresh() {
        rows.clear();
        for (PresentableGoalkeeper g : Context.goalkeepers.all) {
            GoalkeeperRow row = new GoalkeeperRow(g);
            rows.add(row);
        }
        initTableView(table, rows, trans("%teams.no"));
    }
}
