/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.schedule;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.stats.schedule.ScheduleStats;

public class ScheduleController extends Openable {

    @FXML
    private VBox stats;
    @FXML
    private Label teamsCount, groupsCount, fieldsCount, playersCount, matchesCount, goalsCount, goalsAverage;

    @FXML
    private PieChart matchesChart;

    public void initialize() {
        refresh();
    }

    public void refresh() {
        ScheduleStats stats = Context.stats;
        teamsCount.setText(stats.teamsCount + "");
        groupsCount.setText(stats.groupsCount + "");
        fieldsCount.setText(stats.fieldsCount + "");
        playersCount.setText(stats.playersCount + "");
        matchesCount.setText(stats.matchesCount + "");
        goalsCount.setText(stats.goalsCount + "");
        goalsAverage.setText(String.format("%.1f", stats.averageGoalsInMatch));

        matchesChart.getData().clear();
        addToPie("Zbývající zápasy", stats.countUpcomingMatches());
        addToPie("Odehrané zápasy", stats.playedMatchesCount);

        removeIfCountIs(groupsCount, 1);
        removeIfCountIs(fieldsCount, 1);
        removeIfCountIs(playersCount, 0);
    }

    private void removeIfCountIs(Label label, int count) {
        if (label.getText().equals(count + "")) {
            HBox box = (HBox) label.getParent();
            stats.getChildren().remove(box);
        }
    }

    private void addToPie(String title, int count) {
        matchesChart.getData().add(new PieChart.Data(String.format("%s (%d)", title, count), count));
    }
}
