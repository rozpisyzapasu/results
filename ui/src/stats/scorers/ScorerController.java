/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.scorers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.stats.players.PresentablePlayer;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.resetMaxColumnWith;

public class ScorerController extends Openable {

    @FXML
    private TableView table;
    private final ObservableList<ScorerRow> rows = FXCollections.observableArrayList();

    public void initialize() {
        refresh();
    }

    public void onOpen() {
        resetMaxColumnWith(table);
    }

    public void refresh() {
        rows.clear();
        for (PresentablePlayer p : Context.players) {
            ScorerRow row = new ScorerRow(p);
            rows.add(row);
        }
        initTableView(table, rows, "Neexistuje žádný hráč");
    }
}
