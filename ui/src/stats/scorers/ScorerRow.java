/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.scorers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.stats.players.PresentablePlayer;

public class ScorerRow {

    private final SimpleStringProperty name, team;
    private final SimpleIntegerProperty goals;

    public ScorerRow(PresentablePlayer player) {
        name = new SimpleStringProperty(player.name);
        team = new SimpleStringProperty(player.team);
        goals = new SimpleIntegerProperty(player.goalsCount);
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public SimpleStringProperty teamProperty() {
        return team;
    }

    public SimpleIntegerProperty goalsProperty() {
        return goals;
    }
}
