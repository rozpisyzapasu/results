/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.stats;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import sportsscheduler.results.Context;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.match.Stats;
import sportsscheduler.results.overview.PresentableTeam;
import sportsscheduler.results.stats.players.PresentablePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.resetMaxColumnWith;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class AdvancedStatsController extends Openable {

    @FXML
    private HBox filters;
    @FXML
    private TextField search;
    private List<CheckBox> typeFilters = new ArrayList<>();
    @FXML
    private TableView table;
    private final ObservableList<StatRow> allRows = FXCollections.observableArrayList();
    private final ObservableList<StatRow> filteredRows = FXCollections.observableArrayList();

    public void initialize() {
        buildFilters();
        refresh();
    }

    private void buildFilters() {
        search.textProperty().addListener((observable, oldValue, newValue) -> filterRows());
        for (StatRow.Type type : StatRow.Type.values()) {
            if (
                    (!Context.hasPlayers() && type.equals(StatRow.Type.PLAYER_STATS)) ||
                    (Context.hasPlayers() && type.equals(StatRow.Type.SETS))
            ) {
                continue;
            }
            CheckBox checkbox = new CheckBox();
            checkbox.setSelected(true);
            checkbox.setUserData(type);
            checkbox.setOnAction(event -> filterRows());
            Label label = new Label();
            label.setText(trans(type.toString()));
            label.setOnMouseClicked(event -> {
                checkbox.setSelected(!checkbox.isSelected());
                filterRows();
            });
            Separator separator = new Separator();
            separator.setOrientation(Orientation.VERTICAL);
            separator.setPrefHeight(200);
            filters.getChildren().addAll(label, checkbox, separator);
            typeFilters.add(checkbox);
        }
        filters.getChildren().remove(filters.getChildren().size() - 1);
    }

    public void onOpen() {
        resetMaxColumnWith(table);
    }

    public void refresh() {
        loadRows();
        filterRows();
    }

    private void loadRows() {
        allRows.clear();
        for (PresentableTeam team : Context.schedule.teams) {
            for (Map.Entry<String, Integer> entry : team.advancedStats.iterate()) {
                if (Context.schedule.settings.isTeamStat(entry.getKey()) || Context.schedule.settings.isPlayerStat(entry.getKey())) {
                    StatRow.Type type = Context.schedule.settings.isMatchPeriod(entry.getKey()) ? StatRow.Type.SETS : StatRow.Type.TEAM_STATS;
                    allRows.add(new StatRow(entry.getKey(), entry.getValue(), team.name, "", type));
                }
            }
            Stats teamPlayerStats = new Stats();
            for (PresentablePlayer player : team.players) {
                teamPlayerStats.update(player.advancedStats, 1);
                for (Map.Entry<String, Integer> entry : player.advancedStats.iterate()) {
                    if (Context.schedule.settings.isPlayerStat(entry.getKey())) {
                        allRows.add(new StatRow(entry.getKey(), entry.getValue(), team.name, player.name, StatRow.Type.PLAYER_STATS));
                    }
                }
            }
            for (Map.Entry<String, Integer> entry : teamPlayerStats.iterate()) {
                if (Context.schedule.settings.isPlayerStat(entry.getKey())) {
                    allRows.add(new StatRow(entry.getKey(), entry.getValue(), team.name, "", StatRow.Type.TEAM_STATS));
                }
            }
        }
    }

    private void filterRows() {
        List<StatRow.Type> activeTypes = getActiveTypes();
        filteredRows.clear();
        boolean isPlayerVisible = false;
        for (StatRow row : allRows) {
            if (row.isVisible(activeTypes, search.getText())) {
                filteredRows.add(row);
                isPlayerVisible = row.isPlayer() ? true : isPlayerVisible;
            }
        }
        initTableView(table, filteredRows, "Nejsou dostupné žádné statistiky");
        reloadVisibleColumns(isPlayerVisible);
        sortTable();
    }

    private List<StatRow.Type> getActiveTypes() {
        List<StatRow.Type> activeTypes = new ArrayList<>();
        for (CheckBox checkbox : typeFilters) {
            if (checkbox.isSelected()) {
                activeTypes.add((StatRow.Type) checkbox.getUserData());
            }
        }
        return activeTypes;
    }

    private void reloadVisibleColumns(boolean isPlayerVisible) {
        ((TableColumn) table.getColumns().get(0)).setVisible(isPlayerVisible);
    }

    private void sortTable() {
        ObservableList sort = table.getSortOrder();
        sort.clear();
        sort.addAll(table.getColumns().get(2), table.getColumns().get(3));
        table.sort();
    }
}
