/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.stats.stats;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.List;

public class StatRow {

    private final SimpleStringProperty player, team, stat;
    private final SimpleIntegerProperty value;
    private final Type type;

    public StatRow(String stat, int value, String team, String player, Type type) {
        this.stat = new SimpleStringProperty(stat);
        this.value = new SimpleIntegerProperty(value);
        this.team = new SimpleStringProperty(team);
        this.player = new SimpleStringProperty(player);
        this.type = type;
    }

    public SimpleStringProperty statProperty() {
        return stat;
    }

    public SimpleIntegerProperty valueProperty() {
        return value;
    }

    public SimpleStringProperty teamProperty() {
        return team;
    }

    public SimpleStringProperty playerProperty() {
        return player;
    }

    public boolean isVisible(List<Type> types, String search) {
        return isTypesMatched(types) &&
                isSearchMatched(search);
    }

    private boolean isTypesMatched(List<Type> types) {
        return types.isEmpty() ||
                types.contains(this.type);
    }

    private boolean isSearchMatched(String search) {
        return search == null || search.isEmpty() ||
                containsText(search, stat) ||
                containsText(search, team) ||
                (isPlayer() && containsText(search, player));
    }

    private boolean containsText(String text, SimpleStringProperty field) {
        return field.getValue().toLowerCase().contains(text.toLowerCase());
    }

    public boolean isPlayer() {
        return type.equals(Type.PLAYER_STATS);
    }

    public enum Type {
        SETS("%stats.matchPeriods"),
        TEAM_STATS("%stats.teams"),
        PLAYER_STATS("%stats.players");

        private final String title;

        Type(String t) {
            title = t;
        }

        public String toString() {
            return title;
        }
    }
}
