/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.summary;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import sportsscheduler.results.Context;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.summary.SummaryResponse;
import static sportsscheduler.results.fx.ResultsFx.redirectToWeb;

public class SummaryController {

    @FXML
    private TextArea console;

    public void initialize() {
        SummaryResponse response = UseCaseInvoker.get(UseCaseInvoker.SCHEDULE_SUMMARY);
        console.setText(response.json);
    }

    public void toHtml() {
        redirectToWeb(String.format(
                "https://www.rozpisyzapasu.cz/vysledky/export/?unitTeam=%s&unitMatch=%s",
                Context.getTeamUnit().toString(),
                Context.getMatchUnit().toString()
        ));
    }
}
