/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.table;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import sportsscheduler.results.Context;
import sportsscheduler.results.ScheduleSettings;
import sportsscheduler.results.fx.FxUtils;
import sportsscheduler.results.fx.Openable;
import sportsscheduler.results.stats.table.PresentableTeamResult;
import sportsscheduler.results.stats.table.Tables;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.FxUtils.resetMaxColumnWith;
import static sportsscheduler.results.fx.FxUtils.tableColumnVisibility;
import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class TablesController extends Openable {

    @FXML
    private TableView classicTable;
    @FXML
    private ToggleGroup tableType, groupType;
    @FXML
    private HBox groups;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Label points;

    private final Map<String, Tables.ClassicTable> classicTables = new LinkedHashMap<>();
    private final Map<String, TableView> crossTables = new LinkedHashMap<>();
    private final String fullTable = "Celková tabulka";

    private String currentGroup;
    private boolean isClassicTableDisplayed;

    public void initialize() {
        loadPoints();
        loadGroupTabs();
        loadClassicTable();
        loadCrossTable();
        showFirstGroupInClassicTable();
        loadTableTypeTabs();
    }

    public void onOpen() {
        resetMaxColumnWith(classicTable);
        for (TableView crossTable : crossTables.values()) {
            resetMaxColumnWith(crossTable);
        }
    }

    public void refresh() {
        classicTables.clear();
        crossTables.clear();
        loadPoints();
        loadClassicTable();
        loadCrossTable();
        setCurrentTable(currentGroup);
    }

    private void loadPoints() {
        ScheduleSettings settings = Context.schedule.settings;
        points.setText(String.format("%d-%d-%d", settings.points.win, settings.points.draw, settings.points.loss));
    }

    private void loadGroupTabs() {
        Set<String> groups = Context.schedule.groups.keySet();
        if (groups.size() > 1) {
            groups.forEach(this::addTab);
            addTab(fullTable);
            FxUtils.deselectableToggleGroup(groupType);
        }
    }

    private void addTab(String title) {
        ToggleButton b = new ToggleButton();
        b.setText(title);
        b.setToggleGroup(groupType);
        b.setOnAction(event -> setCurrentTable(b.getText()));
        groups.getChildren().add(b);
    }

    private void loadClassicTable() {
        for (Map.Entry<String, Tables.GroupTable> e : Context.tables.groups.entrySet()) {
            classicTables.put(e.getKey(), e.getValue().classicTable);
        }
        classicTables.put(fullTable, Context.tables.fullTable);
    }

    // https://github.com/james-d/DynamicTable/blob/master/src/dynamictable/DynamicTable.java
    private void loadCrossTable() {
        for (Map.Entry<String, Tables.GroupTable> g : Context.tables.groups.entrySet()) {
            TableView<ObservableList<StringProperty>> table = new TableView<>();
            crossTables.put(g.getKey(), table);

            createColumn(g.getKey(), "");
            for (PresentableTeamResult t : g.getValue().crossTable.keySet()) {
                createColumn(g.getKey(), t.team);
            }
            createColumn(g.getKey(), "Body");
            createColumn(g.getKey(), "Skóre");
            createColumn(g.getKey(), "Pořadí");

            for (Map.Entry<PresentableTeamResult, Map<String, List<String>>> e : g.getValue().crossTable.entrySet()) {
                ObservableList<StringProperty> row = FXCollections.observableArrayList();
                createCell(row, e.getKey().team);
                for (List<String> scores : e.getValue().values()) {
                    String score = "";
                    for (String s : scores) {
                        if (s != ":") {
                            score += s + "\n";
                        }
                    }
                    createCell(row, score);
                }
                createCell(row, e.getKey().points + "");
                createCell(row, e.getKey().goalsFor + ":" + e.getKey().goalsAgainst);
                createCell(row, e.getKey().rank + "");
                table.getItems().add(row);
            }
        }
    }

    private void createColumn(String group, String text) {
        final int columnIndex = crossTables.get(group).getColumns().size();
        TableColumn<ObservableList<StringProperty>, String> c = new TableColumn<>(text);
        c.setMinWidth(80);
        c.setCellValueFactory(cellDataFeatures -> cellDataFeatures.getValue().get(columnIndex));
        crossTables.get(group).getColumns().add(c);
    }

    private void createCell(ObservableList<StringProperty> items, String text) {
        items.add(new SimpleStringProperty(text));
    }

    private void showFirstGroupInClassicTable() {
        String firstGroup = Context.schedule.groups.keySet().iterator().next();
        isClassicTableDisplayed = true;
        setCurrentTable(firstGroup);
    }

    private void setCurrentTable(String group) {
        currentGroup = group;
        if (isClassicTableDisplayed) {
            Tables.ClassicTable table = classicTables.get(group);
            ObservableList<TeamResultRow> list = FXCollections.observableArrayList();
            list.addAll(table.teams.stream().map(TeamResultRow::new).collect(Collectors.toList()));
            classicTable.setItems(list);
            tableColumnVisibility(classicTable, 4, table.hasExtraWin);
            tableColumnVisibility(classicTable, 5, table.hasDraw);
            tableColumnVisibility(classicTable, 6, table.hasExtraLoss);
        }
        showTable(isClassicTableDisplayed);
    }

    private void loadTableTypeTabs() {
        tableType.selectedToggleProperty().addListener((value, old, newt) -> {
            ToggleButton selectedToggle = (ToggleButton) (newt == null ? old : newt);
            selectedToggle.setSelected(true);
            showTable(selectedToggle.getText().equals("Klasická tabulka"));
        });
    }

    public void showTable(boolean isClassic) {
        isClassicTableDisplayed = isClassic;
        if (isClassicTableDisplayed) {
            borderPane.setCenter(classicTable);
        } else {
            borderPane.setCenter(crossTables.get(currentGroup));
        }
        initTableView((TableView) borderPane.getCenter(), trans("%teams.no"));
    }

    public void onChangePoints() {
        replaceWindow("table/points/points.fxml");
    }
}
