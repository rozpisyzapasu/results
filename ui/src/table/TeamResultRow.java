/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.table;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.stats.table.PresentableTeamResult;

public class TeamResultRow {

    private final SimpleStringProperty team, score;
    private final SimpleIntegerProperty rank, matches, wins, winsExtra, draws, lossesExtra, losses, points;

    public TeamResultRow(PresentableTeamResult result) {
        team = new SimpleStringProperty(result.team);
        rank = new SimpleIntegerProperty(result.rank);
        matches = new SimpleIntegerProperty(result.matchesCount);
        wins = new SimpleIntegerProperty(result.winsCount);
        winsExtra = new SimpleIntegerProperty(result.winsExtraCount);
        draws = new SimpleIntegerProperty(result.drawsCount);
        lossesExtra = new SimpleIntegerProperty(result.lossesExtraCount);
        losses = new SimpleIntegerProperty(result.lossesCount);
        score = new SimpleStringProperty(String.format("%d : %d", result.goalsFor, result.goalsAgainst));
        points = new SimpleIntegerProperty(result.points);
    }

    public SimpleIntegerProperty rankProperty() {
        return rank;
    }

    public SimpleStringProperty teamProperty() {
        return team;
    }

    public SimpleIntegerProperty matchesProperty() {
        return matches;
    }

    public SimpleIntegerProperty winsProperty() {
        return wins;
    }

    public SimpleIntegerProperty winsExtraProperty() {
        return winsExtra;
    }

    public SimpleIntegerProperty drawsProperty() {
        return draws;
    }

    public SimpleIntegerProperty lossesExtraProperty() {
        return lossesExtra;
    }

    public SimpleIntegerProperty lossesProperty() {
        return losses;
    }

    public SimpleStringProperty scoreProperty() {
        return score;
    }

    public SimpleIntegerProperty pointsProperty() {
        return points;
    }
}
