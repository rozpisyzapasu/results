/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.table.points;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import sportsscheduler.results.Context;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.fx.components.Converters;
import sportsscheduler.results.match.ResultType;
import sportsscheduler.results.points.PointsParser;
import sportsscheduler.results.points.TablePoints;
import sportsscheduler.results.points.TablePointsPresenter;
import sportsscheduler.results.points.TablePointsRequest;

import java.util.HashMap;
import java.util.Map;

import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.setStatus;
import static sportsscheduler.results.fx.window.StatusType.ERROR;
import static sportsscheduler.results.fx.window.StatusType.SUCCESS;

public class PointsController implements TablePointsPresenter {

    @FXML
    private TextField win, draw, loss, extraTime, shootout, winByDefault;
    @FXML
    private TextArea matchScores;
    public Map<ResultType, TextField> resultScores = new HashMap<>();

    public void initialize() {
        TablePoints points = Context.schedule.settings.points;
        loadField(win, points.win);
        loadField(draw, points.draw);
        loadField(loss, points.loss);
        resultScores.put(ResultType.EXTRA_TIME, extraTime);
        resultScores.put(ResultType.SHOOTOUT, shootout);
        resultScores.put(ResultType.WIN_BY_DEFAULT, winByDefault);
        for (Map.Entry<ResultType, TextField> entry : resultScores.entrySet()) {
            entry.getValue().setText(PointsParser.convert(entry.getKey(), points));
        }
        matchScores.setText(PointsParser.convert(points.matchScores));
    }

    private void loadField(TextField field, int value) {
        field.setText(value + "");
    }

    public void changePoints() {
        TablePointsRequest r = new TablePointsRequest();
        r.win = getValue(win);
        r.draw = getValue(draw);
        r.loss = getValue(loss);
        r.matchScores = matchScores.getText();
        for (Map.Entry<ResultType, TextField> entry : resultScores.entrySet()) {
            r.resultScores.put(entry.getKey(), entry.getValue().getText());
        }
        UseCaseInvoker.execute(UseCaseInvoker.TABLE_POINTS, r, this);
    }

    private int getValue(TextField win) {
        return Converters.parseInt(win.getText());
    }

    public void pointsAreNotInDescendingOrder() {
        setStatus("Body musí splňovat: výhra >= remíza >= prohra >= 0", ERROR);
    }

    public void pointsChanged() {
        ResultsFx.reloadData();
        backToTable();
        setStatus("Body byly úspěšně upraveny", SUCCESS);
    }

    public void backToTable() {
        replaceWindow("table/tables.fxml");
    }
}
