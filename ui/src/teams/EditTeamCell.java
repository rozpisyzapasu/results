/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams;

import sportsscheduler.results.fx.components.EditButtonCell;
import sportsscheduler.results.fx.teams.team.TeamController;

import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class EditTeamCell extends EditButtonCell<TeamRow> {

    protected String getButtonText() {
        return trans("%team.edit");
    }

    protected void onAction(TeamRow row) {
        TeamController.team = row.getTeam();
        TeamController.isFromMatch = false;
        replaceWindow("teams/team/team.fxml");
    }
}
