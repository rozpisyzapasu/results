/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams;

import javafx.beans.property.SimpleStringProperty;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.overview.PresentableTeam;

import java.util.ArrayList;
import java.util.List;

public class TeamRow {

    private PresentableTeam originalTeam;
    private final SimpleStringProperty team, group, players;

    public TeamRow(PresentableTeam team) {
        originalTeam = team;
        this.team = new SimpleStringProperty(team.name);
        group = new SimpleStringProperty(team.group);
        players = new SimpleStringProperty(convertPlayers(team));
    }

    public PresentableTeam getTeam() {
        return originalTeam;
    }

    private String convertPlayers(PresentableTeam team) {
        List<String> players = new ArrayList<>();
        for (PresentablePlayer p : team.players) {
            players.add(p.name);
        }
        return String.join(", ", players);
    }

    public SimpleStringProperty teamProperty() {
        return team;
    }

    public SimpleStringProperty groupProperty() {
        return group;
    }

    public SimpleStringProperty playersProperty() {
        return players;
    }
}
