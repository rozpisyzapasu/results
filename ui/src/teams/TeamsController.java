/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import sportsscheduler.results.Context;
import sportsscheduler.results.overview.PresentableTeam;

import static sportsscheduler.results.fx.FxUtils.initTableView;
import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class TeamsController {

    @FXML
    private TableView table;
    @FXML
    private Button goToGroups;
    private final ObservableList<TeamRow> rows = FXCollections.observableArrayList();

    public void initialize() {
        loadGroupsButton();
        loadTeams();
        loadColumns();
        loadEditColumn();
    }

    private void loadGroupsButton() {
        goToGroups.setVisible(Context.schedule.groups.size() > 1);
        goToGroups.setOnAction(event -> replaceWindow("teams/groups/groups.fxml"));
    }

    private void loadTeams() {
        for (PresentableTeam t : Context.schedule.teams) {
            TeamRow row = new TeamRow(t);
            rows.add(row);
        }
        initTableView(table, rows, trans("%teams.no"));
    }

    private void loadColumns() {
        ((TableColumn)table.getColumns().get(1)).setVisible(Context.schedule.groupsCount > 1);
        ((TableColumn)table.getColumns().get(2)).setVisible(Context.hasPlayers());
    }

    private void loadEditColumn() {
        TableColumn actionColumn = (TableColumn)table.getColumns().get(table.getColumns().size() - 1);
        actionColumn.setCellFactory(tableColumn -> new EditTeamCell());
    }
}
