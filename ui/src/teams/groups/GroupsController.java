/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams.groups;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.TextFieldListCell;
import sportsscheduler.results.Context;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.team.group.RenameGroupsPresenter;

import java.util.stream.Collectors;

import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.setStatus;
import static sportsscheduler.results.fx.window.StatusType.ERROR;
import static sportsscheduler.results.fx.window.StatusType.SUCCESS;

public class GroupsController implements RenameGroupsPresenter {

    @FXML
    private ListView<String> list;
    private final ObservableList<String> groups = FXCollections.observableArrayList();

    public void initialize() {
        list.setEditable(true);
        list.setItems(groups);
        list.setCellFactory(TextFieldListCell.forListView());
        groups.addAll(Context.schedule.groups.keySet().stream().collect(Collectors.toList()));
    }

    public void saveGroups() {
        String[] names = groups.toArray(new String[groups.size()]);
        UseCaseInvoker.execute(UseCaseInvoker.GROUPS_RENAME, names, this);
    }

    public void nothingChanged() {
        setStatus("Musíte změnit jméno aspoň jedné skupiny", ERROR);
    }

    public void nonUniqueGroupNames() {
        setStatus("Každá skupina musí mít unikátní jméno", ERROR);
    }

    public void groupsRenamed() {
        ResultsFx.reloadData();
        goBack();
        setStatus("Skupiny byly úspěšně přejmenovány", SUCCESS);
    }

    public void goBack() {
        replaceWindow("teams/teams.fxml");
    }
}
