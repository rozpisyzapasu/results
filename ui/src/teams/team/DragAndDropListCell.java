/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams.team;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.util.converter.DefaultStringConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// http://stackoverflow.com/a/25687668/4587679
public class DragAndDropListCell extends TextFieldListCell<String> {

    final static IntegerProperty dragFromIndex = new SimpleIntegerProperty(-1);

    public DragAndDropListCell(ListView list) {
        super(new DefaultStringConverter());

        setOnDragDetected(event -> {
            if (!this.isEmpty()) {
                dragFromIndex.set(this.getIndex());
                Dragboard db = this.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent cc = new ClipboardContent();
                cc.putString(this.getItem());
                db.setContent(cc);
                db.setDragView(this.snapshot(null, null));
            }
        });

        setOnDragOver(event -> {
            if (dragFromIndex.get() >= 0 && dragFromIndex.get() != this.getIndex()) {
                event.acceptTransferModes(TransferMode.MOVE);
            }
        });


        setOnDragEntered(event -> {
            if (dragFromIndex.get() >= 0 && dragFromIndex.get() != this.getIndex()) {
                this.setStyle("-fx-background-color: gold;");
            }
        });
        setOnDragExited(event -> this.setStyle(""));

        setOnDragDropped(event -> {

            int dragItemsStartIndex;
            int dragItemsEndIndex;
            int direction;
            if (this.isEmpty()) {
                dragItemsStartIndex = dragFromIndex.get();
                dragItemsEndIndex = list.getItems().size();
                direction = -1;
            } else {
                if (this.getIndex() < dragFromIndex.get()) {
                    dragItemsStartIndex = this.getIndex();
                    dragItemsEndIndex = dragFromIndex.get() + 1;
                    direction = 1;
                } else {
                    dragItemsStartIndex = dragFromIndex.get();
                    dragItemsEndIndex = this.getIndex() + 1;
                    direction = -1;
                }
            }

            List<String> rotatingItems = list.getItems().subList(dragItemsStartIndex, dragItemsEndIndex);
            List<String> rotatingItemsCopy = new ArrayList<>(rotatingItems);
            Collections.rotate(rotatingItemsCopy, direction);
            rotatingItems.clear();
            rotatingItems.addAll(rotatingItemsCopy);
            dragFromIndex.set(-1);
        });

        setOnDragDone(event -> {
            dragFromIndex.set(-1);
            list.getSelectionModel().select(event.getDragboard().getString());
        });
    }
}
