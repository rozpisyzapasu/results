/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.teams.team;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.PlayersControls;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.fx.matches.score.ScoreController;
import sportsscheduler.results.stats.players.PresentablePlayer;
import sportsscheduler.results.overview.PresentableTeam;
import sportsscheduler.results.team.*;
import sportsscheduler.results.team.player.*;

import static sportsscheduler.results.fx.ResultsFx.*;
import static sportsscheduler.results.fx.window.StatusType.*;

public class TeamController implements UpdateTeamPresenter, RenamePlayerPresenter, DeleteTeamPresenter {

    public static PresentableTeam team;
    public static boolean isFromMatch;
    @FXML
    private ListView<String> list;
    @FXML
    private Button goBack;
    @FXML
    private GridPane grid;
    @FXML
    private TextField newPlayer, deletedPlayer;
    private final ObservableList<String> players = FXCollections.observableArrayList();

    @FXML
    private TextField teamField;

    public void initialize() {
        teamField.setText(team.name);
        list.setEditable(true);
        list.setItems(players);
        list.setCellFactory(param -> new DragAndDropListCell(list));
        for (PresentablePlayer player : team.players) {
            players.add(player.name);
        }
        loadGoBackButton();
        PlayersControls.show(grid);
    }

    public void showDeletedPlayer() {
        int selectedIndex = list.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            String player = players.get(selectedIndex);
            deletedPlayer.setText(String.format("%s (počet gólů: %d)", player, getGoals(player)));
        }
    }

    private int getGoals(String player) {
        for (PresentablePlayer p : team.players) {
            if (p.name.equals(player)) {
                return p.goalsCount;
            }
        }
        return 0;
    }

    public void createPlayer() {
        String newName = newPlayer.getText();
        if (!players.contains(newName)) {
            players.add(newName);
            newPlayer.clear();
            newPlayer.requestFocus();
        }
    }

    public void deleteSelectedPlayer() {
        int selectedIndex = list.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            players.remove(selectedIndex);
            deletedPlayer.clear();
        }
    }

    public void renamePlayer(ListView.EditEvent<String> event) {
        RenamePlayerRequest r = new RenamePlayerRequest();
        r.team = team.name;
        r.player = players.get(event.getIndex());
        r.newName = event.getNewValue();

        UseCaseInvoker.execute(UseCaseInvoker.PLAYER_RENAME, r, this);
        players.set(event.getIndex(), r.newName);
        showDeletedPlayer();
    }

    private void loadGoBackButton() {
        goBack.setText(isFromMatch ? "Zpět na detail zápasu" : trans("%teams.goBack"));
        goBack.setOnAction(event -> backToOverview());
    }

    private void backToOverview() {
        if (isFromMatch) {
            replaceWindow("matches/score/score.fxml");
        } else {
            replaceWindow("teams/teams.fxml");
        }
    }

    public void updateTeam() {
        UpdateTeamRequest r = new UpdateTeamRequest();
        r.team = team.name;
        r.newName = teamField.getText();
        r.players = players.toArray(new String[players.size()]);

        UseCaseInvoker.execute(UseCaseInvoker.TEAM_UPDATE, r, this);
    }

    public void nonExistentTeam() {
        setStatus("Vypadá to, že tým neexistuje", ERROR);
    }

    public void nothingChanged() {
        setStatus(trans("%team.noChange"), ERROR);
    }

    public void successfulChange(UpdateTeamResponse response) {
        String message = "'%s' byl úspěšně upraven";
        if (response.wasNameChanged) {
            message += String.format(" (předchozí jméno bylo '%s')", team.name);
        }
        redirectAfterSuccess(message);
    }

    public void nonExistentPlayer() {
        setStatus("Vypadá to, že hráč neexistuje", ERROR);
    }

    public void nameAlreadyExists() {
        setStatus("Hráč s takovým jménem už je v týmu, zkuste jiné jméno", ERROR);
    }

    public void playerRenamed() {
        setStatus("Hráč byl úspěšně upraven", SUCCESS);
        ResultsFx.reloadData();
    }

    public void successfulDelete() {
        redirectAfterSuccess("'%s' byl smazán");
    }

    private void redirectAfterSuccess(String message) {
        ResultsFx.reloadData();
        if (isFromMatch && ScoreController.match != null) {
            replaceWindow("matches/score/score.fxml");
        } else {
            backToOverview();
        }
        setStatus(String.format(message, teamField.getText()), SUCCESS);
    }

    public void deleteTeam() {
        UseCaseInvoker.execute(UseCaseInvoker.TEAM_DELETE, team.name, this);
    }
}
