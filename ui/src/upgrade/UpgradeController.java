/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.upgrade;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.out.upgrade.AvailableVersion;
import sportsscheduler.results.out.upgrade.UpgradePresenter;

import java.util.List;

import static sportsscheduler.results.fx.ResultsFx.redirectToWeb;

public class UpgradeController implements UpgradePresenter {

    @FXML
    private Button download;
    @FXML
    private TextArea console;
    @FXML
    private Label version, status;

    public static final String currentVersion = "2.2.4";

    public void initialize() {
        version.setText(version.getText() + currentVersion);
        UseCaseInvoker.execute(UseCaseInvoker.APP_UPGRADE, currentVersion, this);
    }

    public void checkFailed() {
        showStatus("Jejda, nepovedlo se stáhnout aktualizaci. Jste připojeni k Internetu?");
    }

    public void noNewVersionExists() {
        showStatus("Používáte nejnovější verzi aplikace!");
    }

    public void upgradeIsAvailable(String downloadUrl, List<AvailableVersion> newVersions) {
        showStatus("Je k dispozici aktualizace. Změny v nově dostupných verzí");
        showDownloadButton(downloadUrl);
        showChangelog(newVersions);
    }

    private void showStatus(String message) {
        status.setText(message);
    }

    private void showDownloadButton(String downloadUrl) {
        download.setVisible(true);
        download.setOnAction(event -> redirectToWeb(downloadUrl));
    }

    private void showChangelog(List<AvailableVersion> changelog) {
        String text = "";
        for (AvailableVersion e : changelog) {
            text += String.format("Verze %s z %s\n", e.version, e.releaseDateTo("d.M.yyyy"));
            for (String s : e.changelog) {
                text += String.format("- %s\n", s);
            }
            text += "\n";
        }
        console.setVisible(true);
        console.setText(text);
    }
}
