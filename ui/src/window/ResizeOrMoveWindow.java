/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.event.EventType;

/**
 * Inspiration:
 * https://github.com/search?utf8=%E2%9C%93&q=UNDECORATED+setOnMouseDragged+resize&type=Code&ref=searchresults
 * https://github.com/oxidyan/oxidyan-thick-client/blob/e93278849d0914e1d3e2c635720aec093bad21e8/src/main/java/com/oxidyan/thick/client/util/ResizeListener.java
 * https://github.com/larsnaesbye/mars-sim/blob/28f7094f2f11c524f381a91fa0d3bfb07f32fb87/mars-sim/mars-sim-ui/src/main/java/com/jme3x/jfx/ResizeAndMoveHelper.java
 */
public class ResizeOrMoveWindow implements EventHandler<MouseEvent> {
    
    private Stage stage;
    private Cursor cursorEvent = Cursor.DEFAULT;
    private int borderSize = 4;
    private double startX = 0, startY = 0;
    private double dragDeltaX, dragDeltaY;
    private double minHeight, minWidth;
    private boolean isResizing;

    public ResizeOrMoveWindow(final Stage s, BorderPane layout) {
        stage = s;
        this.minWidth = layout.getMinWidth();
        this.minHeight = layout.getMinHeight();

        Scene scene = stage.getScene();
        scene.setOnMouseMoved(this);
        scene.setOnMousePressed(this);
        scene.setOnMouseDragged(this);

        layout.setOnMouseDragged(mouseEvent -> {
            if (isWindowMoved(scene, mouseEvent)) {
                s.setX(mouseEvent.getScreenX() + ResizeOrMoveWindow.this.dragDeltaX);
                s.setY(mouseEvent.getScreenY() + ResizeOrMoveWindow.this.dragDeltaY);
                scene.setCursor(Cursor.MOVE);
            } else {
                scene.setCursor(cursorEvent);
            }
        });
        layout.setOnMousePressed(mouseEvent -> {
            // record a delta distance for the drag and drop operation.
            ResizeOrMoveWindow.this.isResizing = false;
            ResizeOrMoveWindow.this.dragDeltaX = s.getX() - mouseEvent.getScreenX();
            ResizeOrMoveWindow.this.dragDeltaY = s.getY() - mouseEvent.getScreenY();
        });
        layout.setOnMouseReleased(event -> {
            scene.setCursor(Cursor.DEFAULT);
        });
    }

    public boolean isWindowMoved(Scene scene, MouseEvent mouseEvent) {
        return !isResizing
            && isInsideBorder(scene.getWidth(), mouseEvent.getSceneX())
            && isInsideBorder(scene.getHeight(), mouseEvent.getSceneY());
    }

    private boolean isInsideBorder(double size, double point) {
        double position = Math.min(point, Math.abs(size - point));
        return position > 2 * borderSize;
    }

    public void handle(final MouseEvent mouseEvent) {
        final Scene scene = stage.getScene();
        final double mouseEventX = mouseEvent.getSceneX(), mouseEventY = mouseEvent.getSceneY();

        if (isEvent(mouseEvent, MouseEvent.MOUSE_MOVED)) {
            if (mouseEventX < borderSize && mouseEventY < borderSize) {
                cursorEvent = Cursor.NW_RESIZE;
            } else if (mouseEventX < borderSize && mouseEventY > scene.getHeight() - borderSize) {
                cursorEvent = Cursor.SW_RESIZE;
            } else if (mouseEventX > scene.getWidth() - borderSize && mouseEventY < borderSize) {
                cursorEvent = Cursor.NE_RESIZE;
            } else if (mouseEventX > scene.getWidth() - borderSize && mouseEventY > scene.getHeight() - borderSize) {
                cursorEvent = Cursor.SE_RESIZE;
            } else if (mouseEventX < borderSize) {
                cursorEvent = Cursor.W_RESIZE;
            } else if (mouseEventX > scene.getWidth() - borderSize) {
                cursorEvent = Cursor.E_RESIZE;
            } else if (mouseEventY < borderSize) {
                cursorEvent = Cursor.N_RESIZE;
            } else if (mouseEventY > scene.getHeight() - borderSize) {
                cursorEvent = Cursor.S_RESIZE;
            } else {
                cursorEvent = Cursor.DEFAULT;
            }
            scene.setCursor(cursorEvent);
        } else if (isEvent(mouseEvent, MouseEvent.MOUSE_PRESSED)) {
            startX = stage.getWidth() - mouseEventX;
            startY = stage.getHeight() - mouseEventY;
        } else if (isEvent(mouseEvent, MouseEvent.MOUSE_DRAGGED)) {
            isResizing = false;
            if (dontHasCursors(Cursor.DEFAULT)) {

                if (dontHasCursors(Cursor.W_RESIZE, Cursor.E_RESIZE)) {
                    final double minHeight = this.minHeight > (borderSize * 2) ? this.minHeight : (borderSize * 2);
                    if (hasOneOfCursors(Cursor.NW_RESIZE, Cursor.N_RESIZE, Cursor.NE_RESIZE)) {
                        if (stage.getHeight() > minHeight || mouseEventY < 0) {
                            isResizing = true;
                            stage.setHeight(stage.getY() - mouseEvent.getScreenY() + stage.getHeight());
                            stage.setY(mouseEvent.getScreenY());
                        }
                    } else {
                        if (stage.getHeight() > minHeight || mouseEventY + startY - stage.getHeight() > 0) {
                            isResizing = true;
                            stage.setHeight(mouseEventY + startY);
                        }
                    }
                }

                if (dontHasCursors(Cursor.N_RESIZE, Cursor.S_RESIZE)) {
                    final double minWidth = this.minWidth > (borderSize * 2) ? this.minWidth : (borderSize * 2);
                    if (hasOneOfCursors(Cursor.NW_RESIZE, Cursor.W_RESIZE, Cursor.SW_RESIZE)) {
                        if (stage.getWidth() > minWidth || mouseEventX < 0) {
                            isResizing = true;
                            stage.setWidth(stage.getX() - mouseEvent.getScreenX() + stage.getWidth());
                            stage.setX(mouseEvent.getScreenX());
                        }
                    } else {
                        if (stage.getWidth() > minWidth || mouseEventX + startX - stage.getWidth() > 0) {
                            isResizing = true;
                            stage.setWidth(mouseEventX + startX);
                        }
                    }
                }
            }
        }
    }

    private boolean isEvent(MouseEvent mouseEvent, EventType<MouseEvent> expectedEvent) {
        return mouseEvent.getEventType().equals(expectedEvent);
    }

    private boolean dontHasCursors(Cursor... cursors) {
        for (Cursor c : cursors) {
            if (hasCursor(c)) {
                return false;
            }
        }
        return true;
    }

    private boolean hasOneOfCursors(Cursor... cursors) {
        for (Cursor c : cursors) {
            if (hasCursor(c)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasCursor(Cursor c) {
        return cursorEvent.equals(c);
    }
}
