/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import sportsscheduler.results.fx.upgrade.UpgradeController;

import static sportsscheduler.results.fx.ResultsFx.redirectToWeb;
import static sportsscheduler.results.fx.ResultsFx.replaceWindow;
import static sportsscheduler.results.fx.ResultsFx.trans;

public class SidebarController {

    @FXML
    private Label version;
    @FXML
    private Button teams, matches, tables, stats, summary, settings;
    private Button activeButton;

    public void initialize() {
        version.setText(version.getText() + UpgradeController.currentVersion);
    }

    public void onOverview() {
        replaceWindow("teams/teams.fxml");
    }

    public void onMatches() {
        replaceWindow("matches/matches.fxml");
    }

    public void onTables() {
        replaceWindow("table/tables.fxml");
    }

    public void onStats() {
        replaceWindow("stats/stats.fxml");
    }

    public void onSummary() {
        replaceWindow("summary/summary.fxml");
    }

    public void onSettings() {
        replaceWindow("settings/settings.fxml");
    }

    public void goToWebsite() {
        redirectToWeb("https://www.rozpisyzapasu.cz");
    }

    public void goToUpgrade() {
        replaceWindow("upgrade/upgrade.fxml");
        deactivateButton();
    }

    public void activateButton(String selectedSection) {
        deactivateButton();
        activeButton = findButton(selectedSection);
        activeButton.getStyleClass().add("active");
    }

    public void deactivateButton() {
        if (activeButton != null) {
            activeButton.getStyleClass().remove("active");
            activeButton = null;
        }
    }

    private Button findButton(String selectedSection) {
        switch (selectedSection) {
            case "teams": return teams;
            case "matches": return matches;
            case "tables": return tables;
            case "stats": return stats;
            case "summary": return summary;
            case "settings": return settings;
            default: return teams;
        }
    }

    public void reloadButtonsTexts() {
        teams.setText(trans("%teams"));
    }
}
