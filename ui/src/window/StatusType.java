/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

public enum StatusType {
    SUCCESS("success"),
    ERROR("warning");

    private final String cssClass;

    private StatusType(String s) {
        cssClass = s;
    }

    public String toString(){
        return cssClass;
    }
}
