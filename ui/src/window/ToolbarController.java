/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import sportsscheduler.results.Context;
import sportsscheduler.results.UseCaseInvoker;
import sportsscheduler.results.fx.components.FileDialog;
import sportsscheduler.results.fx.ResultsFx;
import sportsscheduler.results.out.exporter.ExportPresenter;
import sportsscheduler.results.out.exporter.JsonWriter.WriteResult;
import sportsscheduler.results.overview.PresentableSchedule;

import static sportsscheduler.results.fx.ResultsFx.*;
import static sportsscheduler.results.fx.window.StatusType.*;

public class ToolbarController implements ExportPresenter {

    @FXML
    private Button saveButton;
    private String defaultSaveTooltip = "Uloží JSON soubor s rozpisem na disk";

    @FXML
    private TextField path;
    @FXML
    private ComboBox scheduleName;
    private FileDialog fileDialog;

    private ObservableList<String> schedules = FXCollections.observableArrayList();

    public void initialize() {
        fileDialog = new FileDialog(path);
        fileDialog.canCreateNewFile();
        scheduleName.setItems(schedules);
        scheduleName.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            String selectedSchedule = getSelectedSchedule();
            ResultsFx.changeSchedule(selectedSchedule);
        });
        saveButton.setTooltip(new Tooltip(defaultSaveTooltip));
    }

    private String getSelectedSchedule() {
        int index = scheduleName.getSelectionModel().getSelectedIndex();
        return schedules.get(index);
    }

    public void updateToolbar(PresentableSchedule schedule, boolean isChanged) {
        if (!schedules.contains(schedule.name)) {
            schedules.add(schedule.name);
        }
        int index = schedules.indexOf(schedule.name);
        scheduleName.getSelectionModel().select(index);
        path.setText(schedule.path);
        moveCaretToEnd();
        updateSaveButton(isChanged);
    }

    public void onImport() {
        replaceWindow("importer/import.fxml");
    }

    public void save() {
        UseCaseInvoker.execute(UseCaseInvoker.SCHEDULE_EXPORT, path.getText(), this);
    }

    public void openFileDialog() {
        fileDialog.openFileDialog();
        moveCaretToEnd();
    }

    private void moveCaretToEnd() {
        path.end();
    }

    public void scheduleNotSaved(WriteResult result) {
        setStatus(translateErrorMessage(result), ERROR);
    }

    private String translateErrorMessage(WriteResult error) {
        switch (error) {
            case PATH_IS_WEBLINK:
                return "Zvolte, kam se má rozpis uložit. Klikněte na 'Změnit', vyberte umístění a následně uložte změny.";
            case IO_ERROR:
            default:
                return "Rozpis se nepodařilo uložit. Zkuste to znovu, zkuste změnit cestu k rozpisu v horním panelu";
        }
    }

    public void scheduleExported() {
        setStatus("Rozpis byl úspěšně uložen na disk", SUCCESS);
        updateSaveButton(false);
    }

    public void updateSaveButton(boolean isChanged) {
        Context.currentSchedule.isSaved = Context.currentSchedule.isSaved && !isChanged;
        if (Context.currentSchedule.isSaved) {
            saveButton.getTooltip().setText(defaultSaveTooltip);
            saveButton.getStyleClass().removeAll("warning");
        } else {
            saveButton.getTooltip().setText("Poslední změny nebyly uloženy");
            saveButton.getStyleClass().add("warning");
        }
    }
}
