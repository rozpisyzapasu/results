/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sportsscheduler.results.fx.ResultsFx;

import java.io.InputStream;

public class UndecoratedWindowController {

    @FXML
    protected BorderPane window;
    private Stage stage;

    private boolean isMaximized = false;
    private Rectangle2D backupWindowBounds = null;

    public void setStage(Stage s) {
        stage = s;
        deleteDefaultWindowStyle();
        setAppIcon();
        if (isResizable()) {
            new ResizeOrMoveWindow(stage, window);
        }
    }

    public Stage openWindow(UndecoratedWindowController controller, Scene scene, Modality modality) {
        Stage newStage = new Stage();
        newStage.initOwner(stage);
        newStage.setScene(scene);
        newStage.initModality(modality);
        controller.setStage(newStage);
        newStage.show();
        return newStage;
    }

    protected boolean isResizable() {
        return true;
    }

    public void deleteDefaultWindowStyle() {
        stage.initStyle(StageStyle.UNDECORATED);
    }

    private void setAppIcon() {
        InputStream file = ResultsFx.class.getResourceAsStream("assets/images/logo.png");
        stage.getIcons().add(new Image(file));
    }

    public void onClose() {
        stage.close();
    }

    public void onMaximize() {
        isMaximized = !isMaximized;
        if (isMaximized) {
            maximize();
        } else {
            resetToWindow();
        }
    }

    private void maximize() {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        backupWindowBounds = new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
    }

    private void resetToWindow() {
        stage.setX(backupWindowBounds.getMinX());
        stage.setY(backupWindowBounds.getMinY());
        stage.setWidth(backupWindowBounds.getWidth());
        stage.setHeight(backupWindowBounds.getHeight());
    }

    public void onMinimize() {
        stage.setIconified(true);
    }

    public void onFullScreen() {
        boolean isFullScreen = stage.isFullScreen();
        stage.setFullScreen(!isFullScreen);
    }
}
