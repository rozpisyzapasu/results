/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import sportsscheduler.results.Context;

import java.io.IOException;

import static sportsscheduler.results.fx.ResultsFx.showConfirmDialog;
import static sportsscheduler.results.fx.ResultsFx.showNonModalWindow;

public class WindowController extends UndecoratedWindowController {

    @FXML
    private BorderPane content;
    @FXML
    private AnchorPane sidebar, toolbar;
    @FXML
    private Label status;

    public void setSidebar(Node sidebar) {
        this.sidebar.getChildren().addAll(sidebar);
    }

    public void setToolbar(Node toolbar) {
        this.toolbar.getChildren().addAll(toolbar);
        AnchorPane.setLeftAnchor(toolbar, 0.0);
        AnchorPane.setRightAnchor(toolbar, 0.0);
    }

    public void setContent(Node content) {
        this.content.setCenter(content);
    }

    public void setStatus(String message, StatusType type) {
        status.setText(message);
        status.getStyleClass().clear();
        status.getStyleClass().add(type.toString());
    }

    public void clearStatusBar() {
        status.setText("");
        status.getStyleClass().clear();
    }

    public void onClose() {
        if (Context.repository.areAllSchedulesSaved()) {
            Platform.exit();
        } else {
            showConfirmDialog();
        }
    }

    public void onUndock() throws IOException {
        showNonModalWindow();
    }
}
