/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window.confirm;

import javafx.application.Platform;
import sportsscheduler.results.fx.window.UndecoratedWindowController;

public class ConfirmController extends UndecoratedWindowController {

    protected boolean isResizable() {
        return false;
    }

    public void closeApp() {
        Platform.exit();
    }

    public void stayInApp() {
        onClose();
    }
}
