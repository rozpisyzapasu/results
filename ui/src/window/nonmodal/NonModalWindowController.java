/*
 * Sports Scheduler Results (https://gitlab.com/rozpisyzapasu/results)
 * @license New BSD License
 * @author Zdenek Drahos
 */

package sportsscheduler.results.fx.window.nonmodal;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import sportsscheduler.results.fx.window.UndecoratedWindowController;

public class NonModalWindowController extends UndecoratedWindowController {

    private int fontSize = 14;
    private int fontIncrement = 2;
    private int minSize = 12, maxSize = 48;

    @FXML
    private Button increase, decrease;

    public void setContent(Node n) {
        n.setId("main");
        window.setCenter(n);
        changeFontSize(0);
    }

    private void changeFontSize(int changedValue) {
        int newSize = fontSize + changedValue;
        if (!isValidFontSize(newSize)) {
            return;
        }
        fontSize = newSize;
        window.getCenter().setStyle("-fx-font-size: " + fontSize);
        refreshButtonsVisibility();
    }

    private boolean isValidFontSize(int newSize) {
        return newSize >= minSize && newSize <= maxSize;
    }

    public void increaseFont() {
        changeFontSize(fontIncrement);
    }

    public void decreaseFont() {
        changeFontSize(-fontIncrement);
    }

    private void refreshButtonsVisibility() {
        increase.setVisible(isValidFontSize(fontSize + fontIncrement));
        decrease.setVisible(isValidFontSize(fontSize - fontIncrement));
    }
}
